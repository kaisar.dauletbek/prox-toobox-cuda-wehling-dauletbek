import unittest
import cupy as cp
import numpy as np

from proxtoolbox.experiments.phase.SIRIUS_CDI_Experiment import SIRIUS_CDI_Experiment
from proxtoolbox.experiments.phase.SIRIUS_CDI_Experiment_cuda import SIRIUS_CDI_Experiment_Cuda


class TestSIRIUSExperiment(unittest.TestCase):
    def test_loadData(self):
        CDI = SIRIUS_CDI_Experiment(algorithm = 'DRl', Nx = 256, Ny = 256, 
            lambda_0 = 0.9, lambda_max = 0.5)
        CDI_CUDA = SIRIUS_CDI_Experiment_Cuda(algorithm = 'RAAR_Cuda', Nx = 256, Ny = 256, 
            lambda_0 = 0.9, lambda_max = 0.5)
        # loadData() is called by by the initialization of the Experiment object
        # now compare the loaded data

        cp.testing.assert_array_almost_equal(CDI.rt_data[0], CDI_CUDA.rt_data, decimal=5)
        
        # self.data
        cp.testing.assert_array_almost_equal(CDI.data[0], CDI_CUDA.data, decimal=5)

        # self.norm_rt_data
        cp.testing.assert_array_almost_equal(CDI.norm_rt_data, CDI_CUDA.norm_rt_data, decimal=5)
        
        # self.support
        cp.testing.assert_array_almost_equal(CDI.support, CDI_CUDA.support, decimal=5)

        # self.data_sq
        cp.testing.assert_array_almost_equal(CDI.data_sq[0], CDI_CUDA.data_sq, decimal=4)

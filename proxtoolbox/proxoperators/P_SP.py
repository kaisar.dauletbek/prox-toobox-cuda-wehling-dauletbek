
from proxtoolbox.proxoperators.proxoperator import ProxOperator
import numpy as np
from numpy import zeros, real, shape

class P_SP(ProxOperator):
    """
    Projection onto nonnegativity and support constraints
    
    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on May 23, 2002.
    """

    def __init__(self, experiment):
        self.support_idx = experiment.support_idx

    def eval(self, u, prox_idx=None):
        """
        Projects the input data onto nonnegativity and
        support constraints.

        Parameters
        ----------
        u : ndarray
            Function in the physical domain to be projected
        prox_idx : int, optional
            Index of this prox operator
        
        Returns
        -------
        p_SP : ndarray
            The projection in the physical (time) domain
        """
        p_SP = zeros(shape(u), np.float64)
        p_SP[self.support_idx] = np.maximum(real(u[self.support_idx]), 0)
        return p_SP


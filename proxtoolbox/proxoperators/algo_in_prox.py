import time

import numpy as np
from proxtoolbox import algorithms, proxoperators
from proxtoolbox.proxoperators.proxoperator import ProxOperator

class algo_in_prox(ProxOperator):
    """
    Class to call an interative algorithm as a prox operator.

    Written by Deborah Lepper, 
    Universität Göttingen, 2023.

    """

    def __init__(self, experiment):
        """
        Initialization method for a concrete instance
        
        Parameters
        ----------
        experiment : instance of Experiment class
            Experiment object that will use this prox operaror.
            This experiment should define the main algorithm
            parameters as inner_XXX.
        """
        super().__init__(experiment)

        self.algorithm_name = experiment.inner_algorithm
        self.iterate_monitor_name = experiment.inner_iterate_monitor
        self.accelerator_name = experiment.inner_accelerator
        self.proxOperators = experiment.inner_proxOperators
        self.productProxOperators = experiment.inner_productProxOperators
        self.dBregman_potential = experiment.inner_dBregman_potential
        self.MAXIT = experiment.inner_MAXIT
        self.TOL = experiment.inner_TOL
        self.silent = experiment.inner_silent

        self.norm_data = experiment.norm_data

        self.truth = None
        self.truth_dim = None
        self.norm_truth = None
        self.formulation = None
        self.diagnostic = None
        self.n_product_Prox = None

        self.anim = False
        self.animate = None
        self.anim_step = None
        self.Nx = None
        self.Ny = None
        self.Nz = None
        self.product_space_dimension = None
        self.lambda_0 = None
        self.lambda_max = None
        self.lambda_switch = None
        self.debug = None

        # retrieve the classes corresponding to the prox operators' names given above
        self.retrieveProxOperatorClasses()

        # get specific parameters for the algorithm to run as prox
        self.getAlgoParameters(experiment)


    def retrieveProxOperatorClasses(self):
        """
        Retrieve the Python classes corresponding to the prox operators
        defined by the setupProxOperators() method in the experiment class.
        
        This is a helper method called during the initialization
        process. It should not be overriden, unless the derived 
        experiment class uses an additional category of prox operators
        not covered here.
        """

        # find prox operators classes based on their name
        # and replace names by actual classes
        proxOperatorClasses = []
        for prox in self.proxOperators:
            if prox != '':
                proxOperatorClasses.append(getattr(proxoperators, prox))
        self.proxOperators = proxOperatorClasses
        # same with product prox operators
        proxOperatorClasses = []
        for prox in self.productProxOperators:
            if prox != '':
                proxOperatorClasses.append(getattr(proxoperators, prox))
        self.productProxOperators = proxOperatorClasses
        # same with Bregman potentials
        if self.dBregman_potential is not None:
            self.dBregman_potential = getattr(proxoperators, self.dBregman_potential)


    def getAlgoParameters(self, experiment):
        """
        Get specific parameters for the algorithm to run as prox.

        Can be overwritten or extended to include
        more algorithm-specific parameters.
        
        Parameters
        ----------
        experiment : instance of Experiment class
            Experiment object that will use this prox operaror.
        """

        if self.algorithm_name == 'GPM':
            self.inner_f = experiment.inner_f
            self.inner_df = experiment.inner_df
            self.inner_constBounds = experiment.inner_constBounds
            self.alphaGPM = experiment.alphaGPM
            self.betaGPM = experiment.betaGPM


    def instanciateAlgorithm(self, u, blockNo=None, masks=None):
        """
        Instanciate algorithm and some objects used by the
        algorithm, namely the iterate monitor and accelerator,
        based on the names provided.
        Is called by eval(u)

        May be extended for further algorithms.
        
        Parameters
        ----------
        u : ndarray or a list of ndarray objects
            Initial iterate for the algorithm/prox operator.
        blockNo : int, optional
            Number of the current block of u, where the
            algorithm should run on.
        masks : ndarray or list of ndarray objects of dtype bool, optional
            Masks corresponding to the blocks. For extracting
            correct entries from u.
        """
        
        if self.algorithm_name == 'GPM':

            assert blockNo is not None, 'No valid block number given for algo_in_prox'
            assert masks is not None, 'No valid masks given for algo_in_prox'

            self.u0 = u[masks[blockNo]]
            self.f = self.inner_f(u,blockNo)
            self.df = self.inner_df(u,blockNo)
            self.constBounds = self.inner_constBounds[blockNo]

            if np.linalg.norm(self.df(self.u0)) < self.TOL:
                return True

        algorithmClass = getattr(algorithms, self.algorithm_name)
        iterateMonitorClass = getattr(algorithms, self.iterate_monitor_name)
        iterateMonitor = iterateMonitorClass(self)
        if self.accelerator_name is not None:
            acceleratorClass = getattr(algorithms, self.accelerator_name)
            accelerator = acceleratorClass(self)
        else:
            accelerator = None
        self.algorithm = algorithmClass(self, iterateMonitor, accelerator)
        return False

        
    def eval(self, u, blockNo=None, masks=None, prox_parameter=None, prox_idx=None):
        """
        Applies an algorithm as a prox operator to the input data.
        
        Parameters
        ----------
        u : ndarray or a list of ndarray objects
            Input data where to run the algorithm on.
        blockNo : int, optional
            Number of the current block of u, where the
            algorithm should run on.
        masks : ndarray or list of ndarray objects of dtype bool, optional
            Masks corresponding to the blocks. For extracting
            correct entries from u.
        prox_parameter : optional scalar-valued
            Prox parameter.
        prox_idx : int, optional
            Index of this prox operator.
        
        Returns
        -------
        ndarray or a list of ndarray objects
            Result of the application of the prox operator onto
            the input data.
        """

        # instantiate iterate monitor, accelerator and algorithm
        bOut = self.instanciateAlgorithm(u, blockNo=blockNo, masks=masks)
        if bOut:
            return u

        if not self.silent:
            print("Running " + self.algorithm_name + " as prox operator...")

        t = time.time()

        self.output = self.runAlgorithm(self.u0)

        self.output['stats']['time'] = time.time() - t

        self.postprocess(u=u,blockNo=blockNo, masks=masks)

        if not self.silent:
            print("Took", self.output['stats']['iter'], "iterations and",
                self.output['stats']['time'], "seconds.")

        return self.uOut
    

    def runAlgorithm(self, u):
        """
        Run the algorithm associated with this prox operator.
        Called by the run() method. May be extended/overwritten
        to provide additional behaviour, such as running a warmup
        algorithm before the main algorithm.

        Parameters
        ----------
        u : ndarray or a list of ndarray objects
            Input data where to run the algorithm on.
        
        Returns
        -------
        dictionary containing the output of the algorithm. In 
            particular, it contains the last iterate and various
            statistics.
        """
        return self.algorithm.run(u)
    
    
    def postprocess(self,u=None,blockNo=None, masks=None):
        """
        Processing of the data returned by the algorithm.
        Mainly storing the output data in self.uOut.
        If the algorithm was run just on a block of u,
        the arguments 'u', 'blockNo' and 'masks' should
        not be None.

        Parameters
        ----------
        u : ndarray or a list of ndarray objects, optional
            Input data where to run the algorithm on.
        blockNo : int, optional
            Number of the current block of u, where the
            algorithm should run on.
        masks : ndarray or list of ndarray objects of dtype bool, optional
            Masks corresponding to the blocks. For extracting
            correct entries from u.
        """

        if blockNo != None and type(masks) != type(None):
            self.uOut = u.copy()
            self.uOut[masks[blockNo]] = self.output['u']
        else:
            self.uOut = self.output['u']
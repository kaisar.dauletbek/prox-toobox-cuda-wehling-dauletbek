import cupy as cp
from cupy import zeros_like, unravel_index, sum, max, zeros
from proxtoolbox.proxoperators.proxoperator import ProxOperator
from proxtoolbox.utils.orbitaltomog import tile_array, bin_array
from proxtoolbox.utils.size import size_matlab

__all__ = ['P_Sparsity_cuda', 'P_Sparse_support_cuda', 'P_Sparsity_real_cuda', 'P_Sparsity_Symmetric_cuda', 'P_Sparsity_Symmetric_real_cuda',
           'P_Sparsity_Superpixel_cuda', 'P_Sparsity_Superpixel_real_cuda', 'P_nonneg_sparsity_cuda']

class P_Sparsity_cuda(ProxOperator):
    """
    Projection subroutine for projecting onto a sparsity constraint
    """

    def __init__(self, experiment):
        """
        Initialization

        Parameters
        ----------
        experiment : class with the attribute 'sparsity_parameter', which should be an integer
        indicating the number of pixels allowed by the projection
        """
        super(P_Sparsity_cuda, self).__init__(experiment)
        self.sparsity_parameter = experiment.sparsity_parameter

        if hasattr(experiment, 'use_sparsity_with_support') and experiment.use_sparsity_with_support:
            self.support = cp.asarray(experiment.sparsity_support.real, dtype=cp.uint)
            assert cp.sum(self.support) > 0, 'Invalid empty sparsity_support given'
        else:
            self.support = 1

        if self.sparsity_parameter > 30 or len(experiment.u0.shape) != 2:
            def value_selection(original, indices, sparsity_parameter):
                idx_for_threshold = unravel_index(indices[-sparsity_parameter], original.shape)
                threshold_val = cp.abs(original[idx_for_threshold]).get()
                return (cp.abs(original) >= threshold_val) * original
        else:
            def value_selection(original, indices, sparsity_parameter):
                out = zeros_like(original)
                hits = indices[-sparsity_parameter:].get()
                hit_idx = [unravel_index(hit, original.shape) for hit in hits]
                for _idx in hit_idx:
                    out[_idx[0], _idx[1]] = original[_idx[0], _idx[1]]
                return out

        self.value_selection = value_selection

    def eval(self, u, prox_idx=None):
        """
        Parameters
        ----------
        u : array_like - Input, array to be projected

        Returns
        -------
        p_Sparsity : array_like, the projection
        """
        u *= self.support  # apply support (simple 1 if no support)
        u_flat = cp.ravel(u)  # flatten array before sorting
        sorting = cp.argsort(cp.abs(u_flat), axis=None)  # gives indices of sorted array in ascending order
        p_sparse = cp.zeros_like(sorting, dtype=u.dtype)  # create array of zeros
        p_sparse[sorting[-1 * int(self.sparsity_parameter):]] = u_flat[sorting[-1 * int(self.sparsity_parameter):]]
        p_sparse = cp.reshape(p_sparse, u.shape)  # recreate original shape
        return p_sparse

class P_Sparse_support_cuda(P_Sparsity_cuda):
    """
    Projection subroutine for projecting onto a combined sparsity and realness constraint

    order: realness, sparsity
    """
    def __init__(self, experiment):
        self.support_idx = experiment.support_idx
        self.support = experiment.support
        self.sparsity_parameter = experiment.sparsity_parameter
    
    def eval(self, u, prox_idx=None):
        """
        Projects the input data onto nonnegativity and
        support constraints.

        Parameters
        ----------
        u : ndarray
            Function in the physical domain to be projected
        prox_idx : int, optional
            Index of this prox operator
        
        Returns
        -------
        p_S : ndarray
            The projection
        """
        p_S = cp.zeros(u.shape, dtype=u.dtype)
        p_S[self.support_idx] = u[self.support_idx]

        return super(P_Sparse_support_cuda, self).eval(p_S, prox_idx)

class P_Sparsity_real_cuda(P_Sparsity_cuda):
    """
    Projection subroutine for projecting onto a combined sparsity and realness constraint

    order: realness, sparsity
    """

    def eval(self, u, prox_idx=None):
        return super(P_Sparsity_real_cuda, self).eval(u.real, prox_idx)


class P_Sparsity_Symmetric_cuda(P_Sparsity_cuda):
    """
    Projection subroutine for projecting onto a combined sparsity and symmetry constraint

    order: symmetry, sparsity
    """

    def __init__(self, experiment):
        super(P_Sparsity_Symmetric_cuda, self).__init__(experiment)
        self.symmetry = experiment.symmetry_type  # antisymmetric = -1, symmetric = 1
        self.symmetry_axis = experiment.symmetry_axis  # -1 for last, 0 for first, 'both', 'all' or None for full flip

    def eval(self, u, prox_idx=None):
        if self.symmetry_axis in ['both', 'all']:
            mirror = cp.flip(u, axis=None)
        else:
            mirror = cp.flip(u, axis=self.symmetry_axis)
        inp = (u + self.symmetry * mirror) / 2
        out = super(P_Sparsity_Symmetric_cuda, self).eval(inp, prox_idx)
        return out


class P_Sparsity_Symmetric_real_cuda(P_Sparsity_Symmetric_cuda):
    """
    Projection subroutine for projecting onto a combined symmetry, sparsity and realness constraint

    order: realness, symmetry, sparsity
    """

    def eval(self, u, prox_idx=None):
        out = super(P_Sparsity_Symmetric_real_cuda, self).eval(u.real, prox_idx)
        return out


class P_Sparsity_Superpixel_cuda(P_Sparsity_cuda):
    """
    Apply sparsity on superpixels, i.e. on the binned array
    """

    def __init__(self, experiment):
        super(P_Sparsity_Superpixel_cuda, self).__init__(experiment)
        # Set the superpixel size:
        self.superpixel_size = experiment.superpixel_size
        # Bin the support:
        if self.support != 1:
            self.support = bin_array(self.support, self.superpixel_size)
            self.support = self.support / max(self.support)
        # Assert that the binning+upsampling conserves the array size
        test_shape = tile_array(bin_array(experiment.u0, self.superpixel_size, pad_zeros=False),
                                self.superpixel_size).shape
        assert test_shape == experiment.u0.shape, 'Given array size does not allow for binning'
        # TODO: allow for padding, then cut of the remainder after tile_array

    def eval(self, u, prox_idx=None):
        binned = bin_array(cp.abs(u), self.superpixel_size)
        sparse_array = super(P_Sparsity_Superpixel_cuda, self).eval(binned, prox_idx)
        mask = tile_array(sparse_array, self.superpixel_size, normalize=True) > 0
        return cp.where(mask, u, 0)


class P_Sparsity_Superpixel_real_cuda(P_Sparsity_Superpixel_cuda):
    """
    Apply real-valued sparsity on superpixels, i.e. on the binned array
    """

    def eval(self, u, prox_idx=None):
        return super(P_Sparsity_Superpixel_real_cuda, self).eval(u.real, prox_idx)


class P_nonneg_sparsity_cuda(ProxOperator):
    """
    Projection subroutine for projecting onto support constraints
    """
    def eval(self, u, prox_idx=None):
        """
        Parameters
        ----------
        u : array_like - Input, array to be projected

        Returns
        -------
        p_Sparsity : array_like, the projection
        """
        m, n, p, q = size_matlab(u)
        null = cp.zeros(u.shape)
        tmp = cp.maximum(null, cp.real(u))
        tmp = cp.reshape(tmp, (m * n * p * q), order='F')
        indices = cp.argsort(tmp)[::-1]  # descend sort
        p_Sparsity = cp.zeros(m * n * p * q)
        p_Sparsity[indices[0:int(self.sparsity_parameter)]] = tmp[indices[0:int(self.sparsity_parameter)]]
        p_Sparsity = cp.reshape(p_Sparsity, u.shape, order='F')
        return p_Sparsity

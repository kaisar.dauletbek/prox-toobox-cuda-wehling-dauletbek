from proxtoolbox.proxoperators.proxoperator import ProxOperator

#__all__ = ['MC_P_A']

class MC_P_A(ProxOperator):
    """
    Class for matrix completion experiment with alternating projections.

    Written by Maximilian Seeber, 
    Universität Göttingen, 2022.

    """

    def __init__(self, experiment):
        super().__init__(experiment)
        self.mask = experiment.mask
        self.M = experiment.M

    def eval(self, u):
        # u[self.mask] = self.M[self.mask]
        # return u
        return u + self.mask * (self.M - u)
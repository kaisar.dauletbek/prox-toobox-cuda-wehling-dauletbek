from proxtoolbox.proxoperators.proxoperator import ProxOperator
from proxtoolbox.proxoperators import Prox_lp
import numpy as np

__all__ = ['Proj_l1_cmplx', 'Proj_l1_cmplx_multi']

class Proj_l1_cmplx(ProxOperator):
    """
    Projection onto the scaled complex l1 ball.
    """

    def __init__(self, experiment):
        super().__init__(experiment)
    
    def eval(self, x, c, tol=1e-6):
        """
        Projection onto complex L1 ball B = {x in C^n : sum_j |x_j| <= c}.

        Parameters
        ----------
        x : ndarray, complex
            Complex input data to be projected.
        c : float
            Scaling of the l1 ball.
        tol : float, optional
            Bound, where entry counts as being equal to zero.
            For avoiding division through zero. 

        Returns
        -------
        res : array, complex
            projection of x onto l1 ball with bound c.
        """
        
        scale = Prox_lp.Proj_l1.eval(np.abs(x), c)
        
        idx = np.where(np.abs(x) > tol)
        
        res = np.zeros(x.shape,dtype='complex')
        res[idx] = scale[idx] * x[idx] / np.abs(x)[idx]

        return res
    
class Proj_l1_cmplx_multi(ProxOperator):
    """
    Projection onto multiple scaled complex l1 balls
        B1 x B2 x ... x BNx

        where Bk = {x in C^n : sum_j |x_j| <= c_k} for
        k = 1,...,Nx.

    Assumed that the masks are disjoint.
    """
    
    def __init__(self, experiment):
        super().__init__(experiment)
        self.Nx = experiment.Nx
        self.masks = experiment.constMasks
        self.proj = Proj_l1_cmplx(experiment)
    
    def eval(self, x, c):
        """
        Projection on all N complex l1 balls B = {x in C : sum |x_i| <= c}.

        Parameters
        ----------
        x : ndarray, complex
            Complex input data to be projected.
        c : 1D-ndarray
            Scalings of the l1 ball.

        Returns
        -------
        res : array, complex
            Projection of x onto l1 balls with bounds c.
        """
        
        xhat = np.zeros(x.shape,dtype='complex')
        for n in range(self.Nx):
            xhat[self.masks[n]] = self.proj.eval(x[self.masks[n]], c[n])
        return xhat
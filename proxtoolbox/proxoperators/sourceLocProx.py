
from proxtoolbox.utils.cell import Cell, isCell
from proxtoolbox.proxoperators.proxoperator import ProxOperator
from proxtoolbox.utils.size import size_matlab
from proxtoolbox.utils.accessors import accessors
import numpy as np
from numpy.linalg import norm

class SourceLocProx(ProxOperator):
    """
    Projection onto the given entries in a Sudoku problem
    """
    
    def __init__(self, experiment):
        self.sensors = experiment.sensors
        self.data = experiment.data
        self.shift_data = experiment.shift_data
        self.Nx = experiment.Nx
        self.Ny = experiment.Ny
        self.product_space_dimension = experiment.product_space_dimension


class Project_on_sphere(SourceLocProx):
    """
    Prox operator that projects a vector on the product of
    balls with different radius 

    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on Sept. 8, 2016.    
    """

    def __init__(self, experiment):
        super(Project_on_sphere, self).__init__(experiment)

    def eval(self, u, prox_index = None):
        """
        Projection of a vector on the product of balls with different radius        

        Parameters
        ----------
        u : ndarray
            Input data to be projected
        prox_idx : int, optional
            Index of this prox operator
         
        Returns
        -------
        u_new : ndarray
            The projection
        """
        u_new = u.copy()
        if prox_index is not None:
            j = prox_index
        else:
            j = 0

        center = self.shift_data[j]
        norm_u = norm(u - center)
        u_new = (u - center) * self.data[j]/norm_u + center
        return u_new



class Project_on_spheres(SourceLocProx):
    """
    Prox operator that projects a vector on the product of
    balls with different radius 

    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on Sept. 8, 2016.    
    """

    def __init__(self, experiment):
        super(Project_on_spheres, self).__init__(experiment)

    def eval(self, u, prox_index = None):
        """
        Projection of a vector on the product of balls with different radius        

        Parameters
        ----------
        u : Cell 
            Input data to be projected
        prox_idx : int, optional
            Index of this prox operator
         
        Returns
        -------
        u_new : Cell
            The projection
        """
        if isCell(u):
            u_new = Cell(len(u))
            for j in range (len(u)):
                norm_u = norm(u[j])
                u_new[j] = u[j] * self.data[j] / norm_u
        else:
            errMsg = "Only cells are supported for the input data to be projected"
            raise NotImplementedError(errMsg)

        return u_new

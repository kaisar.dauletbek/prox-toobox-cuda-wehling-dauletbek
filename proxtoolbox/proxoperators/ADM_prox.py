
from distutils.log import error
from proxtoolbox import proxoperators
from proxtoolbox.proxoperators.proxoperator import ProxOperator, magproj
from proxtoolbox.proxoperators import dQuadratic, HQuadratic
from proxtoolbox.proxoperators.Bregman_potentials import *
from proxtoolbox.utils.cell import Cell, isCell
from proxtoolbox.utils.size import size_matlab
from proxtoolbox.utils.accessors import accessors
from proxtoolbox.utils.functions import Quadratic
from numpy import pi, shape, reshape, where, amax, zeros_like, identity, zeros, conj, log, real, cbrt, sqrt, matmul, transpose
from numpy.fft import fft2, ifft2, fft, ifft
from scipy.sparse.linalg import cg

from proxtoolbox.algorithms.samsara import Samsara, BFGS1_product, cdSY_mat


class ADM_Context:
    """
    Class containing data and helper functions for ADM
    prox operators and ADM related classes.
    """
    def __init__(self, experiment):
        # the rest below is phase retreival specific
        if hasattr(experiment, 'illumination'):
            self.illumination = experiment.illumination
        else:
            self.illumination = None
        if hasattr(experiment, 'FT_conv_kernel'):
            self.FT_conv_kernel = experiment.FT_conv_kernel
        else:
            self.FT_conv_kernel = 'Yo Mama'
        if hasattr(experiment, 'fresnel_nr'):
            self.fresnel_nr = experiment.fresnel_nr
        else:
            self.fresnel_nr = None
        if hasattr(experiment, 'farfield'):
            self.fresnel_nr = experiment.farfield
        else:
            self.farfield = None
        # what comes next is an inelegant hack
        if hasattr(experiment, 'Nx'):
            if experiment.Nx is None:
                # put the domain_dim into the Nx ``slot"   
                self.Nx = experiment.domain_dim
            else:
                self.Nx = experiment.Nx
        else:
            # put the domain_dim into the Nx ``slot"   
            self.Nx = experiment.domain_dim

        if hasattr(experiment, 'Ny'):
            if experiment.Ny is None:
                # put the image_dim into the Ny ``slot"   
                self.Ny = experiment.image_dim
            else:
                self.Ny = experiment.Ny
        else:
            # put the image_dim into the Ny ``slot"   
            self.Ny = experiment.image_dim
        if hasattr(experiment, 'domain_objective'):
            self.domain_objective = experiment.domain_objective
        else:
            self.domain_objective = None
        if hasattr(experiment, 'image_objective'):
            self.image_objective = experiment.image_objective
        else:
            self.image_objective = None

        if hasattr(experiment, 'coupling_function'):
            self.coupling_function = experiment.coupling_function
        else:
            self.coupling_function = None
        if hasattr(experiment, 'coupling_approximation'):
            self.coupling_function = experiment.coupling_function
        else:
            self.coupling_approximation = 'linear'

        if hasattr(experiment, 'masks'):
            self.masks = experiment.masks
        else:
            self.masks = None
        if hasattr(experiment, 'magn'):
            self.magn = experiment.magn
        else:
            self.magn = None
        if hasattr(experiment, 'beam'):
            self.beam = experiment.beam
        else:
            self.beam = None
   
class Prox_NoLips_primal_l0_plus_Bregman(ProxOperator, ADM_Context):
    """
    Prox operator for the NoLips experiment with l0 primal regularization 
    and the part of the Bregman potential that is not strongly convex  
    """
    def __init__(self, experiment):
        # because of multiple inheritance we call explicitely
        # the __init__ method of the two parent classes 
        ProxOperator.__init__(self, experiment)
        ADM_Context.__init__(self, experiment)
        if hasattr(experiment, 'A_tens'):
            self.A_tens = experiment.A_tens
        else:
            self.A_tens = 0
        if hasattr(experiment, 'b_mat'):
            self.b_mat = experiment.b_mat
        else:
            self.b_mat = 0
        if hasattr(experiment, 'c_vec'):
            self.c_vec = experiment.c_vec
        else:
            self.c_vec = 0
        if hasattr(experiment, 'Lip_grad_max'):
            self.Lip_grad_max = experiment.Lip_grad_max
        else:
            self.Lip_grad_max = 1.0
        if hasattr(experiment, 'Lip_grad_k'):
            self.Lip_grad_k = experiment.Lip_grad_k
        else:
            self.Lip_grad_k = 1.0
        if hasattr(experiment, 'augmentation_scaling'):
            self.augmentation_scaling = experiment.augmentation_scaling
        else:
            self.augmentation_scaling = 1.0
        if hasattr(experiment, 'dBregman_potential'):
            self.dBregman_potential = experiment.dBregman_potential
        else:
            self.dBregman_potential = None
        if hasattr(experiment, 'primal_scaling'):
            self.primal_scaling = experiment.primal_scaling
        else:
            self.primal_scaling = 1.0
        if hasattr(experiment, 'image_scaling'):
            self.image_scaling = experiment.image_scaling
        else:
            self.image_scaling = 1.0


    def eval(self, u):
        """
        Computes the Bregman prox of the l0-function via the formula in 
        Bolte, Sabach, Teboulle, and Vaisbourd (2018) 

        Had to write this in an odd way.  Formally, the ADM method
        for the NoLips problem structure 
        works on 2 blocks of variables, (x,y) with different 
        dimensions.  In the fixed point interpretation of this
        the ADM algorithm is written as the composition of two operators
        T_1T_2, each self-mappings on the domain of x.  For monitoring 
        purposes, I keep the (x,y) block structure, but then have to split 
        the T_2 operator between this primal operator and the operator in the 
        image domain.  If that makes any sense.  

        Input
        ----------
        u : cell of 2 vectors, the primal domain variable of dim n
            and the auxilliary image variable of dim m 
        prox_idx : int, optional
            Index of this prox operator
        scaling : scaling in the l0-prox
        
        Returns
        -------
        u_vec : vector of dim n
        """
        # This first part completes the action of the operator T_2 in 
        # the fixed point formulation of NoLips ADM:
        dq_xk_mat = dQuadratic.eval(self, u[0])
        q_xk_vec = Quadratic(self.A_tens, self.b_mat, self.c_vec,u[0])
        image_resid_vec = q_xk_vec - u[1]
        # augmentation scaling is the parameter rho in the paper
        dQxkyk_vec = self.augmentation_scaling * matmul(transpose(dq_xk_mat), image_resid_vec)
        if self.dBregman_potential==None:
            dphi_xk_vec = 0
        else:
            dphi_xk_vec = self.dBregman_potential.eval(u[0])
        # self.lmbda is alpha/Lbar in the paper
        u_prime = u[0] - self.lmbda * (dQxkyk_vec - self.Lip_grad_max * dphi_xk_vec)

        # This second part is just the action of the operator T_1 in 
        # the fixed point formulation of NoLips ADM:
        # self.lambda is alpha/Lbar in the paper
        shrinkage_parameter = self.lmbda/self.primal_scaling
        z_vec = proxoperators.Prox_l0.eval(u_prime, shrinkage_parameter)
        if self.dBregman_potential==None:
                tmp=1
        # couldn't get sting comparison to work
        # elif self.dBregman_potential=='dh_4':
        else:
            # using map avoids for... loops at each of the inner functions!
            a = sum(z_vec**2) * self.lmbda * self.Lip_grad_max 
            # tmp = real root of a*t^3 + t - 1 via Cardano's formula
            if abs(a)>=1e-16:
                tmp = cbrt(0.5/a + sqrt(0.25/(a**2) + (1/27)/(a**3))) + cbrt(0.5/a - sqrt(0.25/(a**2) + (1/27)/(a**3)))
            else:
                tmp=1
        # else:
        #    print('Message from ADM_prox.py: Not implemented')

        u_vec = (+1) * tmp * z_vec 
        return u_vec
 
class Prox_NoLips_primal_l1_plus_Bregman(ProxOperator, ADM_Context):
    """
    Prox operator for the NoLips experiment with l1 primal regularization 
    and the part of the Bregman potential that is not strongly convex  
    """
    def __init__(self, experiment):
        # because of multiple inheritance we call explicitely
        # the __init__ method of the two parent classes 
        ProxOperator.__init__(self, experiment)
        ADM_Context.__init__(self, experiment)
        if hasattr(experiment, 'A_tens'):
            self.A_tens = experiment.A_tens
        else:
            self.A_tens = 0
        if hasattr(experiment, 'b_mat'):
            self.b_mat = experiment.b_mat
        else:
            self.b_mat = 0
        if hasattr(experiment, 'c_vec'):
            self.c_vec = experiment.c_vec
        else:
            self.c_vec = 0
        if hasattr(experiment, 'Lip_grad_max'):
            self.Lip_grad_max = experiment.Lip_grad_max
        else:
            self.Lip_grad_max = 1.0
        if hasattr(experiment, 'Lip_grad_k'):
            self.Lip_grad_k = experiment.Lip_grad_k
        else:
            self.Lip_grad_k = 1.0
        if hasattr(experiment, 'augmentation_scaling'):
            self.augmentation_scaling = experiment.augmentation_scaling
        else:
            self.augmentation_scaling = 1.0
        if hasattr(experiment, 'dBregman_potential'):
            self.dBregman_potential = experiment.dBregman_potential
        else:
            self.dBregman_potential = None
        if hasattr(experiment, 'primal_scaling'):
            self.primal_scaling = experiment.primal_scaling
        else:
            self.primal_scaling = 1.0

    def eval(self, u):
        """
        Computes the Bregman prox of the l1-norm via the formula in 
        Bolte, Sabach, Teboulle, and Vaisbourd (2018) 

        Had to write this in an odd way.  Formally, the ADM method
        for the NoLips problem structure 
        works on 2 blocks of variables, (x,y) with different 
        dimensions.  In the fixed point interpretation of this
        the ADM algorithm is written as the composition of two operators
        T_1T_2, each self-mappings on the domain of x.  For monitoring 
        purposes, I keep the (x,y) block structure, but then have to split 
        the T_2 operator between this primal operator and the operator in the 
        image domain.  If that makes any sense.  

        Input
        ----------
        u : cell of 2 vectors, the primal domain variable of dim n
            and the auxilliary image variable of dim m 
        prox_idx : int, optional
            Index of this prox operator
        scaling : scaling in the l1-prox
        
        Returns
        -------
        u_vec : vector of dim n
        """
        # This first part completes the action of the operator T_2 in 
        # the fixed point formulation of NoLips ADM:
        dq_xk_mat = dQuadratic.eval(self, u[0])
        q_xk_vec = Quadratic(self.A_tens, self.b_mat, self.c_vec,u[0])
        image_resid_vec = q_xk_vec - u[1]
        # augmentation scaling is the parameter rho in the paper
        dQxkyk_vec = self.augmentation_scaling * matmul(transpose(dq_xk_mat), image_resid_vec)
        if self.dBregman_potential==None:
            dphi_xk_vec = 0
        else:
            dphi_xk_vec = self.dBregman_potential.eval(u[0])
        # self.lmbda is alpha/Lbar in the paper
        u_prime = u[0] - self.lmbda * (dQxkyk_vec - self.Lip_grad_max * dphi_xk_vec)

        # This second part is just the action of the operator T_1 in 
        # the fixed point formulation of NoLips ADM:
        # self.lambda is alpha/Lbar in the paper
        shrinkage_parameter = self.lmbda / self.primal_scaling
        z_vec = proxoperators.Prox_l1.eval(u_prime, shrinkage_parameter)
        if self.dBregman_potential==None:
                tmp=1
        # couldn't get sting comparison to work
        # elif self.dBregman_potential=='dh_4':
        else:
            # using map avoids for... loops at each of the inner functions!
            a = sum(z_vec**2) * self.lmbda * self.Lip_grad_max 
            # tmp = real root of a*t^3 + t - 1 via Cardano's formula
            if abs(a)>=1e-16:
                tmp = cbrt(0.5/a + sqrt(0.25/(a**2) + (1/27)/(a**3))) + cbrt(0.5/a - sqrt(0.25/(a**2) + (1/27)/(a**3)))
            else:
                tmp=1
        # else:
        #    print('Message from ADM_prox.py: Not Implemented')
        u_vec = (+1) * tmp * z_vec 
        return u_vec

class Prox_NoLips_primal_l2_plus_Bregman(ProxOperator, ADM_Context):
    """
    Prox operator for the NoLips experiment with l2^2 primal regularization 
    and the part of the Bregman potential that is not strongly convex  
    """
    def __init__(self, experiment):
        # because of multiple inheritance we call explicitely
        # the __init__ method of the two parent classes 
        ProxOperator.__init__(self, experiment)
        ADM_Context.__init__(self, experiment)
        if hasattr(experiment, 'A_tens'):
            self.A_tens = experiment.A_tens
        else:
            self.A_tens = 0
        if hasattr(experiment, 'b_mat'):
            self.b_mat = experiment.b_mat
        else:
            self.b_mat = 0
        if hasattr(experiment, 'c_vec'):
            self.c_vec = experiment.c_vec
        else:
            self.c_vec = 0
        if hasattr(experiment, 'Lip_grad_max'):
            self.Lip_grad_max = experiment.Lip_grad_max
        else:
            self.Lip_grad_max = 1.0
        if hasattr(experiment, 'Lip_grad_k'):
            self.Lip_grad_k = experiment.Lip_grad_k
        else:
            self.Lip_grad_k = 1.0
        if hasattr(experiment, 'augmentation_scaling'):
            self.augmentation_scaling = experiment.augmentation_scaling
        else:
            self.augmentation_scaling = 1.0
        if hasattr(experiment, 'dBregman_potential'):
            self.dBregman_potential = experiment.dBregman_potential
        else:
            self.dBregman_potential = None
        if hasattr(experiment, 'primal_scaling'):
            self.primal_scaling = experiment.primal_scaling
        else:
            self.primal_scaling = 1.0


    def eval(self, u):
        """
        Computes the Bregman prox of the l2-norm^2 

        Had to write this in an odd way.  Formally, the ADM method
        for the NoLips problem structure 
        works on 2 blocks of variables, (x,y) with different 
        dimensions.  In the fixed point interpretation of this
        the ADM algorithm is written as the composition of two operators
        T_1T_2, each self-mappings on the domain of x.  For monitoring 
        purposes, I keep the (x,y) block structure, but then have to split 
        the T_2 operator between this primal operator and the operator in the 
        image domain.  If that makes any sense.  

        Input
        ----------
        u : cell of 2 vectors, the primal domain variable of dim n
            and the auxilliary image variable of dim m 
        prox_idx : int, optional
            Index of this prox operator
        scaling : scaling in the l2-prox
        
        Returns
        -------
        u_vec : vector of dim n
        """
        # This first part completes the action of the operator T_2 in 
        # the fixed point formulation of NoLips ADM:
        dq_xk_mat = dQuadratic.eval(self, u[0])
        q_xk_vec = Quadratic(self.A_tens, self.b_mat, self.c_vec,u[0])
        image_resid_vec = q_xk_vec - u[1]
        # augmentation scaling is the parameter rho in the paper
        dQxkyk_vec = self.augmentation_scaling * matmul(transpose(dq_xk_mat), image_resid_vec)
        if self.dBregman_potential==None:
            dphi_xk_vec = 0
        else:
            dphi_xk_vec = self.dBregman_potential.eval(u[0])

        # self.lmbda is alpha/Lbar in the paper
        u_prime = u[0] - self.lmbda * (dQxkyk_vec - self.Lip_grad_max * dphi_xk_vec)

        # This second part is just the action of the operator T_1 in 
        # the fixed point formulation of NoLips ADM:
        # self.lambda is alpha/Lbar in the paper
        prox_parameter = self.lmbda / self.primal_scaling
        if self.dBregman_potential==None:
                tmp=1/(1+prox_parameter)
        # couldn't get string comparison to work
        # elif self.dBregman_potential=='dh_4':
        else:
            # using map avoids for... loops at each of the inner functions!
            a = sum(u_prime**2) * prox_parameter * self.Lip_grad_max 
            # tmp = real root of a*zeta^3 + (1+t)zeta - 1 via Cardano's formula
            if abs(a)>=1e-5:
                descr = sqrt(0.25/(a**2) + (1/27)*(((1+prox_parameter * self.Lip_grad_max)/a)**3))
                tmp = cbrt(0.5/a + descr) + cbrt(0.5/a - descr)
            else:
                tmp=1/(1+prox_parameter)
        # else:
        #    print('Message from ADM_prox.py: Not Implemented')
        u_vec = (+1) * tmp * u_prime 
        return u_vec

class Prox_NoLips_primal_quadratic_plus_Bregman(ProxOperator, ADM_Context):
    """
    Prox operator for the NoLips experiment with shifted l2^2 primal regularization 
    and the part of the Bregman potential that is not strongly convex  
    """
    def __init__(self, experiment):
        # because of multiple inheritance we call explicitely
        # the __init__ method of the two parent classes 
        ProxOperator.__init__(self, experiment)
        ADM_Context.__init__(self, experiment)
        if hasattr(experiment, 'A_tens'):
            self.A_tens = experiment.A_tens
        else:
            self.A_tens = 0
        if hasattr(experiment, 'b_mat'):
            self.b_mat = experiment.b_mat
        else:
            self.b_mat = 0
        if hasattr(experiment, 'c_vec'):
            self.c_vec = experiment.c_vec
        else:
            self.c_vec = 0
        if hasattr(experiment, 'Lip_grad_max'):
            self.Lip_grad_max = experiment.Lip_grad_max
        else:
            self.Lip_grad_max = 1.0
        if hasattr(experiment, 'Lip_grad_k'):
            self.Lip_grad_k = experiment.Lip_grad_k
        else:
            self.Lip_grad_k = 1.0
        if hasattr(experiment, 'augmentation_scaling'):
            self.augmentation_scaling = experiment.augmentation_scaling
        else:
            self.augmentation_scaling = 1.0
        if hasattr(experiment, 'dBregman_potential'):
            self.dBregman_potential = experiment.dBregman_potential
        else:
            self.dBregman_potential = None
        if hasattr(experiment, 'primal_scaling'):
            self.primal_scaling = experiment.primal_scaling
        else:
            self.primal_scaling = 1.0
        if hasattr(experiment, 'primal_shift'):
            self.primal_shift = experiment.primal_shift
        else:
            self.primal_shift = None


    def eval(self, u):
        """
        Computes the Bregman prox of the l2-norm^2 

        Had to write this in an odd way.  Formally, the ADM method
        for the NoLips problem structure 
        works on 2 blocks of variables, (x,y) with different 
        dimensions.  In the fixed point interpretation of this
        the ADM algorithm is written as the composition of two operators
        T_1T_2, each self-mappings on the domain of x.  For monitoring 
        purposes, I keep the (x,y) block structure, but then have to split 
        the T_2 operator between this primal operator and the operator in the 
        image domain.  If that makes any sense.  

        Input
        ----------
        u : cell of 2 vectors, the primal domain variable of dim n
            and the auxilliary image variable of dim m 
        prox_idx : int, optional
            Index of this prox operator
        scaling : scaling in the l2-prox
        
        Returns
        -------
        u_vec : vector of dim n
        """
        # This first part completes the action of the operator T_2 in 
        # the fixed point formulation of NoLips ADM:
        dq_xk_mat = dQuadratic.eval(self, u[0])
        q_xk_vec = Quadratic(self.A_tens, self.b_mat, self.c_vec,u[0])
        image_resid_vec = q_xk_vec - u[1]
        # augmentation scaling is the parameter rho in the paper
        dQxkyk_vec = self.augmentation_scaling * matmul(transpose(dq_xk_mat), image_resid_vec)
        if self.dBregman_potential==None:
            dphi_xk_vec = 0
        else:
            dphi_xk_vec = self.dBregman_potential.eval(u[0])

        # self.lmbda is alpha/Lbar in the paper
        u_prime = u[0] - self.lmbda * (dQxkyk_vec - self.Lip_grad_max * dphi_xk_vec)

        # This second part is just the action of the operator T_1 in 
        # the fixed point formulation of NoLips ADM:
        # self.lambda is alpha/Lbar in the paper
        if self.primal_shift!=None:
            u_prime = u_prime-self.primal_shift 

        prox_parameter = self.lmbda / self.primal_scaling
        if self.dBregman_potential==None:
            tmp=1/(1+prox_parameter)
        # couldn't get string comparison to work
        # elif self.dBregman_potential=='dh_4':
        else:
            # using map avoids for... loops at each of the inner functions!
            a = sum(u_prime**2) * prox_parameter * self.Lip_grad_max 
            
            # tmp = real root of a*zeta^3 + (1+t)zeta - 1 via Cardano's formula
            if abs(a)>=1e-5:
                descr = sqrt(0.25/(a**2) + (1/27)*(((1+prox_parameter * self.Lip_grad_max)/a)**3))
                tmp   = cbrt(0.5/a + descr) + cbrt(0.5/a - descr)
            else:
                tmp=1/(1+prox_parameter)
            # else:
            #    print('Message from ADM_prox.py: Not Implemented')
        u_vec = (+1) * tmp * u_prime
        return u_vec

class Prox_NoLips_primal_0_plus_Bregman(ProxOperator, ADM_Context):
    """
    Prox operator for the NoLips experiment with zero primal objective 
    and the part of the Bregman potential that is not strongly convex  
    This is the only experiment where we are trying quadratic 
    approximation of the coupling function, to this class has more
    to it than the other classes for the primal prox
    """
    def __init__(self, experiment):
        # because of multiple inheritance we call explicitely
        # the __init__ method of the two parent classes 
        ProxOperator.__init__(self, experiment)
        ADM_Context.__init__(self, experiment)
        if hasattr(experiment, 'A_tens'):
            self.A_tens = experiment.A_tens
        else:
            self.A_tens = 0
        if hasattr(experiment, 'b_mat'):
            self.b_mat = experiment.b_mat
        else:
            self.b_mat = 0
        if hasattr(experiment, 'c_vec'):
            self.c_vec = experiment.c_vec
        else:
            self.c_vec = 0
        if hasattr(experiment, 'Lip_grad_max'):
            self.Lip_grad_max = experiment.Lip_grad_max
        else:
            self.Lip_grad_max = 1.0
        if hasattr(experiment, 'Lip_grad_k'):
            self.Lip_grad_k = experiment.Lip_grad_k
        else:
            self.Lip_grad_k = 1.0
        if hasattr(experiment, 'augmentation_scaling'):
            self.augmentation_scaling = experiment.augmentation_scaling
        else:
            self.augmentation_scaling = 1.0
        if hasattr(experiment, 'dBregman_potential'):
            self.dBregman_potential = experiment.dBregman_potential
        else:
            self.dBregman_potential = None
        if hasattr(experiment, 'coupling_approximation'):
            self.coupling_approximation = experiment.coupling_approximation
        else:
            self.coupling_approximation = 'linear'
        if hasattr(experiment, 'primal_scaling'):
            self.primal_scaling = experiment.primal_scaling
        else:
            self.primal_scaling = 1.0
        if hasattr(experiment, 'image_scaling'):
            self.image_scaling = experiment.image_scaling
        else:
            self.image_scaling = 1.0


    def eval(self, u):
        """
        Computes the Bregman prox of the 0-function via the formula in 
        Bolte, Sabach, Teboulle, and Vaisbourd (2018) 

        Had to write this in an odd way.  Formally, the ADM method
        for the NoLips problem structure 
        works on 2 blocks of variables, (x,y) with different 
        dimensions.  In the fixed point interpretation of this
        the ADM algorithm is written as the composition of two operators
        T_1T_2, each self-mappings on the domain of x.  For monitoring 
        purposes, I keep the (x,y) block structure, but then have to split 
        the T_2 operator between this primal operator and the operator in the 
        image domain.  If that makes any sense.  

        Input
        ----------
        u : cell of 2 vectors, the primal domain variable of dim n
            and the auxilliary image variable of dim m 
        prox_idx : int, optional
            Index of this prox operator
        scaling : scaling in the l0-prox
        
        Returns
        -------
        u_vec : vector of dim n
        """
        # This first part completes the action of the operator T_2 in 
        # the fixed point formulation of NoLips ADM:
        dq_xk_mat = dQuadratic.eval(self, u[0])
        q_xk_vec = Quadratic(self.A_tens, self.b_mat, self.c_vec,u[0])
        image_resid_vec = q_xk_vec - u[1]
        # augmentation scaling is the parameter rho in the paper
        dQxkyk_vec = self.augmentation_scaling * matmul(transpose(dq_xk_mat), image_resid_vec)
        if self.dBregman_potential==None:
            dphi_xk_vec = 0
        else:
            dphi_xk_vec = self.dBregman_potential.eval(u[0])
        zk_vec = dQxkyk_vec - self.Lip_grad_max * dphi_xk_vec    
        if self.coupling_approximation=='quadratic':
            HQ_xk_tens = HQuadratic.eval(self, u[0])
            HQ_xk_mat = zeros(shape(HQ_xk_tens[0,:,:]))
            for j in range(len(image_resid_vec)):
                HQ_xk_mat += image_resid_vec[j]*HQ_xk_tens[j,:,:]
            HQ_xk_mat += matmul(transpose(dq_xk_mat,[1,0]), dq_xk_mat)     
            HQ_xk_mat *=  self.lmbda
            # I am overwriting the true Hessian in the line below: 
            HQ_xk_mat += identity(shape(HQ_xk_mat)[1])
            # at this point, I need to compute the solution to 
            # HQ_xk_mat*u_prime = zk_vec
            # and then compute u_vec = u[0] -  self.lmbda*u_prime
            u_prime = reshape(cg(HQ_xk_mat, zk_vec, x0=dQxkyk_vec, tol=1e-10, maxiter=None, M=None, callback=None, atol=None)[0],(shape(u[0])))
            # The following only holds if (F+phi)=0.  If 
            # we are in this class, F=0, so the only thing to check is
            # that phi=0
            if (dphi_xk_vec == 0):
                u_vec = u[0] -  self.lmbda*u_prime
            else:
                print('Message from ADM_prox.py, class Prox_NoLips_primal_0_plus_Bregman:')
                print('The only implementation so far is for no Bregman prox.')
                u_vec = None  
                # This should throw an error
        else:
            # the linear approximation is the default
            # self.lmbda is alpha/Lbar in the paper
            u_prime = u[0] - self.lmbda * (zk_vec)

            # This second part is just the action of the operator T_1 in 
            # the fixed point formulation of NoLips ADM:
            # self.lambda is alpha/Lbar in the paper
            prox_parameter = self.lmbda / self.primal_scaling
            if self.dBregman_potential==None:
                tmp=1
            else:
                # using map avoids for... loops at each of the inner functions!
                a = sum(u_prime**2) * prox_parameter * self.Lip_grad_max 
                # tmp = real root of a*t^3 + t - 1 via Cardano's formula
                if abs(a)>=1e-5:
                    descr = sqrt(0.25/(a**2) + (1/27)*(1/(a**3)))
                    tmp = cbrt(0.5/a + descr) + cbrt(0.5/a - descr)
                else:
                    tmp=1
            u_vec = tmp * u_prime 
        return u_vec

class Prox_NoLips_primal_0_plus_3prox(ProxOperator, ADM_Context):
    """
    Prox operator for the NoLips experiment with zero primal objective 
    and a proximal term to the power 3.
    In this experiment we are trying quadratic 
    approximation of the coupling function.  The primal prox is approximated
    numerically with a call to the samsara nonlinear optimization routine.
    """
    def __init__(self, experiment):
        # because of multiple inheritance we call explicitely
        # the __init__ method of the two parent classes 
        ProxOperator.__init__(self, experiment)
        ADM_Context.__init__(self, experiment)
        if hasattr(experiment, 'A_tens'):
            self.A_tens = experiment.A_tens
        else:
            self.A_tens = 0
        if hasattr(experiment, 'b_mat'):
            self.b_mat = experiment.b_mat
        else:
            self.b_mat = 0
        if hasattr(experiment, 'c_vec'):
            self.c_vec = experiment.c_vec
        else:
            self.c_vec = 0
        if hasattr(experiment, 'Lip_grad_max'):
            self.Lip_grad_max = experiment.Lip_grad_max
        else:
            self.Lip_grad_max = 1.0
        if hasattr(experiment, 'Lip_grad_k'):
            self.Lip_grad_k = experiment.Lip_grad_k
        else:
            self.Lip_grad_k = 1.0
        if hasattr(experiment, 'augmentation_scaling'):
            self.augmentation_scaling = experiment.augmentation_scaling
        else:
            self.augmentation_scaling = 1.0
        if hasattr(experiment, 'Gprox'):
            self.Gprox = experiment.Gprox
        else:
            self.Gprox = None
        if hasattr(experiment, 'dGprox'):
            self.dGprox = experiment.dGprox
        else:
            self.dGprox = None
        if hasattr(experiment, 'dBregman_potential'):
            self.dBregman_potential = experiment.dBregman_potential
        else:
            self.dBregman_potential = None
        if hasattr(experiment, 'coupling_approximation'):
            self.coupling_approximation = experiment.coupling_approximation
        else:
            self.coupling_approximation = 'linear'
        if hasattr(experiment, 'primal_scaling'):
            self.primal_scaling = experiment.primal_scaling
        else:
            self.primal_scaling = 1.0
        if hasattr(experiment, 'image_scaling'):
            self.image_scaling = experiment.image_scaling
        else:
            self.image_scaling = 1.0


    def eval(self, u):
        """
        Computes the generalized prox of the 0-function via numerical approximation

        Had to write this in an odd way.  Formally, the ADM method
        for the NoLips problem structure 
        works on 2 blocks of variables, (x,y) with different 
        dimensions.  In the fixed point interpretation of this
        the ADM algorithm is written as the composition of two operators
        T_1T_2, each self-mappings on the domain of x.  For monitoring 
        purposes, I keep the (x,y) block structure, but then have to split 
        the T_2 operator between this primal operator and the operator in the 
        image domain.  If that makes any sense.  

        Input
        ----------
        u : cell of 2 vectors, the primal domain variable of dim n
            and the auxilliary image variable of dim m 
        prox_idx : int, optional
            Index of this prox operator
        scaling : scaling in the l0-prox
        
        Returns
        -------
        u_vec : vector of dim n
        """
        # This first part computes to quadratic approximation of the 
        # primal part of the coupling:
        dq_xk_mat = dQuadratic.eval(self, u[0])
        q_xk_vec = Quadratic(self.A_tens, self.b_mat, self.c_vec,u[0])
        image_resid_vec = q_xk_vec - u[1]
        # augmentation scaling is the parameter rho in the paper
        dQxkyk_vec = self.augmentation_scaling * matmul(transpose(dq_xk_mat), image_resid_vec)
        zk_vec = dQxkyk_vec
        # Initial guess:  the linear prox point
        # self.lmbda is alpha/Lbar in the paper
        uold_vec = u[0] - zk_vec
        unew_vec = None
        stepsize = 999.
        prox_objective_new = None
        dprox_objective_new_vec = None

        # This second part is just the action of the operator T_1 in 
        # the fixed point formulation of NoLips ADM:
        # self.lambda is alpha/Lbar in the paper
        # end initial guess

        if self.coupling_approximation=='quadratic':
            HQ_xk_tens = HQuadratic.eval(self, u[0])
            HQ_xk_mat = zeros(shape(HQ_xk_tens[0,:,:]))
            for j in range(len(image_resid_vec)):
                HQ_xk_mat += image_resid_vec[j]*HQ_xk_tens[j,:,:]
            HQ_xk_mat += matmul(transpose(dQ_xk_mat,[1,0]), dQ_xk_mat)     
            HQ_xk_mat *=  self.augmentation_scaling
        else:
            HQ_xk_mat = zeros(shape(u[0]))

        # The following only holds if (F+phi)=0.  If 
        # we are in this class, F=0, so the only thing to check is
        # that phi=0
        if (dphi_xk_vec != 0):
            print('Message from ADM_prox.py, class Prox_NoLips_primal_0_plus_3prox:')
            print('The only implementation so far is for no Bregman prox.')
            u_vec = None  
            # This should throw an error
        else:
            # call to samsara
            # the objective for the generalized prox is:  
            prox_objective0_vec = dQxkyk_vec + matmul(HQ_xk_mat, u[0])
            prox_objective1_vec = matmul(HQ_xk_mat, uold_vec)
            prox_objective_old = matmul(prox_objective0_vec+prox_objective1_vec, uold_vec)
            primal_resid_vec = uold_vec - u[0]
            norm_primal_resid = matmul(primal_resid_vec, primal_resid_vec)
            prox_objective_old += ((1/3)/self.lambd)*norm_primal_resid**3
            dprox_objective_old_vec = prox_objective0_vec + prox_objective1_vec
            dprox_objective_old_vec += 1/self.lambd*norm_primal_resid*primal_resid_vec
            norm_dprox_objective_new = 999.
            # Samsara options
            options = {}
            options['verbose'] = True
            options['alpha_0'] = 5e-12
            options['gamma'] = .5
            options['c'] = .01
            options['beta'] = .9999999
            options['eta1'] = .995
            options['eta2'] = .8
            options['eta3'] = .25
            # options['eta4'] = .025
            options['maxmem'] = 8
            # options['lookback'] = 0
            options['tr'] = 1e+15

            options['ngrad_TOL'] = 1e-6
            options['step_TOL'] = 1e-12
            options['Maxit'] = 1000

            # options['QN_method'] = Broyden1_product
            # options['Step_finder'] = Explicit_TR
            # options['History'] = cdSY_mat
            # options['update_conditions'] = 'Exact'
            options['initial_step'] = 1e+5*options['step_TOL']
            options['QN_method'] = BFGS1_product
            # options['Step_finder'] = Explicit_TR
            options['History'] = cdSY_mat
            options['update_conditions'] = 'Trust Region'
            options['initial_step'] = 1e-7

            s = Samsara(**options)
            # Tolerances
            ngrad_TOL = 2e-14
            step_TOL = 0.0001*ngrad_TOL
            Maxit = 1500
            it = 0
       
            # enter Samsara main loop
            while it < Maxit and norm_dprox_objective_new > ngrad_TOL and stepsize > step_TOL:
                unew_vec, uold_vec, prox_objective_old, dprox_objective_old_vec, stepsize =\
                    s.run(uold_vec, unew_vec, prox_objective_old, prox_objective_new,
                      dprox_objective_old_vec, dprox_objective_new_vec)

                it += 1
                # evaluate new objective value and new gradient:
                primal_resid_vec = unew_vec - u[0]
                norm_primal_resid = matmul(primal_resid_vec, primal_resid_vec)
                prox_objective_new += ((1/3)/self.lambd)*norm_primal_resid**3
                dprox_objective_new_vec = prox_objective0_vec + prox_objective1_vec
                dprox_objective_new_vec += 1/self.lambd*norm_primal_resid*primal_resid_vec
                norm_dprox_objective_new = matmul(dprox_objective_new_vec, dprox_objective_new_vec)

            if stepsize <= step_TOL:
                print('Quasi_Newton Algorithm stagnated:  stepsize tolerance violated.')
            if it >= Maxit:
                print('Quasi-Newton Maxit exceeded:  maximum step count violated.')

            u_vec = unew_vec
        return u_vec

class Prox_NoLip_image_l1_at_q(ProxOperator, ADM_Context):
    """
        Prox operator for the NoLips experiment with l1-penalized quadratic constriants 
    """
    def __init__(self, experiment):
        # because of multiple inheritance we call explicitely
        # the __init__ method of the two parent classes 
        ProxOperator.__init__(self, experiment)
        ADM_Context.__init__(self, experiment)
        if hasattr(experiment, 'A_tens'):
            self.A_tens = experiment.A_tens
        else:
            self.A_tens = 0
        if hasattr(experiment, 'b_mat'):
            self.b_mat = experiment.b_mat
        else:
            self.b_mat = 0
        if hasattr(experiment, 'c_vec'):
            self.c_vec = experiment.c_vec
        else:
            self.c_vec = 0
        if hasattr(experiment, 'Lip_grad_max'):
            self.Lip_grad_max = experiment.Lip_grad_max
        else:
            self.Lip_grad_max = 1.0
        if hasattr(experiment, 'image_scaling'):
            self.image_scaling = experiment.image_scaling
        else:
            self.image_scaling = 1.0
        if hasattr(experiment, 'augmentation_scaling'):
            self.augmentation_scaling = experiment.augmentation_scaling
        else:
            self.augmentation_scaling = 1.0


    def eval(self, u):
        """
        Computes the prox of the l1-norm in the image of another mapping Q:dim(u)-> m. 

        Had to write this in an odd way.  Formally, the ADM method
        for the NoLips problem structure 
        works on 2 blocks of variables, (x,y) with different 
        dimensions.  In the fixed point interpretation of this
        the ADM algorithm is written as the composition of two operators
        T_1T_2, each self-mappings on the domain of x.  For monitoring 
        purposes, I keep the (x,y) block structure, but then have to split 
        the T_2 operator between this operator in the image domain
        and the operator in the 
        primal domain.  If that makes any sense.  

        Input
        ----------
        u : cell of vectors, the primal/domain variable dim n, and the 
            image/dual variable dim m...only actually uses the primal variable
        Q : a mapping u-> y of dimension m
        shrinkage_parameter : scaling in the l1-prox
        
        Returns
        -------
        y_new : vector of dim u[1]
        """
        q_xk_vec = Quadratic(self.A_tens, self.b_mat, self.c_vec,u[0])
        shrinkage_parameter = self.augmentation_scaling / self.image_scaling
        y_new = proxoperators.Prox_l1.eval(q_xk_vec, shrinkage_parameter)
        return y_new

class Prox_NoLip_image_linfty_at_q(ProxOperator, ADM_Context):
    """
        Prox operator for the NoLips experiment with l_infty-penalized quadratic constriants
        with a quadratic approximation to the coupling and no Bregman divergence 
        (the Euclidean case) 
    """
    def __init__(self, experiment):
        # because of multiple inheritance we call explicitely
        # the __init__ method of the two parent classes 
        ProxOperator.__init__(self, experiment)
        ADM_Context.__init__(self, experiment)
        if hasattr(experiment, 'A_tens'):
            self.A_tens = experiment.A_tens
        else:
            self.A_tens = 0
        if hasattr(experiment, 'b_mat'):
            self.b_mat = experiment.b_mat
        else:
            self.b_mat = 0
        if hasattr(experiment, 'c_vec'):
            self.c_vec = experiment.c_vec
        else:
            self.c_vec = 0
        if hasattr(experiment, 'Lip_grad_max'):
            self.Lip_grad_max = experiment.Lip_grad_max
        else:
            self.Lip_grad_max = 1.0
        if hasattr(experiment, 'image_scaling'):
            self.image_scaling = experiment.image_scaling
        else:
            self.image_scaling = 1.0
        if hasattr(experiment, 'augmentation_scaling'):
            self.augmentation_scaling = experiment.augmentation_scaling
        else:
            self.augmentation_scaling = 1.0


    def eval(self, u):
        """
        Computes the prox of the l1-norm in the image of another mapping Q:dim(u)-> m. 

        Had to write this in an odd way.  Formally, the ADM method
        for the NoLips problem structure 
        works on 2 blocks of variables, (x,y) with different 
        dimensions.  In the fixed point interpretation of this
        the ADM algorithm is written as the composition of two operators
        T_1T_2, each self-mappings on the domain of x.  For monitoring 
        purposes, I keep the (x,y) block structure, but then have to split 
        the T_2 operator between this operator in the image domain
        and the operator in the 
        primal domain.  If that makes any sense.  

        Input
        ----------
        u : cell of vectors, the primal/domain variable dim n, and the 
            image/dual variable dim m...only actually uses the primal variable
        Q : a mapping u-> y of dimension m
        prox_parameter : scaling in the l_infty-prox
        
        Returns
        -------
        y_new : vector of dim u[1]
        """
        q_xk_vec = Quadratic(self.A_tens, self.b_mat, self.c_vec,u[0])
        prox_parameter = self.augmentation_scaling / self.image_scaling
        y_new = proxoperators.Prox_linfty.eval(q_xk_vec, prox_parameter)
        return y_new

class Prox_ADM_primal_phase_indicator(ProxOperator, ADM_Context):
    """
    Projecting onto physical (object) 
    domain constraints, adapted for the ADM algorithm
    This is an approximate projector onto a ball around
    the data determined by the Kullback-Leibler divergence,
    as appropriate for Poisson noise.  The analysis of such 
    approximate projections was studied in 
    D.R.Luke, "Local Linear Convergence of Approximate 
    Projections onto Regularized Sets", Nonlinear Analysis, 
    75(2012):1531--1546.

    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on Oct 4, 2017.    
    """

    def __init__(self, experiment):
        # because of multiple inheritance we call explicitely
        # the __init__ method of the two parent classes 
        ProxOperator.__init__(self, experiment)
        ADM_Context.__init__(self, experiment)

        if isinstance(experiment.Prox0, str):
            proxClass = getattr(proxoperators, experiment.Prox0)
        else:
            proxClass = experiment.propagator
        self.Prox0 = proxClass(experiment)


    def eval(self, u, prox_index = None):
        """
        Projection subroutine for projecting onto physical (object) 
        domain constraints, adapted for the ADMM algorithm.
            
        Parameters
        ----------
        u : cell
            Input data to be projected
        prox_idx : int, optional
            Index of this prox operator
         
        Returns
        -------
        xprime : cell
            The projection
        """
        m1, n1, p1, _q1 = size_matlab(u[0])
        k2 = self.product_space_dimension
        get, _set, _data_shape = accessors(u[1], self.Nx, self.Ny, k2)
           
        eta = self.lmbda

        xprime = zeros_like(u[0])

        if m1 > 1 and n1 > 1 and p1 == 1:
            FFT = lambda u: fft2(u)
            IFFT = lambda u: ifft2(u)
        elif m1 == 1 or n1 == 1:
            FFT = lambda u: fft(u)
            IFFT = lambda u: ifft(u)
        else:
            raise NotImplementedError('Error Prox_primal_ADMM_indicator: phase reconstruction for 3D signals not implemented')

        for j in range(k2):
       
            if self.farfield:
                if self.fresnel_nr is not None and self.fresnel_nr[j] > 0:
                        xprime += (self.Nx*self.Ny*2*pi) * IFFT(get(u[1],j) - get(u[2],j)/eta) / self.fresnel_nr[j]
                elif self.FT_conv_kernel is not None:
                        xprime += conj(self.FT_conv_kernel[j]) * IFFT(get(u[1],j) - get(u[2],j)/eta) * (self.Nx*self.Ny)
                elif self.masks is not None:
                        xprime += (self.Nx*self.Ny*2*pi) * IFFT(get(u[1],j) - get(u[2],j)/eta) / get(self.masks,j)
                else:
                        xprime += (self.Nx*self.Ny*2*pi) * IFFT(get(u[1],j) - get(u[2],j)/eta)
            else:  # near field
                if self.beam is not None:
                        xprime +=  IFFT(conj(self.FT_conv_kernel[j]) * FFT(get(u[1],j) - get(u[2],j)/eta) * get(self.magn,j)) / get(self.beam,j)

        # now project onto the constraints on the primal block
        xprime = self.Prox0.eval(xprime/k2)
        return xprime

class Prox_ADM_Approx_Pphase_FreFra_Poisson(ProxOperator, ADM_Context):
    """
    Prox operator  for computing the update to the auxilliary
    variables in the ADM scheme subject to constraints in the 
    image domain (the intensity measurements in the 
    Fresnel/Fraunhofer domain.
    
    The projection onto the data constraints is an approximate
    projector onto a ball around the data determined
    by the Kullback-Leibler divergence, as appropriate
    for Poisson noise.  The analysis of such 
    approximate projections was studied in 
    D.R.Luke, "Local Linear Convergence of Approximate 
    Projections onto Regularized Sets", Nonlinear Analysis, 
    75(2012):1531--1546.

    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on Oct 4, 2017.
    """

    def __init__(self, experiment):
        ProxOperator.__init__(self, experiment)
        ADM_Context.__init__(self, experiment)
        self.data = experiment.data
        self.data_sq = experiment.data_sq
        self.data_zeros = experiment.data_zeros
        self.data_ball = experiment.data_ball
        self.TOL2 = experiment.TOL2


    def eval_helper(self, u, j, proj_scale=1.0, proj_const=0.0):
        
        m1, n1, p1, _q1 = size_matlab(u)
        if p1 > 1:
            raise NotImplementedError('Error Approx_Pphase_FreFra_ADMM_Poisson: phase reconstruction for 3D signals not implemented')

        if m1 > 1 and n1 > 1:
            FFT = lambda u: fft2(u)
            IFFT = lambda u: ifft2(u)
        else:
            FFT = lambda u: fft(u)
            IFFT = lambda u: ifft(u)
        if self.farfield:
            if self.fresnel_nr is not None and self.fresnel_nr[j] > 0:
                u_hat = -1j*self.fresnel_nr[j]/(self.Nx*self.Ny*2*pi)*FFT(u-self.illumination[j]) + self.FT_conv_kernel[j]
            elif self.FT_conv_kernel is not None:
                u_hat = FFT(self.FT_conv_kernel[j]*u) / (self.Nx*self.Ny)
            else:
                u_hat = FFT(u) / (self.Nx*self.Ny)
        else: # near field
            if self.beam is not None:
                u_hat = IFFT(self.FT_conv_kernel[j]*FFT(u*self.beam[j]))/self.magn[j]
            else:
                u_hat = IFFT(self.FT_conv_kernel[j]*FFT(u))/self.magn[j]

        u_hat_sq = real(u_hat * conj(u_hat))
        # Update data_sq to prevent division by zero.
        # Note that data_sq_zeros, as defined below,
        # can be different from self.data_zeros[j]] 
        # (e.g., JWST experiment).
        data_sq = self.data_sq[j].copy()
        data_sq_zeros = where(data_sq == 0)
        data_sq[data_sq_zeros] = 1

        tmp = u_hat_sq / data_sq
        data_zeros = self.data_zeros[j]
        tmp[data_zeros] = 1
        u_hat_sq[data_zeros] = 0
        I_u_hat = tmp == 0
        tmp[I_u_hat] = 1
        tmp = log(tmp)
        h_u_hat = real(sum(sum(u_hat_sq*tmp + self.data_sq[j] - u_hat_sq)))
        if h_u_hat >= self.data_ball + self.TOL2:
            u_image = magproj(self.data[j], u_hat*proj_scale + proj_const)
        else:
            u_image = u
        return u_image


    def eval(self, u, prox_idx=None):
        """
        Projection onto the data constraints
            
        Parameters
        ----------
        u : cell
            Image in the physical domain to be projected.
            assumed here to be a cell, the first element of which
            project onto the amplitude constraints

        prox_idx : int, optional
            Index of this prox operator
         
        Returns
        -------
        u_image : cell
            The projection in the same format as u.
        """
        if not isCell(u):
            u_image = self.eval_helper(u, 0)
        else:
            k2 = amax(u[2].shape)
            u_image = Cell(len(u[2]))
            eta = self.lmbda
            for j in range(k2):
                u_image[j] = self.eval_helper(u[0], j, eta, u[2][j])
        return u_image


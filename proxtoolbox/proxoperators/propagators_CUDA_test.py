import unittest
from numpy.testing import assert_array_almost_equal
from numpy.fft import fft, ifft, fft2, ifft2
from numpy import sqrt, pi
import numpy as np
from propagators_CUDA import Propagator_FreFra_Base_CUDA, Propagator_FreFra_CUDA
import cupy as cp

# Assuming Propagator_FreFra_Base and Propagator_FreFra class definitions are included above

class MockExperiment:
    def __init__(self, FT_conv_kernel=None, fresnel_nr=None, farfield=True, Nx=1, Ny=1, illumination=None, magn=1, beam=None, data_sq=None):
        self.FT_conv_kernel = FT_conv_kernel
        self.fresnel_nr = fresnel_nr
        self.farfield = farfield
        self.Nx = Nx
        self.Ny = Ny
        self.illumination = illumination
        self.magn = magn
        self.beam = beam
        self.data_sq = data_sq  # Added the missing attribute


class TestPropagatorFreFra(unittest.TestCase):
    def setUp(self):
        experiment = MockExperiment()
        self.propagator = Propagator_FreFra_CUDA(experiment)
        self.u = cp.array([1, 2, 3])
        self.u_2d = cp.array([[1, 2, 3], [4, 5, 6]])

    def test_eval_default_prox_idx(self):
        """Test eval with default prox_idx (None)."""
        u_hat = self.propagator.eval(self.u).get()  # Convert CuPy array to NumPy array
        expected_u_hat = np.fft.fft(self.u.get())/np.sqrt(self.propagator.Nx*self.propagator.Ny)  # Ensure operations are on NumPy arrays
        assert_array_almost_equal(u_hat, expected_u_hat)
        
    def test_eval_with_prox_idx_far_field(self):
        """Test eval with a specified prox_idx in far field."""
        experiment = MockExperiment(FT_conv_kernel=[cp.array([1.0])], farfield=True)
        self.propagator = Propagator_FreFra_CUDA(experiment)
        u_hat = self.propagator.eval(self.u, prox_idx=0).get()  # Convert CuPy array to NumPy array
        expected_u_hat = np.fft.fft(experiment.FT_conv_kernel[0].get()*self.u.get())/(self.propagator.Nx*self.propagator.Ny)
        assert_array_almost_equal(u_hat, expected_u_hat)

    def test_eval_with_prox_idx_near_field(self):
        """Test eval with a specified prox_idx in near field."""
        experiment = MockExperiment(FT_conv_kernel=[cp.array([1.0])], beam=[cp.array([1.0, 1.0, 1.0])], farfield=False, data_sq=cp.array([1, 2, 3])**2)
        self.propagator = Propagator_FreFra_CUDA(experiment)
        u_hat = self.propagator.eval(self.u, prox_idx=0).get()  # Convert CuPy array to NumPy array
        expected_u_hat = np.fft.ifft(experiment.FT_conv_kernel[0].get()*np.fft.fft(self.u.get()*experiment.beam[0].get()))/experiment.magn
        assert_array_almost_equal(u_hat, expected_u_hat)

    def test_eval_2d_input(self):
        """Test eval with 2D input array."""
        u_hat = self.propagator.eval(self.u_2d).get()  # Convert CuPy array to NumPy array
        expected_u_hat = np.fft.fft2(self.u_2d.get())/np.sqrt(self.propagator.Nx*self.propagator.Ny)
        assert_array_almost_equal(u_hat, expected_u_hat)

# Add more test cases as needed

if __name__ == '__main__':
    unittest.main()

from proxoperator import ProxOperator
from proxoperator import magproj
import numpy as np


def test_ProxOperator():
    # prox_operator = ProxOperator(experiment)
    # assert prox_operator.eval(u, prox_parameter, prox_idx) == "Not implemented"
    pass

def test_magproj():
    constr = np.array([1, 2, 3])
    u = np.array([1, 2, 3])
    assert np.allclose(magproj(constr, u), np.array([1, 2, 3]))
    print(magproj(constr, u))
    constr = np.array([1, 2, 3])
    u = np.array([1, 2, 4])
    assert np.allclose(magproj(constr, u), np.array([1, 2, 3]))
    print(magproj(constr, u))

# run test upon call
test_ProxOperator()
test_magproj()
print("All tests passed!")
from proxtoolbox.proxoperators.proxoperator import ProxOperator

import numpy as np


__all__ = ['MC_prox_nuc']

class MC_prox_nuc(ProxOperator):
    """
    Class for matrix completion experiment with nuclear norm minimization.

    Written by Maximilian Seeber, 
    Universität Göttingen, 2022.

    """
    def __init__(self, experiment):
        super().__init__(experiment)
        self.gamma = experiment.gamma

    def eval(self, u):
        n_1, n_2 = u.shape
        U, sigma, V = np.linalg.svd(u)
        sigma = sigma - self.gamma
        sigma[sigma < 0] = 0
        n = min(n_1, n_2)
        E = np.zeros((n_1, n_2))
        E[:n, :n] = np.diag(sigma)
        return U @ E @ V
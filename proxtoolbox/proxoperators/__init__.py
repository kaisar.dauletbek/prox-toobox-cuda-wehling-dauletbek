# -*- coding: utf-8 -*-
"""
This package contains the ProxOperator abstract class and various concrete classes.
"""

from .proxoperator import *
from .Prox_product_space import *
from .dQuadratic import *
from .HQuadratic import *
from .Approx_Pphase_FreFra_Poisson import *
from .Approx_Pphase_FreFra_Poisson_cuda import *
from .P_CDP import *
from .P_avg import *
from .P_diag import *
from .P_amp_support import *
from .P_SP import *
from .P_Amod import *
from .P_Sparsity import *
from .P_Sparsity_cuda import *
from .Prox_Sparsity import *
from .Prox_lp import *
from .Prox_l1_at_Q import *
from .P_M import *
from .propagators import *
from .ptychographyProx import *
from .sudokuProx import *
from .CT_prox import *
from .ADMM_prox import *
from .ADM_prox import *
from .Linearized_phaseret_object import *
from .Regularized_phaseret_object import *
from .Approx_Pphase_JWST_Wirt import *
from .P_CDP_ADMM import *
from .P_S import *
from .P_CDP_cyclic import *
from .sourceLocProx import *
from .Pphase_phasepack import *
from .P_orthonorm import *
from .P_incoherent import *
from .MC_P_A import *
from .MC_P_M import *
from .MC_prox_nuc import *
from .MC_ALS import *
from .Proj_l1_cmplx import *
from .algo_in_prox import *



import time
from matplotlib import pyplot as plt
import scipy as sp
from proxtoolbox.experiments.experiment import Experiment
import numpy as np

class SumOfLeastSquares_Experiment(Experiment):
    """
    Experiment class for problems of the form
        min  sum_k || A_k x_k + b_k ||^2
        s.t. sum_j | (x_k)_j | <= c_k,   for k=1,...,Nz
    where for k=1,...,Nz
        A_k are complex matrices of shape (Ny,Nx),
        b_k are complex vectors of shape (Ny),
        x_k are complex vectors of shape (Nx) and
        c_k is a non-negative vector of shape (Nz).

    In the visualisation the solution to the unconstrained
    problem computed via the pseudoinverse is shown
    as a comparison.

    Written by Deborah Lepper, 
    Universität Göttingen, 2023.
    """

    def __init__(self, norm_data=1, Nx=8, Ny=12, Nz=100, constBound=np.inf,
                 algorithm='GPM', MAXIT=1000, inner_MAXIT=100, TOL=1e-6,
                 inner_TOL=1e-4, anim=True, anim_step=2, rnd_seed=1234,
                 randomizedAlgo=True, alphaGPM=0.1, betaGPM=0.5, pltLogy=True,
                 inner_algorithm='GPM', inner_iterate_monitor='IterateMonitor',
                 inner_accelerator=None, inner_silent=True, inner_norm_data=1,
                 **kwargs):
        super().__init__(norm_data=norm_data, Nx=Nx, Ny=Ny, Nz=Nz,
                         algorithm=algorithm, MAXIT=MAXIT, TOL=TOL,
                         anim=anim, anim_step=anim_step, rnd_seed=rnd_seed,
                         **kwargs)
        
        self.alphaGPM = alphaGPM
        self.betaGPM = betaGPM
        self.randomizedAlgo = randomizedAlgo
        self.pltLogy = pltLogy
        self.constBound = constBound

        self.fun_vals = []
        self.const_vals = []
        self.norm_grad_vals = []

        self.inner_algorithm = inner_algorithm
        self.inner_iterate_monitor = inner_iterate_monitor
        self.inner_accelerator = inner_accelerator
        self.inner_silent = inner_silent
        self.inner_norm_data = inner_norm_data
        self.inner_MAXIT = inner_MAXIT
        self.inner_TOL = inner_TOL

        self.inner_proxOperators = []
        self.inner_productProxOperators = []
        self.inner_dBregman_potential = None


    @staticmethod
    def getDefaultParameters():
        defaultParams = {
            'experiment_name': 'l1-constrained sum of complex least squares',
            'rnd_seed': 1234,
            'Nx': 8,
            'Ny': 12,
            'Nz': 50,
            'constBound': np.inf,
            'algorithm': 'GPM',
            'alphaGPM': 0.1,
            'betaGPM': 0.5,
            'randomizedAlgo': True,
            'inner_algorithm': 'GPM',
            'inner_iterate_monitor': 'IterateMonitor',
            'inner_accelerator': None,
            'inner_silent': True,
            'inner_normData': 1,
            'normData': 1,
            'MAXIT': 1000,
            'TOL': 1e-6,
            'inner_TOL': 1e-4,
            'anim': True,
            'anim_step': 2,
            'pltLogy': True
        }
        return defaultParams


    def loadData(self):
        """
        Load/create matrices and error vectors for least squares,
        a vector containing the bounds for the l1-constraints and
        the initial iterate.
        To use GPM and BCD algorithm, the objectives, their differentials
        and constraint functions are created.
        """
        
        print('load random data of shape',(self.Nz,self.Ny,self.Nx))
        
        # complex matricies for least squares. For each of the Nz summands there is one (Ny,Nx)-matrix.
        A = np.zeros((self.Nz,self.Ny,self.Nx),dtype='complex')
        
        # complex error vectors. For each of the Nz summand there is one vector of shape (Nx)
        b = np.zeros((self.Nz,self.Ny),dtype='complex')

        # fill A and b
        for k in range(self.Nz):
            A[k] = np.random.random((self.Ny,self.Nx))*10-5 + (np.random.random((self.Ny,self.Nx))*10-5)*1j
            b[k] = np.random.random(self.Ny)*10-5   + (np.random.random(self.Ny)*10-5)*1j
        
        # upper bounds for l1-norms (real and non-negative); take for each constraint the same bound for simplicity
        self.constBounds = self.constBound*np.ones(self.Nx)

        # compute pseudoinverse as the optimal unconstrained solution for comparison
        Atmp = sp.linalg.block_diag(*[A[k] for k in range(self.Nz)])
        btmp = np.hstack([b[k] for k in range(self.Nz)])
        try:
            self.pinv = np.linalg.inv(np.conj(Atmp).T@Atmp)@np.conj(Atmp).T@btmp
        except np.linalg.LinAlgError as err:
            if 'Singular matrix' in str(err):
                print('Pseudoinverse could not be computed, since A ist not invertable.')
                self.pinv = np.zeros(self.Nx*self.Nz)
            else:
                raise

        # objective function
        self.f = lambda x: np.sum([np.linalg.norm(A[k]@x[k*self.Nx:(k+1)*self.Nx] - b[k])**2 for k in range(self.Nz)])
        # gradient of the k-th summand of the objective
        df_k = lambda x,k : np.conj(A[k]).T @ (A[k]*x[k*self.Nx:(k+1)*self.Nx] - b[k].reshape((-1,1))) if self.Nx == 1 else np.conj(A[k]).T @ (A[k]@x[k*self.Nx:(k+1)*self.Nx] - b[k])
        # gradient of the objective function
        self.df = lambda x: np.hstack([df_k(x,k) for k in range(self.Nz)])

        def inner_f(x,blockNo):
            # just need the data for the blockNo-th block 
            XX = A[:,:,blockNo].reshape((self.Nz,self.Ny,1))
            
            # new error vector depends on all other x values except those for the blockNo-th block
            YY = np.array([b[h] - A[h][:,:blockNo]@x[h*self.Nx:h*self.Nx+blockNo]
                           - A[h][:,blockNo+1:]@x[h*self.Nx+blockNo+1:(h+1)*self.Nx]
                           for h in range(self.Nz)])
            
            return lambda u: np.sum([np.linalg.norm(XX[k]*u[k] - YY[k].reshape((-1,1)))**2 for k in range(self.Nz)])
        
        def inner_df(x,blockNo):
            XX = A[:,:,blockNo].reshape((self.Nz,self.Ny,1))
            
            # new error vector depends on all other x values except those for the blockNo-th block
            YY = np.array([b[h] - A[h][:,:blockNo]@x[h*self.Nx:h*self.Nx+blockNo]
                           - A[h][:,blockNo+1:]@x[h*self.Nx+blockNo+1:(h+1)*self.Nx]
                           for h in range(self.Nz)])
            
            return lambda u: np.concatenate([np.conj(XX[k]).T @ (XX[k]*u[k] - YY[k].reshape((-1,1)))
                                        for k in range(self.Nz)]).flatten()
        
        self.inner_f = inner_f
        self.inner_df = inner_df
        self.inner_constBounds = self.constBounds

        # starting iterate
        self.u0 = np.zeros(self.Nz*self.Nx, dtype='complex')


        # fill masks for constraints. each mask filters the x-components for one constraint.
        self.constMasks = []
        for i in range(self.Nx):
            tmp = np.zeros(self.Nz*self.Nx, dtype=bool)
            tmp[i::self.Nx] = 1
            self.constMasks.append(tmp.copy())

        # fill masks for blocks for the BCD. Each mask filters the x-components for one block.
        # each constraint represents one block, so they are equal
        self.blockMasks = self.constMasks
        self.numBlocks = self.Nx
            
        # constraint evaluation
        self.g = lambda x: np.array([np.sum(np.abs(x[mask])) - self.constBounds[i] for i,mask in enumerate(self.constMasks)])


    def setupProxOperators(self):
        """
        Determine the prox operators to be used for this experiment.
        Implemented for the GPM and the BCD algorithms.
        """

        if self.algorithm_name == 'GPM':
            self.proxOperators = ['Proj_l1_cmplx_multi']
        elif self.algorithm_name == 'BCD':
            if self.inner_algorithm != 'GPM':
                raise NotImplementedError("This experiment with algorithm 'BCD' is just implemented with the inner algorithm 'GPM'.")
            self.proxOperators = ['algo_in_prox']
            self.inner_proxOperators = ['Proj_l1_cmplx']
        else:
            raise NotImplementedError("This experiment is just implemented with the algorithms 'GPM' and 'BCD'.")


    def show(self):
        """
        Generate graphical output from the solution and print some statistical results.
        """

        u = self.output['u']
        numIters = len(self.output['stats']['changes'])

        print()
        print(f'     x    | \t  f(x) \t\t| \tmax(g(x)) \t|      norm(df(x))')
        print(f' ----------------------------------------------------------------------------')
        print(f'    x0    | \t {self.f(self.u0):.4f} \t| \t {np.max(self.g(self.u0)):3.3f}   \t|\t {np.linalg.norm(self.df(self.u0)):.3f}')
        print(f'    xn    | \t {self.f(u):.4f} \t| \t {np.max(self.g(u)):3.3f}   \t|\t {np.linalg.norm(self.df(u)):.3f}')
        print(f'   pinv   | \t {self.f(self.pinv):.4f} \t| \t {np.max(self.g(self.pinv)):3.3f}   \t|\t {np.linalg.norm(self.df(self.pinv)):.3f}')


        plt.figure('Changes in the argument, '+self.algorithm_name+' on '+self.experiment_name,
                   figsize=(self.figure_width,self.figure_height),
                   dpi=self.figure_dpi)
        plt.title('Changes in $x$')
        plt.plot(self.output['stats']['changes'])
        if self.pltLogy:
            plt.yscale('log')
        plt.grid(True,'both')
        plt.xlabel('iterations')

        if self.anim:
            self.fun_vals = np.array(self.fun_vals)
            self.const_vals = np.array(self.const_vals)
            self.norm_grad_vals = np.array(self.norm_grad_vals)

            plt.figure('Function values, '+self.algorithm_name+' on '+self.experiment_name,
                    figsize=(self.figure_width,self.figure_height),
                    dpi=self.figure_dpi)
            plt.title('Function values of the objective')
            plt.plot(np.arange(numIters+1,step=self.anim_step),self.fun_vals)
            plt.grid(True,'both')
            plt.xlabel('iterations')

            plt.figure('Last function values, '+self.algorithm_name+' on '+self.experiment_name,
                    figsize=(self.figure_width,self.figure_height),
                    dpi=self.figure_dpi)
            plt.title('Function values of the objective')
            plt.plot(np.arange(numIters+1,step=self.anim_step)[np.abs(self.fun_vals-self.fun_vals[-1])<=10],
                    self.fun_vals[np.abs(self.fun_vals-self.fun_vals[-1])<=10])
            plt.yscale('log')
            plt.grid(True,'both')
            plt.xlabel('iterations')

            if self.constBound < np.inf:
                plt.figure('Constraint values, '+self.algorithm_name+' on '+self.experiment_name,
                        figsize=(self.figure_width,self.figure_height),
                        dpi=self.figure_dpi)
                plt.title('Values of the constaints')
                plt.plot(np.arange(numIters+1,step=self.anim_step),
                        self.const_vals,
                        label=['constraint '+str(i) for i in range(self.Nx)])
                if self.pltLogy:
                    plt.yscale('log')
                plt.legend()
                plt.grid(True,'both')
                plt.xlabel('iterations')

            plt.figure('Gradient values, '+self.algorithm_name+' on '+self.experiment_name,
                    figsize=(self.figure_width,self.figure_height),
                    dpi=self.figure_dpi)
            plt.title('Norms of gradients')
            plt.plot(np.arange(numIters+1,step=self.anim_step), self.norm_grad_vals)
            if self.pltLogy:
                plt.yscale('log')
            plt.grid(True,'both')
            plt.xlabel('iterations')

            plt.figure('Last gradient values, '+self.algorithm_name+' on '+self.experiment_name,
                    figsize=(self.figure_width,self.figure_height),
                    dpi=self.figure_dpi)
            plt.title('Norms of gradients')
            plt.plot(np.arange(numIters+1,step=self.anim_step)[np.abs(self.norm_grad_vals-self.norm_grad_vals[-1])<=10],
                     self.norm_grad_vals[np.abs(self.norm_grad_vals-self.norm_grad_vals[-1])<=10])
            if self.pltLogy:
                plt.yscale('log')
            plt.grid(True,'both')
            plt.xlabel('iterations')

        plt.show()
    

    def animate(self, alg):
        """
        Display animation. This method is called
        after each iteration of the algorithm `alg` if
        data member `anim` is set to True.
        This method also saves some data like function
        values of these iterates.
 
        Parameters
        ----------
        alg : instance of Algorithm class
            Algorithm that is running.
        """

        if alg.u_new is not None:
            title = self.algorithm_name+f' iteration: {alg.iter} \n weights/entries of the argument $x$ \n$f(x_{{{alg.iter}}}) = {self.f(alg.u_new):.2f}$ and $\max(g(x_{{{alg.iter}}})) = {np.max(self.g(alg.u_new)):.2f}$'
            self.animateFigure(np.concatenate([np.real(alg.u_new),np.imag(alg.u_new)]),
                               title,
                               target=np.concatenate([np.real(self.pinv),
                                                      np.imag(self.pinv)]),
                               label_target=f'pseudoinverse, $f(x_{{pinv}}) = {self.f(self.pinv):.2f}$, $\max(g(x_{{pinv}}))={np.max(self.g(self.pinv)):.1f}$')
            self.fun_vals.append(self.f(alg.u_new))
            self.const_vals.append(self.g(alg.u_new))
            self.norm_grad_vals.append(np.linalg.norm(self.df(alg.u_new)))
            time.sleep(0.005)
        else:
            title = self.algorithm_name+f' iteration: 0 \n weights/entries of the argument $x$ \n$f(x_0) = {self.f(self.u0):.2f}$ and $\max(g(x_0)) = {np.max(self.g(self.u0)):.2f}$'
            self.animateFigure(np.concatenate([np.real(self.u0),np.imag(self.u0)]),
                               title,
                               target=np.concatenate([np.real(self.pinv),
                                                      np.imag(self.pinv)]),
                               label_target=f'pseudoinverse, $f(x_{{pinv}}) = {self.f(self.pinv):.2f}$, $\max(g(x_{{pinv}}))={np.max(self.g(self.pinv)):.1f}$')
            self.fun_vals.append(self.f(self.u0))
            self.const_vals.append(self.g(self.u0))
            self.norm_grad_vals.append(np.linalg.norm(self.df(self.u0)))
            time.sleep(0.005)
        
    
    def animateFigure(self, data, title, target=None, label_target=None):
        """
        Display animated figure. This is a helper method 
        that is called by the animate() method to display
        the animated iterates (drawn using plot('.')).

        This method takes care of creating or updating the
        figure
 
        Parameters
        ----------
        data    : 1D scalar array
            The iterate to plot.
        title : str
            The title of the figure.
        target : 1D scalar array (optional)
            The target iterate for comparison.
        label_target : string (optional)
            The name of the target.
        """

        # create a figure if there never way one or if it was closed
        if self.anim_figure == None:
            self.createFigure(data,target,label_target)
        
        # rescale y-axis
        self.min_val = min(np.min(data), self.min_val)
        self.max_val = max(np.max(data), self.max_val)
        tmp = abs(self.max_val-self.min_val)
        if tmp < 1e-12:
            space = 1e-10
        else:
            space = tmp * 0.05
        self.anim_ax.set_ylim((self.min_val-space, self.max_val+space))

        # set title and data and draw it
        self.anim_ax.set_title(title)
        self.anim_data[0].set_data(np.arange(len(data)),data)
        self.anim_figure.canvas.draw_idle()
        self.anim_figure.canvas.flush_events()

    
    def createFigure(self, data, target=None, label_target=None):
        """
        Create figure object. This is a helper method 
        that is called by the animateFigure() method to
        create a figure where to draw the animated iterates
        (drawn using plot('.')).
 
        Parameters
        ----------
        data    : 1D scalar array
            The iterate to plot.
        target : 1D scalar array (optional)
            The target iterate for comparison.
        label_target : string (optional)
            The name of the target.
        """

        # just create a figure if there is none
        if self.anim_figure == None:
            plt.ion()
            self.anim_figure = plt.figure('Animation of experiment '+self.experiment_name,
                                          figsize=(self.figure_width, self.figure_height),
                                          dpi=self.figure_dpi)
            # call parents method if the drawing window was closed
            self.anim_figure.canvas.mpl_connect('close_event', self.onAnimationWindowClosing)

            # plot data
            self.anim_ax = self.anim_figure.add_subplot(111)
            self.anim_data = self.anim_ax.plot(data,'.')
            self.anim_ax.grid()

            self.min_val = np.min(data)
            self.max_val = np.max(data)

            # plot target if it is given
            if type(target) != type(None):
                self.min_val = min(np.min(target), self.min_val)
                self.max_val = max(np.max(target), self.max_val)

                if label_target != None:
                    self.anim_ax.plot(target,'*',label=label_target, ms=self.figure_dpi/40)
                    self.anim_ax.legend()
                else:
                    # plot target data without label
                    self.anim_ax.plot(target,'*',ms=self.figure_dpi/40)
import matplotlib.pyplot as plt
import numpy as np

from proxtoolbox.experiments.orbitaltomography.planar_molecule import PlanarMolecule
from proxtoolbox.utils.orbitaltomog import shifted_fft, fourier_interpolate
from proxtoolbox.utils.visualization.complex_field_visualization import complex_to_rgb

#for downloading data
import proxtoolbox.utils.GetData as GetData
from proxtoolbox.utils.GetData import datadir

class DegenerateOrbital(PlanarMolecule):
    @staticmethod
    def getDefaultParameters():
        defaultParams = {
            'experiment_name': '2D ARPES',
            'data_filename': datadir/'OrbitalTomog'/'2020_10_27_coronene_Homo1+2_ARPES_2e6counts_corrected_80x80.tif',
            'from_intensity_data': True,
            'object': 'real',
            'degeneracy': 2,  # Number of degenerate states to reconstruct
            'constraint': 'sparse real',
            'sparsity_parameter': 40,
            'use_sparsity_with_support': True,
            'threshold_for_support': 0.05,
            'support_filename': None,
            'Nx': None,
            'Ny': None,
            'Nz': 1,
            'MAXIT': 500,
            'TOL': 1e-10,
            'lambda_0': 0.85,
            'lambda_max': 0.50,
            'lambda_switch': 50,
            'data_ball': .999826,
            'TOL2': 1e-15,
            'diagnostic': True,
            'algorithm': 'CP',
            'iterate_monitor_name': 'FeasibilityIterateMonitor',  # 'IterateMonitor',  #
            'rotate': False,
            'verbose': 1,
            'graphics': 1,
            'interpolate_and_zoom': True,
            'debug': True,
            'progressbar': None
        }
        return defaultParams

    def __init__(self, **kwargs):
        super(DegenerateOrbital, self).__init__(**kwargs)
        self.degeneracy = kwargs.pop("degeneracy", 2)

    def loadData(self):
        """
        Load data and set in the correct format for reconstruction
        Parameters are taken from experiment class (self) properties, which must include::
            - data_filename: str, path to the data file
            - from_intensity_data: bool, if the data file gives intensities rather than field amplitude
            - support_filename: str, optional path to file with object support
            - use_sparsity_with_support: bool, if true, use a support before the sparsity constraint.
                The support is calculated by thresholding the object autocorrelation, and dilate the result
            - threshold_for_support: float, in range [0,1], fraction of the maximum at which to threshold when
                determining support or support for sparsity
        """

        # make sure input data can be found, otherwise download it
        GetData.getData('OrbitalTomog')
        
        super(DegenerateOrbital, self).loadData()

    def createRandomGuess(self):
        """
        Taking the measured data, add a random phase and calculate the resulting iterate guess
        """
        self.u0 = np.array([self.data * np.exp(1j * 2 * np.pi * np.random.random_sample(self.data.shape))
                            for i in range(self.degeneracy)])
        self.u0 = shifted_fft(self.u0, axes=(-1, -2))

    def setupProxOperators(self):
        """
        Determine the prox operators to be used based on the given constraint.
        This method is called during the initialization process.

        sets the parameters:
        - self.proxOperators
        - self.propagator and self.inverse_propagator
        """
        # Select the right real space operator sparsity-based proxoperators
        self.proxOperators.append('P_Sparsity_real_incoherent')

        # Apply orthonormality constraint
        self.proxOperators.append('P_orthonorm')

        # Modulus proxoperator (normally the second operator)
        self.proxOperators.append('P_M_incoherent')
        self.propagator = 'PropagatorFFT2'
        self.inverse_propagator = 'InvPropagatorFFT2'

        self.nProx = len(self.proxOperators)

    def plotInputData(self):
        """Quick plotting routine to show the data, initial guess and the sparsity support"""
        fig, ax = plt.subplots(2, self.degeneracy + 1, figsize=(12, 7))
        im = ax[0][0].imshow(self.data)
        plt.colorbar(im, ax=ax[0])
        ax[0][0].set_title("Photoelectron spectrum")
        if self.sparsity_support is not None:
            im = ax[0][-1].imshow(self.sparsity_support, cmap='gray')
            # plt.colorbar(im, ax=ax[2])
            ax[0][-1].set_title("Sparsity support")
        for ii in range(self.degeneracy):
            im = ax[1][ii].imshow(complex_to_rgb(self.u0[ii]))
            plt.colorbar(im, ax=ax[1][ii])
            ax[1][ii].set_title("Degenerate orbit %d" % ii)
        ax[1][-1].imshow(np.sum(abs(self.u0) ** 2, axis=0))
        ax[1][-1].set_title("Local density of states")
        plt.show()

    def show(self, **kwargs):
        """
        Create basic result plots of the phase retrieval procedure
        """
        super(PlanarMolecule, self).show()

        self.output['u1'] = self.algorithm.prox1.eval(self.algorithm.u)
        self.output['u2'] = self.algorithm.prox2.eval(self.algorithm.u)

        figsize = kwargs.pop("figsize", (12, 3))
        for i, operator in enumerate(self.algorithm.proxOperators):
            operator_name = self.proxOperators[i].__name__
            f = self.plot_guess(operator.eval(self.algorithm.u),
                                name="%s satisfied" % operator_name,
                                show=False,
                                interpolate_and_zoom=self.interpolate_and_zoom,
                                figsize=figsize)
            self.output['plots'].append(f)

        # f1 = self.plot_guess(self.output['u1'], name='Best approximation: physical constraint satisfied', show=False)
        # f2 = self.plot_guess(self.output['u2'], name='Best approximation: Fourier constraint satisfied', show=False)
        # prop = self.propagator(self)
        # u_hat = prop.eval(self.algorithm.prox1.eval(self.algorithm.u))
        # h = self.plot_guess(u_hat, show=False, name="Fourier domain measurement projection")

        # self.output['plots'].append(f1)
        # self.output['plots'].append(f2)
        # self.output['plots'].append(h)
        plt.show()

    # def saveOutput(self, **kwargs):
    #     super(PlanarMolecule, self).saveOutput(**kwargs)

    def plot_guess(self, u, name=None, show=True, interpolate_and_zoom=False, figsize=(12, 3)):
        """"Given a list of fields, plot the individual fields and the combined intensity"""
        prop = self.propagator(self)  # This is not a string but the indicated class itself, to be instantiated
        u_hat = prop.eval(u)
        fourier_intensity = np.sqrt(np.sum(abs(u_hat) ** 2, axis=0))
        if interpolate_and_zoom:
            u_show = interp_zoom_field(u)
        else:
            u_show = u
        fig, ax = plt.subplots(1, len(u) + 2, figsize=figsize, num=name)
        for ii in range(self.degeneracy):
            im = ax[ii].imshow(complex_to_rgb(u_show[ii]))
            ax[ii].set_title("Degenerate orbit %d" % ii)
        im = ax[-2].imshow(np.sum(abs(u_show) ** 2, axis=0))
        ax[-2].set_title("Local density of states")
        # plt.colorbar(im, ax=ax[-2], shrink=0.7)
        im = ax[-1].imshow(fourier_intensity)
        ax[-1].set_title("Fourier domain intensity")
        # plt.colorbar(im, ax=ax[-1], shrink=0.7)
        plt.tight_layout()
        if show:
            plt.show()
        return fig


def interp_zoom_field(u, interpolation=2, zoom=0.5):
    """
    interpolate a field and zoom in to the center
    """
    nt, ny, nx = u.shape
    zmy = int(ny * zoom) // 2
    zmx = int(nx * zoom) // 2
    zoomed = u[:, zmy:ny - zmy, zmx:nx - zmx]

    interpolated = np.array([fourier_interpolate(u_i, factor=interpolation) for u_i in zoomed])

    return interpolated

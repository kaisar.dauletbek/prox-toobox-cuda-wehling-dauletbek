import cupy as cp
import numpy as np
from cupy.fft import fftshift, ifft2, fft2
from cupy import zeros, unravel_index, argmax, random, linalg
from proxtoolbox.experiments.phase.phaseExperiment import PhaseExperiment
from proxtoolbox.utils.graphics import addColorbar
from proxtoolbox.utils.ZeroPad import ZeroPad
from proxtoolbox.utils.cell_cuda import CudaCell, isCudaCell

#for downloading data
import proxtoolbox.utils.GetData as GetData
from proxtoolbox.utils.GetData import datadir

import matplotlib.pyplot as plt
from matplotlib.pyplot import subplots, show, figure


class SIRIUS_CDI_Experiment_Cuda(PhaseExperiment):
    """
    SIRIUS_CDI experiment class with CuPy parallelization for GPU acceleration.
    """

    @staticmethod
    def getDefaultParameters():
        defaultParams = {
            'experiment_name': 'SIRIUS_CDI',
            'object': 'nonnegative',
            'constraint': 'sparse support',
            'sparsity_parameter': 300,
            'Nx': 256,
            'Ny': 256,
            'Nz': 1,
            'sets': 1,
            'farfield': True,
            'MAXIT': 6000,
            'TOL': 1e-8,
            'lambda_0': 0.5,
            'lambda_max': 0.50,
            'lambda_switch': 30,
            'data_ball': .999826e-8,
            'diagnostic': True,
            'iterate_monitor_name': 'phase_FeasibilityIterateMonitor',
            'rotate': False,
            'verbose': 0,
            'graphics': 1,
            'anim': False,
            'debug': True
        }
        return defaultParams

    def __init__(self, warmup_iter=0, **kwargs):
        """
        Initialize the SIRIUS_CDI_Experiment_Cuda class.
        """
        super(SIRIUS_CDI_Experiment_Cuda, self).__init__(**kwargs)
        self.warmup_iter = warmup_iter
        self.magn = None
        self.farfield = None
        self.data_zeros = None
        self.mask_idx = None
        self.fmask = None
        self.support_idx = None
        self.abs_illumination = None
        self.supp_phase = None

    def loadData(self):
        """
        Load CDI dataset and create the initial iterate.
        """
        # Load data as before
        f = cp.load(datadir / 'Phase' / 'diff2D_2_2796_2796.npy')
        dp = f[0, :, :]

        orig_res = max(dp.shape[0], dp.shape[1])

        # here we use np and not cp functions because we want the result to be a float and not a cupy array with one element - Till Wehling
        step_up = np.ceil(np.log2(self.Nx) - np.log2(orig_res))                                
        workres = 2**(step_up) * 2**(np.floor(np.log2(orig_res)))

        N = int(workres)

        argmx = unravel_index(argmax(dp), dp.shape)
        Xi = argmx[0] + 1
        Xj = argmx[1] + 1

        Di = N / 2 - (Xi - 1)
        Dj = N / 2 - (Xj - 1)

        Ni = 2 * (Xi + Di * (Di < 0) - 1)
        Nj = 2 * (Xj + Dj * (Dj < 0) - 1)

        tmp = zeros((N, N))
        tmp[int(Di * (Di > 0)):int(Di * (Di > 0) + Ni), int(Dj * (Dj > 0)):int(Dj * (Dj > 0) + Nj)] = \
            dp[int(Xi - Ni / 2) - 1:int(Xi + Ni / 2 - 1), int(Xj - Nj / 2) - 1:int(Xj + Nj / 2 - 1)]
        tmp = cp.roll(tmp, (-1, -1))

        M = fftshift(tmp)
        self.data_zeros = cp.where(M == 0)
        self.mask_idx = cp.where(M == -1)
        self.fmask = cp.ones((N, N))
        self.fmask[self.mask_idx] = 0

        M[self.mask_idx] = 0
        self.dp = fftshift(M)

        support_tightness = 0.5
        DX = cp.ceil((N / 2) * support_tightness)
        S = zeros((N, N))
        S[int(N/2 - 1 - DX) - 1:int(N/2 + 1 + DX), int(N/2 - 1 - DX/2) - 1:int(N/2 + 1 + DX/2)] = 1
        self.magn = 1
        self.farfield = True

        if not self.silent:
            print('Using farfield formula.')
            
        # self.rt_data = CudaCell(M.shape)
        # self.rt_data[0] = M
        self.rt_data = M
        # self.data = CudaCell((1,))
        # self.data[0] = M**2
        self.data = M**2
        self.norm_rt_data = linalg.norm(ifft2(M), 'fro')
        self.support_idx = cp.nonzero(S)
        self.support = S
        self.sets = 2
        self.abs_illumination = S

        if self.initial_guess is None:
            tmp_rnd = (random.rand(N, N)).T
            self.u0 = S * tmp_rnd
            self.u0 = self.u0 / linalg.norm(self.u0, 'fro') * self.norm_rt_data
        else:
            if ~(self.initial_guess.size == M.size):
                tmp = fftshift(fft2(self.initial_guess))
                # print('\n\n\nHUI\n\n\n')
                # print(type(tmp))
                # print('\n\n\nHUI\n\n\n')
                self.u0 = ifft2(fftshift(ZeroPad(cp.asnumpy(tmp))))

    def setupProxOperators(self):
        """
        Determine the prox operators to be used for this experiment.
        """
        # Instead of calling the parent method we set the CUDA version of the prox operator right here
        self.proxOperators = ['P_Sparse_support_cuda']

        self.propagator = 'Propagator_FreFra'
        self.inverse_propagator = 'InvPropagator_FreFra'

        if self.constraint == 'phaselift':
            self.proxOperators.append('P_Rank1')
        elif self.constraint == 'phaselift2':
            self.proxOperators.append('P_rank1_SR')
        else:
            self.proxOperators.append('Approx_Pphase_FreFra_Poisson_cuda')
        self.nProx = self.sets

    def show(self):
        """
        Generate graphical output from the solution.
        """
        f, (ax1, ax2) = subplots(1, 2,
                                 figsize=(self.figure_width, self.figure_height),
                                 dpi=self.figure_dpi)
        # im = ax1.imshow(cp.log10(self.dp + 1e-15))
        im = ax1.imshow(cp.asnumpy(cp.log10(self.dp + 1e-15)))
        addColorbar(im)
        ax1.set_title('Far field data')
        im = ax2.imshow(cp.asnumpy(self.abs_illumination))
        ax2.set_title('Support constraint')
        plt.subplots_adjust(wspace=0.3)
        f.suptitle('SIRIUS_CDI Data')
        plt.savefig("result_figures/figure_123.png")
        super(SIRIUS_CDI_Experiment_Cuda, self).show()


from proxtoolbox.experiments.experiment import Experiment
from proxtoolbox.utils.cell import Cell, isCell
from proxtoolbox.utils.size import size_matlab
from proxtoolbox.utils.graphics import addColorbar
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.pyplot import subplots, show, figure
import numpy as np
from numpy import real, angle, log10
import cupy as cp


class PhaseExperiment(Experiment):
    '''
    Base class for phase experiments. Concrete phase retrieval
    experiments should derive from this class
    '''

    def __init__(self, 
                 distance='far field',
                 farfield=True,
                 rotate=False,
                 **kwargs):
        """
        """
        # call parent's __init__ method
        super(PhaseExperiment, self).__init__(**kwargs)
        # do here any data member initialization
        self.distance = distance
        self.farfield = farfield
        self.rotate = rotate  # tells the iterate monitor to rotate the iterates
        # to a fixed global reference. 
        self.data_sq = None # specific to phase retrieval
        self.norm_data = None # specific to phase retrieval
        self.norm_data_sq = None # specific to phase retrieval
        self.rt_data = None # specific to phase retrieval
        self.norm_rt_data = None # specific to phase retrieval
        self.propagator = None # context specific...only for phase retrieval
        self.inverse_propagator = None # context specific...only for phase retrieval
        self.amplitude = None  # TODO Quick fix for now. Needed for P_amp prox operator - context specific


    def reshapeData(self, Nx, Ny, Nz):
        """
        Reshape data based on the given arguments. This method is called
        during the initialization of the Experiment object, after the call
        to loadData().
        
        Parameters
        ----------
        Nx, Ny, Nz: int
            The new dimensions to be used. 

        Notes
        -----
        TODO: need to revisit this - there shouldn't be any need for arguments
        """

        # reshape and rename the data
        if self.data_sq is None:
            # need to put data into required format
            self.data_sq = self.data
            self.data = self.rt_data
            if self.norm_data is not None:
                self.norm_data_sq = self.norm_data
            self.norm_data = self.norm_rt_data

            # The following lines of code correspond to the Cone_and_Sphere branch. However,
            # the case where the data is a cell is not treated (in Matlab implementation)
            # TODO: Need to check is this is OK.
            if self.data is not None and not isCell(self.data):
                tmp = self.data.shape
                if tmp[0] == 1 or tmp[1] == 1:
                    self.data_sq = self.data_sq.reshape(Nx, Ny)
                    # the prox algorithms work with the square root of the measurement:
                    self.data = self.data.reshape(Nx, Ny)

    def setupProxOperators(self):
        """
        Determine the prox operators to be used for this experiment
        """
        super(PhaseExperiment, self).setupProxOperators()  # call parent's method

        # Algorithm specific initialization
        # The following code should be independent from the actual experiment
        if self.algorithm_name == 'ADMM':
            # There are two main prox mappings, one for each of the first two
            # blocks. These prox mappings may be further decomposed, but that
            # is not handled at this level.
            # Set up Prox0, prox operator used in the primal prox block
            if not hasattr(self, 'Prox0'):
                if self.proxOperators is None:
                    self.Prox0 = None
                else:
                    self.Prox0 = self.proxOperators[0]  # given by previous call to parent

            self.iterate_monitor_name = 'ADMM_IterateMonitor'
            if self.optimality_monitor is None:
                self.optimality_monitor = 'ADMM_phase_indicator_objective'
            self.lagrange_mult_update = 'ADMM_PhaseLagrangeUpdate'
        elif self.algorithm_name == 'AvP2':
            # There are two main prox mappings, one for each of the first two
            # blocks. These prox mappings may be further decomposed, but that
            # is not handled at this level.
            # Set up Prox0, prox operator used in the primal prox block
            if not hasattr(self, 'Prox0'):
                self.Prox0 = self.proxOperators[0]  # given by previous call to parent

            self.iterate_monitor_name = 'AvP2_IterateMonitor'
        elif self.algorithm_name == 'PHeBIE':
            self.iterate_monitor_name = 'PHeBIE_IterateMonitor'
            if self.optimality_monitor is None:
                self.optimality_monitor = 'PHeBIE_phase_objective'
            self.explicit = []
            self.explicit.append('Linearized_phaseret_object')
            self.explicit.append('Regularized_phaseret_object')
        elif self.algorithm_name == 'Wirtinger':
            self.iterate_monitor_name = 'Wirt_IterateMonitor'


    def show(self):
        """
        Generate graphical output from the solution
        """
        u_m = self.output['u_monitor']
        # The goal here is to extract the data that we want to plot
        # One issue is that u_monitor may represent different things.
        # It would be nice to have a description of what u_monitor
        # is to avoid the following guessing process:
        if isCell(u_m):
            u = u_m[0]
            u2 = u_m[len(u_m)-1]
        else:
            u = self.output['u']
            u2 = u_m
        u_plot = self.getPlotData(u)
        u2_plot = self.getPlotData(u2)

        changes = self.output['stats']['changes']
        if 'time' in self.output['stats']:
            time = self.output['stats']['time']
        else:
            time = 999
            
        algo_desc = self.algorithm.getDescription()
        title = "Algorithm: " + algo_desc

        # figure 904
        titles = ["Best approximation amplitude - Physical constraint satisfied",
                  "Best approximation phase - Physical constraint satisfied",
                  "Best approximation amplitude - Fourier constraint satisfied",
                  "Best approximation phase - Fourier constraint satisfied"]

        if u_plot.ndim == 1:
            f = self.createFourGraphFigure(u_plot, u2_plot, titles)
        else:
            f = self.createFourImageFigure(u_plot, u2_plot, titles)
        f.suptitle(title)
        plt.subplots_adjust(hspace = 0.3) # adjust vertical space (height) between subplots (default = 0.2)
        plt.subplots_adjust(wspace = 0.4) # adjust horizontal space (width) between subplots (default = 0.2)
        plt.savefig("result_figures/figure_904.png")

        # figure 900
        if u_plot.ndim == 1:
            f = self.createTwoGraphFigure(u_plot, titles)
        else:
            f = self.createTwoImageFigure(u_plot, titles)
        f.suptitle(title)
        plt.subplots_adjust(hspace = 0.3) # adjust vertical space (height) between subplots (default = 0.2)
        plt.subplots_adjust(wspace = 0.4) # adjust horizontal space (width) between subplots (default = 0.2)
        plt.savefig("result_figures/figure_900.png")
 
        time_str = "{:.{}f}".format(time, 5) # 5 is precision
        label = "Iterations (time = " + time_str + " s)"
        plt.savefig("result_figures/figure_900.png")

        #figure 901
        f = plt.figure(figsize = (self.figure_width, self.figure_height))
        plt.semilogy(changes)
        plt.xlabel(label)
        plt.ylabel('Log of change in iterates')
        f.suptitle(title)
        plt.savefig("result_figures/figure_901.png")

        #figure 902
        if 'gaps' in self.output['stats']:
            gaps = self.output['stats']['gaps']
            f = plt.figure(figsize = (self.figure_width, self.figure_height))
            plt.semilogy(gaps)
            plt.xlabel(label)
            plt.ylabel('Log of the gap distance')
            f.suptitle(title)
        plt.savefig("result_figures/figure_902.png")
        show()

    def getPlotData(self, u):
        """
        Helper function to extract data that we can plot
 
        Parameters
        ----------
        u : Cell or ndarray
            Iterate or monitored data
        """
        if isCell(u):
            u_plot = u[0]
        else:
            u_plot = u
            if self.product_space_dimension != 1 and u.ndim > 1:
                # we may need to refine this based on the info we have
                m, n, _p, _q = size_matlab(u)
                if m == self.product_space_dimension or m == 1:
                    u_plot = u[0,:]
                elif n == self.product_space_dimension or n == 1:
                    u_plot = u[:,0]
                elif u.ndim > 2:
                    u_plot = u[:,:,0]
        return u_plot

    def animate(self, alg):
        """
        Display animation. This method is called
        after each iteration of the algorithm `alg` if
        data member `anim` is set to True.
 
        Parameters
        ----------
        alg : instance of Algorithm class
            Algorithm that is running.
        """
        if alg.u_new is not None:
            u = alg.u_new
        else:  # this the case when called before algorithm runs
            u = alg.u
        if isCell(u):
            u = u[0]
            if isCell(u):
                u = u[0]
    
        m, n, _p, _q = size_matlab(u)
        image = None
        if self.Nx == 1 or self.Ny == 1:
            # This is not an image
            # TODO Not implemented
            raise NotImplementedError('Animation of a plot is not implemented yet')
        else:
            # This is an image
            if m == 1 or n == 1:
                # image is the shape of a column vector
                N = int(np.sqrt(u.shape[0]))
                u = np.reshape(u, (N,N), order='F')
            if self.farfield:
                image = real(u)
                title = "Iteration " + str(alg.iter)
            else:
                image = angle(u)
                title = "Phase of reconstructed object, iteration " + str(alg.iter)

        self.animateFigure(image, title)
   

    def createGraphSubFigure(self, ax, u, title = None):
        ax.plot(u)
        if title is not None:
            ax.set_title(title)
 
    def createImageSubFigure(self, f, ax, u, title = None):
        if isinstance(u, cp.ndarray):
        # Convert CuPy array to NumPy array
            u = cp.asnumpy(u)
        im = ax.imshow(u, cmap='gray')
        addColorbar(im)
        if title is not None:
            ax.set_title(title)
 
    def createFourGraphFigure(self, u, u2, titles):
        f, ((ax1, ax2), (ax3, ax4)) = subplots(2, 2, \
                    figsize = (self.figure_width, self.figure_height),
                    dpi = self.figure_dpi)
        self.createGraphSubFigure(ax1, abs(u), titles[0])
        self.createGraphSubFigure(ax2, real(u), titles[1])
        self.createGraphSubFigure(ax3, abs(u2), titles[2])
        self.createGraphSubFigure(ax4, real(u2), titles[3]) 
        return f

    def createFourImageFigure(self, u, u2, titles):
        f, ((ax1, ax2), (ax3, ax4)) = subplots(2, 2, \
                    figsize = (self.figure_width, self.figure_height),
                    dpi = self.figure_dpi)
        self.createImageSubFigure(f, ax1, abs(u), titles[0])
        self.createImageSubFigure(f, ax2, real(u), titles[1])
        self.createImageSubFigure(f, ax3, abs(u2), titles[2])
        self.createImageSubFigure(f, ax4, real(u2), titles[3])
        return f
       
    def createTwoGraphFigure(self, u, titles):
        f, (ax1, ax2) = subplots(1, 2, \
                    figsize = (self.figure_width, self.figure_height),
                    dpi = self.figure_dpi)
        self.createGraphSubFigure(ax1, abs(u), titles[0])
        self.createGraphSubFigure(ax2, real(u), titles[1])
        return f

    def createTwoImageFigure(self, u, titles):
        f, (ax1, ax2) = subplots(1, 2,
                    figsize = (self.figure_width, self.figure_height),
                    dpi = self.figure_dpi)
        self.createImageSubFigure(f, ax1, log10(abs(u)+1e-10), titles[0])
        self.createImageSubFigure(f, ax2, real(u), titles[1])
        return f


 


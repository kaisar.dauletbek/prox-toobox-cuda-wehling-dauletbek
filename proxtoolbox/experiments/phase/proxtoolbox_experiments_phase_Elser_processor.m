load('../../../InputData/Phase/Elser/data100E')
x=data100E;
datapow=0;
M=max(size(x));
fmag=zeros(M, M/2);
Fmag=zeros(M,M);
for(i=1:M/2)
	j=1;
	datapow=x(i,j)+datapow;
	fmag(i,j)=sqrt(x(i,j))/M;
    	Fmag(i,j) = fmag(i,j);
	for(j=2:M/2)
		datapow=2*x(i,j)+datapow;
		fmag(i,j)=sqrt(x(i,j))/M;
        	Fmag(i,j) = fmag(i,j);
        	if(i>1)
            		Fmag(M-(j-2),M-(i-2)) = fmag(i,j);
        	end
    	end
end
for(i=M/2+1:M)
    	j=1;
	datapow=x(i,j)+datapow;
	fmag(i,j)=sqrt(x(i,j))/M;
    	Fmag(i,j) = fmag(i,j);
	for(j=2:M/2)
		datapow=2*x(i,j)+datapow;
		fmag(i,j)=sqrt(x(i,j))/M;
        	Fmag(i,j) = fmag(i,j);
        	Fmag(j,i) = fmag(i,j);
    	end
end
Fmag(1,M/2:M)=fmag(M/2:M,1);

% Now try this a different way:
Fmag2=zeros(M,M);
fmag2=sqrt(x)/M;
fmag2(:,M/2+1)=0;
Fmag2(:,1:M/2+1)=fmag2;
Fmag2(2:M/2,M/2+2:M)=fmag2(M:-1:M/2+2,M/2-1:-1:1);
Fmag2(M/2+2:M,M/2+2:M)=fmag2(M/2:-1:2,M/2:-1:2);
Fmag2(1,M/2:M)=fmag2(M/2:M,1);
datapow2=sum(sum(Fmag2.^2))*M;

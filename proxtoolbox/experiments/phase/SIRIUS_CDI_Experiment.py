from proxtoolbox.experiments.phase.phaseExperiment import PhaseExperiment
from proxtoolbox.utils.mypoissonrnd import mypoissonrnd
from proxtoolbox.utils.gaussian import gaussian
from proxtoolbox.utils.cell import Cell, isCell
from proxtoolbox.utils.graphics import addColorbar
from proxtoolbox.utils.ZeroPad import ZeroPad


#for downloading data
import proxtoolbox.utils.GetData as GetData
from proxtoolbox.utils.GetData import datadir

import numpy as np
from numpy import exp, sqrt, log2, log10, ceil, floor, unravel_index, argmax, zeros
from scipy.io import loadmat
from numpy.fft import fftshift, ifft2, fft2

import matplotlib.pyplot as plt
from matplotlib.pyplot import subplots, show, figure


class SIRIUS_CDI_Experiment(PhaseExperiment):
    """
    SIRIUS_CDI experiment class
    """

    @staticmethod
    def getDefaultParameters():
        defaultParams = {
            'experiment_name': 'SIRIUS_CDI',
            'object': 'nonnegative',
            'constraint': 'sparse support',
            'sparsity_parameter': 300,
            'Nx': 256,
            'Ny': 256,
            'Nz': 1,
            'sets': 1,
            'farfield': True,
            'MAXIT': 6000,
            'TOL': 1e-8,
            'lambda_0': 0.5,
            'lambda_max': 0.50,
            'lambda_switch': 30,
            'data_ball': .999826e-8,
            'diagnostic': True,
            'iterate_monitor_name': 'phase_FeasibilityIterateMonitor',
            'rotate': False,
            'verbose': 0,
            'graphics': 1,
            'anim': False,
            'debug': True
        }
        return defaultParams

    def __init__(self,
                 warmup_iter=0,
                 **kwargs):
        """
        """
        # call parent's __init__ method
        super(SIRIUS_CDI_Experiment, self).__init__(**kwargs)

        # do here any data member initialization
        self.warmup_iter = warmup_iter

        # the following data members are set by loadData()
        self.magn = None
        self.farfield = None
        self.data_zeros = None
        self.mask_idx = None
        self.fmask = None
        self.support_idx = None
        self.abs_illumination = None
        self.supp_phase = None

    def loadData(self):
        """
        Load CDI dataset. Create the initial iterate.
        """

        # make sure input data can be found, otherwise download it
        GetData.getData('Phase')
        if not self.silent:
            print('Loading data file diff2D_2_2796_2796')
        f = np.load(datadir/'Phase'/'diff2D_2_2796_2796.npy')
        dp = f[0,:,:]

        # diffraction pattern
        orig_res = max(dp.shape[0], dp.shape[1])  # actual data size                # orig_res=2796
        step_up = ceil(log2(self.Nx) - log2(orig_res))                              # -3.
        workres = 2**(step_up) * 2**(floor(log2(orig_res)))  # desired array size   # workres=256.0
        N = int(workres)                                                            # N=256   

        # center data and use region of interest around center
        # central pixel
        # find max data value
        argmx = unravel_index(argmax(dp), dp.shape)

        Xi = argmx[0] + 1
        Xj = argmx[1] + 1

        # get desired roi:
        # necessary conversion
        Di = N/2 - (Xi-1)
        Dj = N/2 - (Xj-1)

        # roi around central pixel
        Ni = 2*(Xi + Di*(Di < 0) - 1)
        Nj = 2*(Xj + Dj*(Dj < 0) - 1)

        tmp = zeros((N, N))
        tmp[int(Di*(Di > 0)):int(Di*(Di > 0) + Ni), int(Dj*(Dj > 0)):int(Dj*(Dj > 0) + Nj)] \
                = dp[int(Xi - Ni/2) - 1:int(Xi + Ni/2 - 1), int(Xj - Nj/2) - 1:int(Xj + Nj/2 - 1)]
        # Zero pad the data
        # tmp=ZeroPad(tmp)
        # N=2*N
        # self.Nx=2*self.Nx
        tmp = np.roll( tmp, (-1,-1) )

        M = (fftshift(tmp))
        self.data_zeros = np.where(M == 0)
        ## find the indexes of the dead pixels
        # and make the corresponding Fourier mask
        self.mask_idx = np.where(M == -1)
        self.fmask = np.ones((N,N))
        self.fmask[self.mask_idx] = 0

        ## set M to zero on the dead pixels
        #  but later we will allows the magnitudes on 
        #  these pixels be free
        M[self.mask_idx] = 0
        # M = M**(.5)
        self.dp = fftshift(M) # store dp for display later

        # M=tmp.^(.5)

        ## define support.  
        # The higher support_tightness the larger the support
        support_tightness = .5
        DX = np.ceil((N/2)* support_tightness)
        S = zeros((N, N))
        S[int(N/2 - 1 - DX) - 1:int(N/2 + 1 + DX), int(N/2 - 1 - DX/2) - 1:int(N/2 + 1 + DX/2)] = 1
        # S[int(N/4 + 1 + DX) - 1:int(3/4 * N - 1 - DX), int(N/4 + 1 + DX) - 1:int(3/4*N - 1 - DX)] = 1
        self.magn = 1  # magnification factor
        self.farfield = True
        if not self.silent:
            print('Using farfield formula.')

        self.rt_data = Cell(1)
        self.rt_data[0] = M
        # standard for the main program is that 
        # the data field is the magnitude SQUARED
        # in Luke.m this is changed to the magnitude.
        self.data = Cell(1)
        self.data[0] = M**2
        self.norm_rt_data = np.linalg.norm(ifft2(M), 'fro')
        self.support_idx = np.nonzero(S)
        self.support = S
        self.sets = 2

        # use the abs_illumination field to represent the 
        # support constraint.
        self.abs_illumination = S
 
        # initial guess
        if self.initial_guess is None:
            tmp_rnd = (np.random.rand(N, N)).T # to match Matlab
            self.u0 = S * tmp_rnd
            self.u0 = self.u0 / np.linalg.norm(self.u0, 'fro') * self.norm_rt_data
        else:
            # zero pad old u0 to fit new dimensions
            if ~(self.initial_guess.size==M.size):
                tmp=fftshift(fft2(self.initial_guess))
                self.u0 = ifft2(fftshift(ZeroPad(tmp)))

    def setupProxOperators(self):
        """
        Determine the prox operators to be used for this experiment
        """
        super(SIRIUS_CDI_Experiment, self).setupProxOperators()  # call parent's method

        self.propagator = 'Propagator_FreFra'
        self.inverse_propagator = 'InvPropagator_FreFra'

        # remark: self.farfield is always true (set in data processor)
        # self.proxOperators already contains a prox operator at slot 0.
        # Here, we add the second one.
        if self.constraint == 'phaselift':
            self.proxOperators.append('P_Rank1')
        elif self.constraint == 'phaselift2':
            self.proxOperators.append('P_rank1_SR')
        else:
            self.proxOperators.append('Approx_Pphase_FreFra_Poisson')
        self.nProx = self.sets

    def show(self):
        """
        Generate graphical output from the solution
        """

        # display plot of far field data and support constraint
        # figure(123)
        f, (ax1, ax2) = subplots(1, 2,
                                 figsize=(self.figure_width, self.figure_height),
                                 dpi=self.figure_dpi)
        im = ax1.imshow(log10(self.dp + 1e-15))
        addColorbar(im)
        ax1.set_title('Far field data')
        im = ax2.imshow(self.abs_illumination)
        ax2.set_title('Support constraint')
        plt.subplots_adjust(wspace=0.3)  # adjust horizontal space (width)
        # between subplots (default = 0.2)
        f.suptitle('SIRIUS_CDI Data')
        plt.savefig("result_figures/figure_123.png")

        # call parent to display the other plots
        super(SIRIUS_CDI_Experiment, self).show()

from proxtoolbox.experiments.experiment import Experiment
from proxtoolbox import proxoperators

import numpy as np
import matplotlib.pyplot as plt
import time

class MC_Experiment(Experiment):
    """
    Matrix completion, MC, experiment class

    Written by Maximilian Seeber, 
    Universität Göttingen, 2022.
    """

    @staticmethod
    def getDefaultParameters():
        defaultParams = {
            'experiment_name': 'MC',
            'MAXIT': 1000,
            'TOL': 1e-6,
            'n_1': 100,
            'n_2': 100,
            'rank': 5,
            'fraction_known_entrees': 0.5,
            'norm_data': 1,
            'diagnostic': False,
            'rnd_seed': None,
            'gamma': 1,
            'lambd': 1e-9,
            'picture_infilling': False,
            'anim': True,
            'anim_colorbar': True
        }
        return defaultParams

    def __init__(self,
        n_1,
        n_2,
        fraction_known_entrees,
        rank,
        norm_data,
        diagnostic,
        gamma,
        lambd,
        picture_infilling,
        **kwargs):
        super().__init__(**kwargs)
        self.n_1 = n_1
        self.n_2 = n_2
        self.p = fraction_known_entrees
        self.r = rank
        self.norm_data = norm_data
        self.diagnostic = False
        self.gamma = gamma
        self.lambd = lambd
        self.picture_infilling = picture_infilling
        self.frob_list = []
        self.rank_list = []

    def loadData(self):        
        if self.picture_infilling:
            self.n_1 = 100
            self.n_2 = 200
            self.mask = np.random.rand(self.n_1, self.n_2) < self.p

            self.u0 = np.ones((self.n_1, self.n_2))
            self.u0[20:80, 10:20] = 0
            self.u0[30:40, 20:30] = 0
            self.u0[40:60, 30:40] = 0
            self.u0[60:70, 40:50] = 0
            self.u0[20:80, 50:60] = 0

            self.u0[40:80, 80:90] = 0.5
            self.u0[20:40, 90:100] = 0.5
            self.u0[60:70, 90:100] = 0.5
            self.u0[40:80, 100:110] = 0.5

            self.u0[20:80, 130:140] = 0
            self.u0[20:30, 140:150] = 0
            self.u0[20:60, 150:160] = 0
            self.u0[20:30, 160:170] = 0
            self.u0[20:80, 170:180] = 0
        else:
            self.mask = np.random.rand(self.n_1, self.n_2) < self.p
            self.u0 = (np.random.randn(self.n_1, self.r))@(np.random.randn(self.r, self.n_2))

        
        self.M = np.copy(self.u0)
        if self.algorithm_name == 'ALS':
            self.u0 = np.random.randn(self.r, self.n_1), np.random.randn(self.r, self.n_2)
        else:
            self.u0 = self.u0*self.mask

    def setupProxOperators(self):
        if self.algorithm_name == 'AP':
            super().setupProxOperators()
            self.proxOperators = ['MC_P_M', 'MC_P_A']
        elif self.algorithm_name == 'DR':
            super().setupProxOperators()
            self.proxOperators = ['MC_P_A', 'MC_prox_nuc']
        elif self.algorithm_name == 'ALS':
            super().setupProxOperators()
            self.proxOperators = ['MC_ALS', 'MC_ALS']

    def show(self):
        ch = self.output['stats']['changes']
        u = self.output['u']
        print(f'reconstruction error: {np.linalg.norm(u-self.M)}')

        fig, (ax11, ax12) = plt.subplots(1, 2, figsize=(20, 10))
        im = ax11.imshow(self.M)
        ax11.set(title = 'original matrix $M$')
        # plt.colorbar(im, cax=ax115)
        ax12.imshow(self.M)
        masked = np.ma.masked_where(self.mask == True, self.mask)
        ax12.imshow(masked, alpha=1, cmap='Reds')
        ax12.set(title = 'algorithm input $X_0$')

        fig, (ax21, ax22) = plt.subplots(1, 2, figsize=(16, 5))
        ax21.semilogy(self.frob_list)
        ax21.set(xlabel='$k$', ylabel='$|| M - X_k ||$', title='distance to $M$')
        ax22.semilogy(ch)
        ax22.set(xlabel='$k$', ylabel='$|| X_{k-1} - X_k ||$', title='change for each iteration')
        plt.tight_layout()
        plt.show()

    def animate(self, alg):
        if alg.u_new is not None:
            if self.algorithm_name == 'DR':
                self.animateFigure(alg.proxOperators[0].eval(alg.u_new), f'$X_{{{alg.iter}}}$')
                self.frob_list += [np.linalg.norm(alg.proxOperators[0].eval(alg.u_new) - self.M)]
                self.rank_list += [np.linalg.matrix_rank(alg.u_new)]
            else:
                self.animateFigure(alg.u_new, f'$X_{{{alg.iter}}}$')
                self.frob_list += [np.linalg.norm(alg.u_new - self.M)]
                self.rank_list += [np.linalg.matrix_rank(alg.u_new)]
            if alg.iter < 50:
                time.sleep(0.3)
            time.sleep(0.1)
        else:
            if self.algorithm_name == 'ALS':
                self.animateFigure(self.u0[0].T@self.u0[1], '$X_0$')
            else:
                self.animateFigure(self.u0, '$X_0$')
            time.sleep(0.7)
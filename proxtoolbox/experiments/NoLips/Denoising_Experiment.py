
from proxtoolbox.experiments.NoLips.nolipsExperiment import NoLipsExperiment
from proxtoolbox import proxoperators
from proxtoolbox.proxoperators.ADM_prox import *
from proxtoolbox.proxoperators.Prox_lp import *
from proxtoolbox.proxoperators.proxoperator import ProxOperator
# from proxtoolbox.proxoperators.Prox_Sparsity import *
from proxtoolbox.utils.functions import Quadratic 

from proxtoolbox.utils.cell import Cell, isCell
import numpy as np
from numpy import random
from skimage import io as ski_io
from skimage import data, io, filters, img_as_float, img_as_ubyte

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.pyplot import subplots, show, figure



class DenoisingExperiment(NoLipsExperiment):
    '''
    denoising experiment class
    '''
    @staticmethod
    def getDefaultParameters():
        defaultParams = {
            'experiment_name': 'Denoising_Experiment',
            'domain_objective': 'Quadratic',
            'primal_scaling' : 1.0,
            'image_objective': 'l1',
            'image_dim': 10,
            'image_scaling' : 1.0,
            'Bregman_potential':'l2',
            'product_space_dimension': 1,
            'algorithm_name' : 'ADM',
            'augmentation_scaling' : 0.01,
            'coupling_function' : 'Square_Quadratic',
            'coupling_approximation' : 'linear',
            'Lip_grad_max' : 20,
            'Maxit': 10000,
            'lambda_0': 0.05, 
            'lambda_max': 0.05,
            'lambda_switch': 5,
            'TOL': 1e-12,
            'theta': 1.0,
            'data_ball': 1e-30,
            'diagnostic': True,
            'save_output': True,
            'iterate_monitor_name': 'ADM_IterateMonitor',
            'verbose': 0,
            'graphics': 1,
            'anim': False,
            'debug':True
        }
        return defaultParams


    def __init__(self, **kwargs):
        super(DenoisingExperiment, self).__init__(**kwargs)
        self.A_tens = None
        self.b_mat = None
        self.c_vec = None

    def loadData(self):
        """
        Load/create quadratics and the initial iterate.
        # Define self parameters:  overwrites defaults unless the user 
        # does not specify these
        for key in self.getDefaultParameters():
            setattr(self, key, self.getDefaultParameters()[key] if key not in kwargs else kwargs[key])
        """
        # read the image
        # N = 976
        datafile=(datadir/'denoising'/'Tubulin_Jennifer_STED_2Det_pix10nm.tiff')
        img_big = ski_io.imread(datafile)
        # Normalize intensity
        N=min(img_big.shape)
        img = img_big[:N, :N]
        imshape = img.shape
        lmin = img.min()
        lmax = img.max()
        img_big = (img - lmin) / (lmax - lmin)
        img_l1norm = img.sum()
        print ('image shape: {}'.format(imshape))
        print ('img min: {} max: {}'.format(img.min(), img.max()))
        print ('img norm (l1): {}'.format(img_l1norm))
        print ('img std: {}'.format(img.std()))
        A = NoLips_dict['A']
        b = NoLips_dict['b']
        c = NoLips_dict['c']
        
        A = 1

        x0 = 2*random.rand(n,1) - 1.0
        self.A_tens = A
        self.b_mat = div_grad
        self.c_vec = 0
        self.x0 = x0

    def setupProxOperators(self):
        """
        Determine the prox operators to be used for this experiment
        """
        super(Denoising_Experiment, self).setupProxOperators()  # call parent's method


        # Now we adjust data structures and prox operators according to the algorithm
        if self.algorithm_name == 'ADMM':
            print('not implemented')

        if self.algorithm_name == 'ADM':
            # set-up variables in cells for block-wise algorithms
            # ADM has two blocks of variables, primal and auxilliary (primal
            # variables in the image space of some mapping). 
            # we sort these into a 2D cell.
            self.u0 = Cell(2)
            self.u0[0] = self.x0
            self.proxOperators = []
            self.productProxOperators = []
            # set Bregman potential
            if self.Bregman_potential == 'h_4':
               self.dBregman_potential = 'dh_4'
            elif self.Bregman_potential == 'l2':
               self.dBregman_potential = None
            else:
                print('potential not implemented')

            # all the next few lines do is initialize the ADM algorithm
            if self.domain_objective == 'Quadratic':
                if ((self.coupling_function == 'Square_Quadratic') or (self.coupling_function == 'Primal_Dual_alignment')):
                    # what is primal_dual alignment? 
                    self.proxOperators.append('Prox_NoLips_primal_quadratic_plus_Bregman')
                    if self.image_objective == 'l1':
                        self.proxOperators.append('Prox_NoLip_image_l1_at_q') 
                    elif self.image_objective == 'linfty':
                        self.proxOperators.append('Prox_NoLip_image_linfty_at_q') 
                    else:
                        print('Message from randomExperiment.py: not implemented')
                else:
                    print('not implemented')
            elif self.domain_objective == 'l2':
                if ((self.coupling_function == 'Square_Quadratic') or (self.coupling_function == 'Primal_Dual_alignment')):
                    # what is primal_dual alignment? 
                    self.proxOperators.append('Prox_NoLips_primal_l2_plus_Bregman')
                    if self.image_objective == 'l1':
                        self.proxOperators.append('Prox_NoLip_image_l1_at_q') 
                    elif self.image_objective == 'linfty':
                        self.proxOperators.append('Prox_NoLip_image_linfty_at_q') 
                    else:
                        print('Message from randomExperiment.py: not implemented')
                else:
                    print('not implemented')
            elif self.domain_objective == 'l1':
                if ((self.coupling_function == 'Square_Quadratic')  or (self.coupling_function == 'Primal_Dual_alignment')):
                    self.proxOperators.append('Prox_NoLips_primal_l1_plus_Bregman')
                    if self.image_objective == 'l1':
                        self.proxOperators.append('Prox_NoLip_image_l1_at_q') 
                    elif self.image_objective == 'linfty':
                        self.proxOperators.append('Prox_NoLip_image_linfty_at_q') 
                    else:
                        print('Message from randomExperiment.py: not implemented')
                else:
                    print('Message from randomExperiment.py: not implemented')
            elif self.domain_objective == 'l0':
                if ((self.coupling_function == 'Square_Quadratic') or (self.coupling_function == 'Primal_Dual_alignment')):
                    self.proxOperators.append('Prox_NoLips_primal_l0_plus_Bregman')
                    if self.image_objective == 'l1':
                        self.proxOperators.append('Prox_NoLip_image_l1_at_q') 
                    elif self.image_objective == 'linfty':
                        self.proxOperators.append('Prox_NoLip_image_linfty_at_q') 
                    else:
                        print('Message from randomExperiment.py: not implemented')
                else:
                    print('Message from randomExperiment.py: not implemented')
            elif self.domain_objective == 'none':
                # So far, the case of zero primal function is the only one with a 
                # quadratic approximation to the coupling function, so this instance is 
                # more involved than the other ones above.  
                if ((self.coupling_function == 'Square_Quadratic') or (self.coupling_function == 'Primal_Dual_alignment')):
                    # what is Primal_Dual_alignment?
                    self.proxOperators.append('Prox_NoLips_primal_0_plus_Bregman')
                    if self.image_objective == 'l1':
                        self.proxOperators.append('Prox_NoLip_image_l1_at_q') 
                    elif self.image_objective == 'linfty':
                        self.proxOperators.append('Prox_NoLip_image_linfty_at_q') 
                    else:
                        print('Message from randomExperiment.py: not implemented')
                else:
                    print('Message from randomExperiment.py: not implemented')
            else:
                print('Message from randomExperiment.py: domain penalty not implemented')

            # fill second block of initial values
            if ((self.coupling_function == 'Square_Quadratic') or (self.coupling_function == 'Primal_Dual_alignment')):
                prox_parameter = self.image_scaling/self.lambda_0
                y_prime = Quadratic(self.A_tens, self.b_mat, self.c_vec, self.u0[0])
                if self.image_objective=='l1':
                    self.u0[1] = Prox_l1.eval(y_prime, prox_parameter)
                elif self.image_objective=='linfty':
                    self.u0[1] = Prox_linfty.eval(y_prime, prox_parameter)
                else:
                    print('Message from randomExperiment.py: not implemented')
            else:
                print('Message from randomExperiment.py: not implemented')
    
    def show(self):
        """
        Generate graphical output from the solution
        """
        u_m = self.output['u_monitor']
        if isCell(u_m):
            u = u_m[0]
            if isCell(u):
                u = u[0]
            u2 = u_m[len(u_m)-1]
            if isCell(u2):
                u2 = u2[len(u2)-1]
        else:
            u2 = u_m
            if u2.ndim > 2:
                u2 = u2[:,:,0]
            u = self.output['u']
            if isCell(u):
                u = u[0]
            elif u.ndim > 2:
                u = u[:,:,0]

        algo_desc = self.algorithm.getDescription()
        title = "Algorithm " + algo_desc 
        
        # figure 900
        changes = self.output['stats']['changes']
        time = self.output['stats']['time']
        time_str = "{:.{}f}".format(time, 5) # 5 is precision
        label = "Iterations (time = " + time_str + " s)"

        f, ((ax1, ax2, ax3)) = subplots(3, 1, \
                    figsize = (self.figure_width, self.figure_height),
                    dpi = self.figure_dpi)
        
        ax1.semilogy(changes)
        ax1.set_xlabel(label)
        ax1.set_ylabel('Log of change in iterates')

        if 'gaps' in self.output['stats']:
            gaps = self.output['stats']['gaps']
            ax2.semilogy(gaps)
            ax2.set_xlabel(label)
            ax2.set_ylabel('Log of the objective values')
        if 'shadows' in self.output['stats']:
            shadows = self.output['stats']['shadows']
            ax3.plot(shadows)
            ax3.set_xlabel(label)
            ax3.set_ylabel('Distance to critical point set')

        f.suptitle(title)
        plt.subplots_adjust(hspace = 0.3) # adjust vertical space (height) between subplots (default = 0.2)
        plt.subplots_adjust(wspace = 0.3) # adjust horizontal space (width) between subplots (default = 0.2)
        
        show()

    def normalize_intensity(img):
        """ Make image with arbitrary real values normalized, i.e. put them
        into [0.1] interval.

        """
        lmin = img.min()
        lmax = img.max()
        return (img - lmin) / (lmax - lmin)

 

# TO DO:  put in the function f and phi.  Not sure yet how to do this since these could vary. Should probably be done
# at the NoLipsExperiment level (one up from this) since these are in principle independent of the particular 
# quadratic forms. 

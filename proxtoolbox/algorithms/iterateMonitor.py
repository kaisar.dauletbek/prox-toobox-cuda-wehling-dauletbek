from numpy import zeros, sqrt
from numpy.linalg import norm

from proxtoolbox import algorithms
from proxtoolbox.utils.cell import isCell
from proxtoolbox.utils.size import size_matlab


class IterateMonitor:
    """
    Base class for iterate monitors. 
    This is the default algorithm analyzer for monitoring
    iterates of fixed point algorithms.
    """

    def __init__(self, experiment):
        self.u0 = experiment.u0
        assert self.u0 is not None, 'No valid initial guess given'
        self.u_monitor = self.u0  # the part of the sequence that is being monitored put u0 as default
        self.isCell = isCell(self.u0)
        self.norm_data = experiment.norm_data
        self.truth = experiment.truth
        self.truth_dim = experiment.truth_dim
        self.formulation = experiment.formulation
        self.norm_truth = experiment.norm_truth
        self.diagnostic = experiment.diagnostic
        self.n_product_Prox = experiment.n_product_Prox
        self.anim = experiment.anim
        self.anim_callback = experiment.animate
        self.anim_step = experiment.anim_step
        self.silent = experiment.silent

        self.changes = None

        # instantiate optimality monitor if it exists
        self.optimality_monitor = None
        if hasattr(experiment, 'optimality_monitor') and experiment.optimality_monitor is not None:
            optimality_monitor_name = experiment.optimality_monitor
            optimality_monitor_class = getattr(algorithms, optimality_monitor_name)
            self.optimality_monitor = optimality_monitor_class(experiment)

    def preprocess(self, alg):
        """
        Allocate data structures needed to collect 
        statistics. Called before running the algorithm.
 
        Parameters
        ----------
        alg : instance of Algorithm class
            Algorithm to be monitored.
        """
        self.maxIter = alg.maxIter
        self.changes = zeros(self.maxIter + 1)
        self.changes[0] = sqrt(999)

    def updateStatistics(self, alg):
        """
        Update statistics. Called at each iteration
        while the algorithm is running.
 
        Parameters
        ----------
        alg : instance of Algorithm class
            Algorithm being monitored.
        """
        self.u_monitor = alg.u_new  # store the last iterate in u_monitor
        if not self.isCell:
            tmp_change = self.evaluateChange(alg.u, alg.u_new)
        else:
            tmp_change = 0
            for u_elem, u_new_elem in zip(alg.u, alg.u_new):
                tmp_change += self.evaluateChange(u_elem, u_new_elem)
        self.changes[alg.iter] = sqrt(tmp_change)

    def displayProgress(self, alg):
        """
        Display progress. This method is called after 
        UpdateStatistics(). Default implementation does
        nothing. May be overriden in derived classes
 
        Parameters
        ----------
        alg : instance of Algorithm class
            Algorithm being monitored.
        """
        if (not self.silent) and self.anim and self.anim_callback is not None \
                and alg.iter % self.anim_step == 0:
            self.anim_callback(alg)

    def postprocess(self, alg, output):
        """
        Called after the algorithm stops. Store statistics in
        the given 'output' dictionary

        Parameters
        ----------
        alg : instance of Algorithm class
            Algorithm that was monitored.
        output : dictionary
            Contains the last iterate and various statistics that
            were collected while the algorithm was running.
                
        Returns
        -------
        output : dictionary into which the following entries are added
            u_monitor : ndarray or list of ndarrays.
                Part of the sequence that is being monitored. Default
                is to monitor the entire variable 'u'.
            u1 : ndarray 
                Part of 'u_monitor'. Used for display.
            u2 : ndarray
                Part of 'u_monitor'. Used for display.
            changes: ndarray
                normalized change in successive iterates
        """

        output['u_monitor'] = self.u_monitor
        if self.diagnostic:
            if isCell(self.u_monitor):
                u_m = self.u_monitor[0]
            else:
                u_m = self.u_monitor
            # Python code for [m,n,p] = size(u_m)
            if isCell(u_m):
                # in matlab this corresponded to a cell
                # here we assume that such a cell is m by n where m = 1
                # so far this seems to be always the case
                # the following tests attempt to match the ndarray case below
                m = 1
                n = len(u_m)
                if n == self.n_product_Prox:
                    u1 = u_m[0]
                    u2 = u_m[n - 1]
                else:  # although the following case corresponds to Matlab's
                    # code, this is suspicious because u1 and u2 do not
                    # have the same structure as in the case above
                    u1 = u_m
                    u2 = u_m
            else:  # ndarray
                if u_m.ndim == 1:
                    u1 = u_m
                    u2 = u_m
                else:
                    m, n, p, q = size_matlab(u_m)
                    if n == self.n_product_Prox:
                        u1 = u_m[:, 0]
                        u2 = u_m[:, n - 1]
                    elif m == self.n_product_Prox:
                        u1 = u_m[0, :]
                        u2 = u_m[m - 1, :]
                    elif p == self.n_product_Prox:
                        if p > 1:
                            u1 = u_m[:, :, 0]
                            u2 = u_m[:, :, p - 1]
                        else:
                            u1 = u_m
                            u2 = u_m
                    else:
                        u1 = u_m
                        u2 = u_m
            output['u1'] = u1
            output['u2'] = u2

        if 'stats' in output:
            output['stats']['changes'] = self.changes[1:alg.iter + 1]
        return output

    def getIterateSize(self, u):
        """
        Given an iterate, determine p and q parameters
        :param u:
        :return: p,q (ints)
        """
        # TODO: explain what p, q are?
        if u.ndim < 3:
            p = 1
            q = 1
        elif u.ndim == 3:
            p = u.shape[2]
            q = 1
        else:
            p = u.shape[2]
            q = u.shape[3]
        return p, q

    def evaluateChange(self, u, u_new):
        """
        Given an old and new iterate calculate the total absolute
        squared difference, normalized to self.norm_data
        :param u: iterate
        :param u_new: new iterate
        :return: sum of abs squared differences = frobenius norm squared
        """
        p, q = self.getIterateSize(u_new)
        tmp_change = 0
        if p == 1 and q == 1:
            tmp_change = (norm(u - u_new) / self.norm_data) ** 2
        elif q == 1:
            for j in range(p):  # TODO this loop can be replaced by np.sum(abs(...)**2)/self.norm_data**2
                tmp_change += (norm(u[:, :, j] - u_new[:, :, j]) / self.norm_data) ** 2
        else:  # 4D arrays?!!!
            for j in range(q):  # TODO this loop can be replaced by np.sum(abs(...)**2)/self.norm_data**2
                for k in range(p):
                    tmp_change += (norm(u[:, :, k, j] - u_new[:, :, k, j]) / self.norm_data) ** 2
        return tmp_change

    def calculateObjective(self, alg):
        """
        Calculate objective value. The default implementation
        uses the optimality monitor if it exists

        Parameters
        ----------
        alg : instance of Algorithm class
            Algorithm that was monitored.
                
        Returns
        -------
        objValue : real
            objective value
        """
        if self.optimality_monitor is not None:
            return self.optimality_monitor.calculateObjective(alg)
        else:
            raise AttributeError("optimality_monitor was not provided")

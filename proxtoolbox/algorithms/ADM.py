
from proxtoolbox.algorithms.algorithm import Algorithm
from proxtoolbox import algorithms
from proxtoolbox.utils.cell import Cell, isCell
from proxtoolbox.utils.size import size_matlab
import numpy as np
from numpy import pi, zeros, conj


class ADM(Algorithm):
    """
    Alternating directions method of multipliers for solving problems of the form :
    minimize f(x) + g(y), 
    subject to Ax=y

    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on October 2, 2017.    
    """

    def __init__(self, experiment, iterateMonitor, accelerator):
        super(ADM, self).__init__(experiment, iterateMonitor, accelerator)
        # instantiate Lagrange multiplier update object
        # heavy handed, but works:
        # self=experiment
    
    def evaluate(self, u):
        """
        Update of the ADM algorithm
       
        Parameters
        ----------
        u : a 2-dimensional cell
            The current iterate. 2 blocks of variables, primal (x),
            and image/auxilliary (y) 
               
        Returns
        -------
        u_new : a 2-dimensional cell
            The new iterate (same type as input parameter `u`).
        """

        lmbda = self.computeRelaxation()
        u_new = u.copy()  # this is only a shallow copy, but this will be enough in this case
        self.prox1.lmbda = lmbda
        u_new[0] = self.prox1.eval(u)
        self.prox2.lmbda = lmbda
        u_new[1] = self.prox2.eval(u_new)

        return u_new



    def getDescription(self):
        return self.getDescriptionHelper("\\lambda", self.lambda_0, self.lambda_max)
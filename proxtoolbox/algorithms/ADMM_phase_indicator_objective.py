
from proxtoolbox.proxoperators.proxoperator import ProxOperator
from proxtoolbox.proxoperators.ADMM_prox import ADMM_Context
from proxtoolbox.utils.cell import Cell, isCell
from proxtoolbox.utils.size import size_matlab
from proxtoolbox.utils.accessors import accessors
import numpy as np
from numpy import pi, zeros, conj
from numpy.fft import fft2, ifft2, fft, ifft
from numpy.linalg import norm
import copy


class ADMM_phase_indicator_objective(ADMM_Context):
    """
    Augmented Lagrangian for the ADMM algorithm applied to the 
    phase retrieval problem with an indicator function for the primal objective:
    Lagrangian(u{1}, u{2}, u{3})= iota_0(u{1}) + ...
    sum_{j=1}^m iota_j(u{2}(j) + <u{3}(j), (F_ju{1}(j))-u{2}(j))>
        + 1/2||F_ju{1}(j))-u{2}(j)||^2

    We assume that the point u is feasible, so the indicator functions
    will be zero and all that need be computed is:
        Lagrangian(u{1}, u{2}, u{3}) = sum_{j=1}^m <u{3}(j),
            (F_ju{1}(j))-u{2}(j)> + 1/2||F_ju{1}(j))-u{2}(j)||^2
       
    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on Oct 3, 2017.    
    """

    def __init__(self, experiment):
        super(ADMM_phase_indicator_objective, self).__init__(experiment)
        if hasattr(experiment, 'norm_data'):
            self.normM = experiment.norm_data
        else:
            self.normM = 1.0


    def calculateObjective(self, alg):
        """
        Evaluate ADMM objective function
            
        Parameters
        ----------
        alg : algorithm instance
            The algorithm that is running
         
        Returns
        -------
        lagrangian : real
            The value of the objective function 
            which will in this case measure the gap 
        """
        # The implementation of this function is in many ways similar to
        # the ADMM_PhaseLagrangeUpdate code. The structure is similar 
        # even if the calculation is different. It would be nice to
        # use one common implementation

        lagrangian = 0
        u = alg.u_new

        m1, n1, p1, _q1 = size_matlab(u[0])
        k2 = self.product_space_dimension
        get, _set, _data_shape = accessors(u[1], self.Nx, self.Ny, k2)

        eta = self.lmbda

        if m1 > 1 and n1 > 1 and p1 == 1:
            FFT = lambda u: fft2(u)
            IFFT = lambda u: ifft2(u)
            for j in range(k2):
                if self.farfield:
                    if self.fresnel_nr is not None and self.fresnel_nr[j] > 0:
                        u_hat = -1j*self.fresnel_nr[j]/(self.Nx*self.Ny*2*pi)*FFT(u[0]-self.illumination[j]) + self.FT_conv_kernel[j]
                    elif self.FT_conv_kernel is not None:
                        u_hat = FFT(self.FT_conv_kernel[j]*u[0]) / (m1*n1)
                    else:
                        u_hat = FFT(u[0]) / (m1*n1)
                else: # near field
                    if self.beam is not None:
                        u_hat = IFFT(self.FT_conv_kernel[j]*FFT(u*self.beam[j]))/self.magn[j]
                    else:
                        u_hat = IFFT(self.FT_conv_kernel[j]*FFT(u))/self.magn[j]
                tmp = u_hat - u[1][j]
                lagrangian += np.real(np.trace(u[2][j].T.conj() @ tmp)) / self.normM**2
                lagrangian += 0.5 * eta * (norm(tmp)/self.normM)**2
        elif m1 == 1 or n1 == 1:  # 1D signals
            if m1 == 1:
                divisor = n1
            else:
                divisor = m1
            FFT = lambda u: fft(u)
            IFFT = lambda u: ifft(u)
            for j in range(k2): 
                if self.farfield:
                    if self.fresnel_nr is not None and self.fresnel_nr[j] > 0:
                        raise NotImplementedError('Error ADMM_phase_indicator_objective: complicated far field set-ups for 1D signals not implemented')
                    elif self.FT_conv_kernel is not None:
                        u_hat = FFT(self.FT_conv_kernel[j]*u[0]) / divisor
                    else:
                        u_hat = FFT(u[0]) / divisor
                else:
                    raise NotImplementedError('Error ADMM_phase_indicator_objective: near field set-ups for 1D signals not implemented')
                
                tmp = u_hat - get(u[1],j)
                lagrangian += np.real((get(u[2],j).T.conj() @ tmp)) / self.normM**2
                lagrangian += 0.5 * (norm(tmp)/self.normM)**2
        else:
            print('Error ADMM_phase_indicator_objective: Not designed to handle 4D arrays - throwing a dummy value 999')
            lagrangian = 999
        
        return lagrangian



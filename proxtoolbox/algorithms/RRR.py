from proxtoolbox.algorithms.algorithm import Algorithm
from proxtoolbox.utils.cell import Cell, isCell
from numpy import exp


class RRR(Algorithm):
    """
    User-friendly version of the Relaxed-Reflect-Reflect algorithm.  
    For background see:
                V. Elser, The complexity of bit retrieval, 
                        IEEE Trans. Inform. Theory, 64 (2018), pp. 412–428


    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on Sept 22, 2017.    
    """

    def evaluate(self, u):
        """
        Update for the Relaxed-Reflect-Reflect

        Parameters
        ----------
        u : ndarray or a list of ndarray objects
            The current iterate.
               
        Returns
        -------
        u_new : ndarray or a list of ndarray objects
            (same type as input parameter u)
            The new iterate.
        """

        lmbd = self.computeRelaxation()
        if not isCell(u):
            tmp1 = 2*self.prox2.eval(u) - u  # Reflection using prox2 on u
            tmp2 = self.prox1.eval(tmp1)     # Projection of the reflection
            # update
            u_new = lmbd*(2*tmp2 - tmp1)/2 + (1 - lmbd/2)*u
        else:
            u_new = self.prox2.eval(u)
            tmp1 = Cell(len(u))
            for j in range(len(u)):
                tmp1[j] = 2*u_new[j] - u[j]
            tmp2 = self.prox1.eval(tmp1)
            # update
            for j in range(len(u)):
                u_new[j] = lmbd*(2*tmp2[j]-tmp1[j])/2 + (1-lmbd/2)*u[j]
        return u_new

    def getDescription(self):
        return self.getDescriptionHelper("\\lambda", self.lambda_0, self.lambda_max)

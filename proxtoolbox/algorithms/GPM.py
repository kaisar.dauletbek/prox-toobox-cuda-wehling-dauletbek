import numpy as np
from proxtoolbox.algorithms.algorithm import Algorithm

class GPM(Algorithm):
    """
    Gradient projection method for solving convex constraint
    problems with lipschitz gradient.
    The lipschitz gradient is an assumption for the
    eventually constant stepsize rule which is
    implemented in this class.
    See e.g. D. Bertsekas in "Convex Optimization Algorithms", pp. 302.
    """

    def __init__(self, experiment, iterateMonitor, accelerator=None):
        super().__init__(experiment, iterateMonitor, accelerator)

        self.f = experiment.f # objective
        self.df = experiment.df # derivative of objective
        self.alpha = experiment.alphaGPM # stepsize
        self.beta = experiment.betaGPM # reducing factor for the stepsize (eventually constant stepsize rule, D. Bertsekas)
        self.constBounds = experiment.constBounds # np-array or scalar


    def evaluate(self, u):
        """
        Update for the gradient projection method using
        the eventually constant stepsize rule. 

        Parameters
        ----------
        u : ndarray or a list of ndarray objects
            The current iterate.
               
        Returns
        -------
        u_new : ndarray or a list of ndarray objects
            The new iterate (same type as input parameter `u`).
        """

        # iteration
        u_new = self.prox1.eval(u-self.alpha*self.df(u), self.constBounds)        
        
        # compute norm of change in x
        diff = np.linalg.norm(u_new-u)

        # adjust step size accordin to eventually constant stepsize rule (D. Bertsekas)
        while not self.f(u_new) <= self.f(u) + np.real(np.conj(self.df(u))@(u_new-u)) + diff**2/(2*self.alpha):
            self.alpha *= self.beta
            u_new = self.prox1.eval(u-self.alpha*self.df(u), self.constBounds)
            diff = np.linalg.norm(u_new-u)
        
        if self.f(u) + 1e-6 < self.f(u_new):
            print('GPM FAILED TO FIND SMALLER SOLUTION')
            print('   iteration:',self.iter)
            print('   f(u),f(u_new):',self.f(u), self.f(u_new))
            print('   stepsize:',self.alpha)
            
        return u_new
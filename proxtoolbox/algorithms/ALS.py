from proxtoolbox.algorithms.algorithm import Algorithm

import numpy as np

class ALS(Algorithm):
    """
    Alternating least squares algorithm
    """

    def evaluate(self, u):
        """
        Update for the Alternating least squares algorithm. 

        Parameters
        ----------
        u : ndarray or a list of ndarray objects
            The current iterate.
               
        Returns
        -------
        u_new : ndarray or a list of ndarray objects
            The new iterate (same type as input parameter `u`).
        """

        unew = self.proxOperators[0].eval(u)

        return unew

    def run(self, u):
        """
        Run the algorithm given an initial iterate u. The algorithm
        runs until the stopping condition is satisfied. Statistics are
        collected at each iteration by the iterate monitor. 

        Parameters
        ----------
        u: ndarray or a list of ndarray objects
            Initial iterate.
        
        Returns
        -------
        dictionary with the following entries:
            u: ndarray or a list of ndarray objects
                The last iterate.
            iter: natural number
                The last iteration count

            additional entries are given by the iterate monitor

        """
        self.U, self.V = u
        self.u = self.U.T @ self.V
        self.preprocess()
        self.displayProgress()

        if self.progressbar == 'tqdm_notebook':
            pbar = tqdm_notebook(total=self.maxIter)
        elif self.progressbar == 'tqdm':
            pbar = tqdm(total=self.maxIter)
        else:
            pbar = None

        while self.stoppingCondition():
            self.iter += 1
            if pbar is not None:
                pbar.update()
            self.U, self.V = self.evaluate((self.U, self.V))
            self.u_new = self.U.T @ self.V
            self.updateStatistics()
            self.displayProgress()
            self.u = self.u_new
        if pbar is not None:
            pbar.close()
        return self.postprocess()

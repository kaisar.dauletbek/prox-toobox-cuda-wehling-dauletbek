from proxtoolbox.algorithms.algorithm import Algorithm
from proxtoolbox.utils.cell import Cell, isCell
from cupy import exp  # Replace NumPy exp with CuPy exp

class RAAR_Cuda(Algorithm):
    """
    GPU-accelerated version of the Relaxed Averaged Alternating
    Reflection algorithm using CuPy.
    """

    def evaluate(self, u):
        """
        Update for the Relaxed Averaged Alternating
        Reflection algorithm using GPU acceleration.
        """
        lmbd = self.computeRelaxation()
        
        # Assuming `u` is now a CuPy array or list of CuPy arrays
        if not isinstance(u, list):
            # Reflection using prox2 on u
            tmp1 = 2 * self.prox2.eval(u) - u  
            # Projection of the reflection
            tmp2 = self.prox1.eval(tmp1)
            # Update
            u_new = (lmbd * (2 * tmp2 - tmp1) + (1 - lmbd) * tmp1 + u) / 2
        else:
            # If `u` is a list of CuPy arrays
            u_new = [self.prox2.eval(ui) for ui in u]
            tmp1 = [2 * u_new[j] - u[j] for j in range(len(u))]
            tmp2 = [self.prox1.eval(tmp1[j]) for j in range(len(tmp1))]
            # Update for each element in the list
            for j in range(len(u)):
                u_new[j] = (lmbd * (2 * tmp2[j] - tmp1[j]) + (1 - lmbd) * tmp1[j] + u[j]) / 2
        
        return u_new

    def getDescription(self):
        return self.getDescriptionHelper("\\lambda", self.lambda_0, self.lambda_max)

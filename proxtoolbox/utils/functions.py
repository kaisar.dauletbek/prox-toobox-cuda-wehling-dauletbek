
from numpy import einsum, matmul, sign
import numpy as np

def l2(X):
    '''
        the 0.5* 2-norm squared
        Works on vectors of length len(X)
        Returns the square norm
    ''' 
    # using map avoids for... loops at each of the inner functions!
    val = 0.5*sum(X**2) 
    return val

def l0(X):
    '''
        the counting function
        Works on vectors of length len(X)
        Returns the number of nonzero entries
    ''' 
    # using map avoids for... loops at each of the inner functions!
    val = sum(map(abs, map(sign, X))) 
    return val

def l1(X):
    '''
        the l1-norm
        Works on vectors of length len(X)
        Returns the l1-norm
    ''' 
    # using map avoids for... loops at each of the inner functions!
    val = sum(map(abs,X)) 
    return val

def linfty(X):
    '''
        the linfty-norm
        Works on vectors of length len(X)
        Returns the linfty-norm
    ''' 
    # using map avoids for... loops at each of the inner functions!
    val = np.max(abs(X)) 
    return val

def Quadratic(A_tens, b_mat, c_vec, x):
    '''
        DESCRIPTION: computes quadratics 
        INPUT:  x            := the variable          
                self.A_tens       := a tensor (3D array) dimension nxnxm
                self.b_mat        := a matrix (2D array) mxn
                self.c_vec        := a vector length m 
        OUTPUT: Q_vec a vector of values of the m quadratics
    ''' 
    # tmp_mat = map(np.reshape, map(np.matmul,self.A_tens, x),(m,n))
    Q_vec = matmul(np.reshape(matmul(A_tens, x), b_mat.shape) + b_mat, x) + c_vec
    return Q_vec

def h_4_2(x, alpha=0.25, beta=0.5):
    '''
        DESCRIPTION: computes 1/4||x||^4_2 + 1/2 ||x||^2_2 
        INPUT:  x            := the variable          
        OUTPUT: h a scalar
    ''' 
    tmp = np.linalg.norm(x)
    h = alpha * tmp**4 + beta * tmp**2 
    return h

def h_4(x, alpha=0.25):
    '''
        DESCRIPTION: computes 1/4||x||^4_2 
        INPUT:  x            := the variable          
        OUTPUT: h a scalar
    ''' 
    tmp = np.linalg.norm(x)
    h = alpha * tmp**4
    return h
    




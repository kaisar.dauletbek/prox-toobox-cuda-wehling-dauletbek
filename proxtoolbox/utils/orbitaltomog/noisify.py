from numpy.random import poisson
from numpy import ndarray, complex128, complex64, sum, sqrt

__all__ = ["noisify_intensity", "noisify_amplitude"]


def noisify_intensity(truth: ndarray, total_counts: int, background_counts: float = 0.0) -> ndarray:
    """
    Given a perfect intensity map, simulate Poissonian noise

    :param truth: intensity  map, np.array
    :param total_counts: over the full array (results will vary within ~sqrt(total_counts))
    :param background_counts: average number of counts in the background
    :return: noisy array like truth
    """
    assert truth.dtype not in [complex128, complex64], 'Dtype should be real valued!'
    _truth = truth*total_counts/sum(truth)
    countmap = poisson(_truth)
    if background_counts != 0.0:
        countmap += poisson(lam=background_counts, size=_truth.shape)
    return countmap


def noisify_amplitude(truth: ndarray, total_counts: int, background_counts: float = 0.0) -> ndarray:
    """
    Given a perfect amplitude map, simulate a noisy amplitude map by sampling Poisson noise on the intensity

    """
    i_truth = abs(truth)**2
    noise_truth = noisify_intensity(i_truth, total_counts, background_counts=background_counts)
    return sqrt(noise_truth)


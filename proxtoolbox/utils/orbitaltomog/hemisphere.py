import numpy as np
from numpy.fft import fftshift
from itertools import combinations

from .noisify import *
from proxtoolbox.utils.orbitaltomog import interpolation, pad_to_square, binning


__all__ = ['k0_sensitivity', 'k0_sensitivity_values', 'kxky_sensitivity', 'kxkykz_sensitivity',
           'hemisphere_from_kspace', 'noisy_arpes', 'a_dot_k', 'photoemission_horizon_mask', 'a_dot_k_3d',
           'arpes_wavelength_scan', 'arpes_to_kspace', 'mirror_kspace', 'arpes_wvlscan_to_kspace']


def k0_sensitivity(k, k0: float, dk: float, integration: str = 'supergaussian') -> float:
    """
    Given an electron momentum k, a selection energy k0 and energy bandwidth dk (FWHM),
    calculate the weight at which that k contributes to the measurement

    :param k: abs momentum for which the sensitivity is being calculated
    :param k0: selected momentum value
    :param dk: integration bandwidth FWHM
    :param integration: 'gaussian', 'square' or 'supergaussian'
    :return:
    """
    methods = ['gaussian', 'square', 'rectangular', 'supergaussian']
    if integration == 'gaussian':
        sigma = dk / 2.355  # 2.355 = 2*np.sqrt(2*np.log(2)), converts FWHM to sigma
        return np.exp(-(k - k0) ** 2 / (2 * sigma ** 2)) / (np.sqrt(2 * np.pi) * sigma)
    elif integration in ['square', 'rectangular']:
        if (k - k0) >= -dk / 2 and k - k0 < dk / 2:
            return 1 / dk
        else:
            return 0
    elif integration == 'supergaussian':
        # use e^(-k**6), with numerically calculated normalization constants
        return np.exp(-1 * ((k - k0) * 2 * 0.9406 / dk) ** 6) / (dk * 0.986306)
    else:
        raise ValueError('Other integration methods will need to be developed. Accepted methods are ' + str(methods))


def k0_sensitivity_values(k0: float, dk: float, ksampling: float = 0.01,
                          integration: str = 'supergaussian',
                          k_max=None):
    """
    Given an electron momentum k, a selection energy k0 and energy bandwidth dk (FWHM),
    calculate the weight at which that k contributes to the measurement for a range of k-values

    :param k0: selected momentum value
    :param dk: integration bandwidth FWHM
    :param ksampling: stepsize for the vector
    :param integration: 'gaussian', 'square' or 'supergaussian'
    :param k_max: highest k-value which is calculated
    :return: list of sensitivity values for k in (0, maxradius) in steps ksampling
    """
    if k_max is None:
        k_max = 1.5 * k0 + 5 * dk
    return np.array([k0_sensitivity(k, k0, dk, integration=integration)
                     for k in np.arange(0, k_max, ksampling)])


def kxkykz_sensitivity(array_shape: tuple, k0: float, dk: float, integration: str = 'supergaussian',
                       interpolate: int = 1) -> np.ndarray:
    """
    Calculate the kx,ky,kz dependent sensitivity of the ARPES measurement based on a certain energy
    bandwidth integration.

    :param array_shape: (ny,nx,nz) shape of the output, or (ny,nx) shape of the momentum space, in which case nz will be
                  guessed. (also accepts arrays of wanted size, 2D or 3D)
    :param k0: selected momentum value
    :param dk: integration bandwidth
    :param integration: 'gaussian', 'square' or 'supergaussian'
    :param interpolate: factor subsampling in the calculation of the k-sensitivity (e.g. 2, 4, ...)
    :return: 3d array to multiply the raw spectrum by
    """
    try:
        _shape = array_shape.shape
    except AttributeError:
        _shape = array_shape
    if len(_shape) == 2:
        ny, nx, nz = _shape + (int(k0 + 5 * dk),)
    else:
        ny, nx, nz = _shape
        assert nz >= int(k0 + 5 * dk), 'Array size in z-direction needs to be at least int(k0 + 5 * dk)'
    k_samp = dk / (5 * interpolate)  # 0.02
    sens_vals = k0_sensitivity_values(k0, dk, ksampling=k_samp, integration=integration,
                                      k_max=1.2 * np.sqrt(0.25 * nx ** 2 + 0.25 * ny ** 2 + nz ** 2))
    k_vec_x = np.arange(nx * interpolate) - int(nx * interpolate / 2)
    k_vec_y = np.arange(ny * interpolate) - int(ny * interpolate / 2)
    k_vec_z = np.arange(nz * interpolate)
    radius_vals = np.sqrt(np.sum(np.array(np.meshgrid(k_vec_x, k_vec_y, k_vec_z)) ** 2, axis=0))
    radius_vals /= interpolate

    full_cube = sens_vals[(radius_vals / k_samp).astype(np.int)]

    if interpolate != 1:
        full_cube = binning.bin_array(full_cube, (ny, nx, nz))

    return full_cube


def kxky_sensitivity(kxky_shape: tuple, k0: float, dk: float, grazing_incidence_angle: float = 0.0,
                     xy_incidence_angle: float = 0., integration: str = 'supergaussian',
                     with_a_dot_k: bool = True, interpolate: int = 1) -> np.ndarray:
    """
    Calculate the kx,ky dependent sensitivity of the ARPES measurement based on a certain energy bandwidth integration.
    For proper integration function, should be approximately 1 for kx==ky==0,
    high for the edges of the view and zero outside.

    :param kxky_shape: integer size (ny,nx) of the momentum space (or example array, will take same size)
    :param k0: selected momentum value
    :param dk: integration bandwidth
    :param grazing_incidence_angle: angle of incidence of the beam (assuming along x-direction and with p-polarization)
    :param xy_incidence_angle: angle of the beam in the xy plane
    :param with_a_dot_k: Include the polarization-induced sensitvity
    :param integration: 'gaussian', 'square' or 'supergaussian'
    :param interpolate: subsampling in the calculation of the k-sensitivity - passes on to kxkykz_sensitivity
    :return: 2d array specifying sensitivity to each ky,kx point
    """
    integration_band = kxkykz_sensitivity(kxky_shape, k0, dk, integration=integration,
                                          interpolate=interpolate)
    if with_a_dot_k:
        sensitivity = integration_band * a_dot_k_3d(kxkykz_shape=integration_band.shape,
                                                    grazing_incidence_angle=grazing_incidence_angle,
                                                    xy_incidence_angle=xy_incidence_angle) ** 2
    else:
        sensitivity = integration_band
    return np.sum(sensitivity, axis=2)


def hemisphere_from_kspace(data, k0: float, dk: float, integration: str = 'supergaussian',
                           kzero_in_center: bool = True, mask_interpolation: int = 1,
                           project_to_plane: bool = True) -> np.ndarray:
    """
    Calculate photoemission dataset by slicing a hemisphere out of a full k-space dataset

    :param data: 3d numpy array (e.g. abs(fft of orbital)**2 )
    :param k0: selected momentum value
    :param dk: integration bandwidth
    :param integration:
    :param kzero_in_center: boolean, whether the kx=ky=kz=0 value is located in the center of the array or at 0,0,0
    :param mask_interpolation: integer n, calculate sensitivity mask on a n-times finer grid and then bin the result
    :param project_to_plane: if true, return 2d data, else 3d
    :return: 3d dataset with data on measured hemisphere
    """
    selector = kxkykz_sensitivity(data.shape[:2], k0, dk, integration=integration, interpolate=mask_interpolation)
    nz_select = selector.shape[2]
    z0_data = int(data.shape[2] / 2)
    if not kzero_in_center:
        _data = fftshift(data)[:, :, z0_data:z0_data + nz_select]
    else:
        _data = data[:, :, z0_data:z0_data + nz_select]
    if project_to_plane:
        return np.sum(_data * selector, axis=2)
    else:
        return _data * selector


def noisy_arpes(data: np.ndarray, k0: float, dk: float, n_counts: int, grazing_incidence_angle: float = 0.0,
                xy_incidence_angle: float = 0,
                integration: str = 'supergaussian',
                kzero_in_center: bool = True, background_counts: float = 0.0,
                sensitivity_correction: bool = True, sensitivity_threshold: float = 1e-3,
                binning: int = 1, inputdata_interpolation: float = 1) -> np.ndarray:
    """
    From a 3d k-space of an ARPES measurement system, return the amplitudes on a hemispherical slice with noisy

    :param data: k-space 3d numpy array, amplitudes (not intensity)
    :param k0: energy selection in units of the data resolution
    :param dk: energy integration bandwidth
    :param n_counts: total number of electrons in measurement (in the absence of noise, so results will vary)
    :param grazing_incidence_angle: grazing angle of incidence of the beam. Assuming along x and with p-polarization
    :param xy_incidence_angle: Incidence of the beam, 0 for along x from left, 90 for along y form top...
    :param integration: 'gaussian', 'square' or 'supergaussian'
    :param kzero_in_center: whether the kx=ky=kz=0 value is located in the center of the array or at 0,0,0
    :param background_counts: average number of counts on the background (homogeneous)
    :param sensitivity_correction: correct intensity for projection on the xy plane
    :param sensitivity_threshold: division by zero safeguard
    :param binning: integer for binning of the raw simlutated poissonian data
    :param inputdata_interpolation: useful when using small values for dk
    :return: sqrt of measured intensity
    """
    if inputdata_interpolation != 1:
        kspace = abs(interpolation.fourier_interpolate_3d(abs(data) ** 2, factor=inputdata_interpolation))
        k0 *= inputdata_interpolation
        dk *= inputdata_interpolation
    else:
        kspace = abs(data) ** 2
    hemispherical_slice = hemisphere_from_kspace(kspace, k0, dk,
                                                 integration=integration,
                                                 kzero_in_center=kzero_in_center)
    hemispherical_slice *= a_dot_k(kxky_shape=hemispherical_slice.shape, k0=k0,
                                   grazing_incidence_angle=grazing_incidence_angle,
                                   xy_incidence_angle=xy_incidence_angle) ** 2
    noisy = noisify_intensity(hemispherical_slice,
                              total_counts=n_counts, background_counts=background_counts)
    if binning != 1:
        noisy = binning.bin_array(noisy, tuple([int(i / binning) for i in noisy.shape]))
    if sensitivity_correction:
        sensitivity = kxky_sensitivity(hemispherical_slice.shape, k0, dk,
                                       grazing_incidence_angle=grazing_incidence_angle,
                                       xy_incidence_angle=xy_incidence_angle,
                                       integration=integration, with_a_dot_k=True)
        if binning != 1:
            sensitivity = binning.bin_array(sensitivity,
                                                            tuple([int(i / binning) for i in sensitivity.shape]))
        # mask = (sensitivity > sensitivity_threshold).astype(np.uint)
        noisy = noisy / np.sqrt(sensitivity ** 2 + sensitivity_threshold ** 2)
        # noisy *= mask
    measured_amplitude = np.sqrt(noisy)
    return measured_amplitude


def a_dot_k(kxky_shape: tuple, k0: float, grazing_incidence_angle: float, xy_incidence_angle: float = 0,
            normalize_k0: bool = False) -> np.ndarray:
    """
    Calculate the strength of the A.k operator for photoemission

    :param kxky_shape: grid dimensions
    :param k0: maximum momentum in grid units
    :param grazing_incidence_angle: angle of incidence in degrees
    :param xy_incidence_angle: purely along x by default
    :param normalize_k0: if true, calculate A.k/k0, else, A.k
    :return: array with sensitivities
    """
    cx, cy = kxky_shape[0] / 2, kxky_shape[1] / 2
    a = np.array([np.sin(grazing_incidence_angle * (np.pi / 180)) * np.cos(xy_incidence_angle * (np.pi / 180)),
                  np.sin(grazing_incidence_angle * (np.pi / 180)) * np.sin(xy_incidence_angle * (np.pi / 180)),
                  np.cos(grazing_incidence_angle * (np.pi / 180))])

    mask = photoemission_horizon_mask(kxky_shape=kxky_shape, k0=k0)
    k_vec_x = mask * np.fromfunction(lambda x, y: (x - cx) / k0, kxky_shape)
    k_vec_y = mask * np.fromfunction(lambda x, y: (y - cy) / k0, kxky_shape)
    k_vec_z = mask * np.sqrt(1 - k_vec_x ** 2 - k_vec_y ** 2)
    k_vec = np.array([k_vec_x, k_vec_y, k_vec_z]).T
    if not normalize_k0:
        k_vec *= k0
    k_dot_a = abs(np.dot(k_vec, a))
    return k_dot_a


def photoemission_horizon_mask(kxky_shape: tuple, k0: float) -> np.ndarray:
    """
    Circular mask of ones and zeros to indicate where kx**2 + ky**2 < k0**2
    """
    cx, cy = kxky_shape[0] / 2, kxky_shape[1] / 2
    mask = np.fromfunction(np.vectorize(lambda kx, ky: 1 if np.sqrt((kx - cx) ** 2 + (ky - cy) ** 2) < k0 else 0),
                           kxky_shape)
    return mask


def a_dot_k_3d(kxkykz_shape: tuple, grazing_incidence_angle: float, xy_incidence_angle: float = 0):
    """
    Calculate the strength of the A.k operator for photoemission over the full (z>0) 3d domain

    :param kxkykz_shape: (y,x,z) grid dimensions
    :param grazing_incidence_angle: angle of incidence in degrees
    :param xy_incidence_angle: purely along x by default
    :return: array with A.k values
    """
    cx, cy = kxkykz_shape[0] / 2, kxkykz_shape[1] / 2
    a = np.array([np.sin(grazing_incidence_angle * (np.pi / 180)) * np.cos(xy_incidence_angle * (np.pi / 180)),
                  np.sin(grazing_incidence_angle * (np.pi / 180)) * np.sin(xy_incidence_angle * (np.pi / 180)),
                  np.cos(grazing_incidence_angle * (np.pi / 180))])

    k_vec_x = np.arange(kxkykz_shape[1]) - cx
    k_vec_y = np.arange(kxkykz_shape[0]) - cy
    k_vec_z = np.arange(kxkykz_shape[2])
    k_vec = np.array(np.meshgrid(k_vec_x, k_vec_y, k_vec_z)).T
    k_dot_a = abs(np.dot(k_vec, a)).T
    return k_dot_a


def arpes_wavelength_scan(data: np.ndarray, k0_vals: tuple, dk: float, total_counts: int,
                          grazing_incidence_angle: float = 0.0, xy_incidence_angle: float = 0,
                          background_counts: float = 0.0,
                          sensitivity_correction: bool = True, sensitivity_threshold: float = 1e-3,
                          binning: int = 1, inputdata_interpolation: float = 1, **kwargs):
    """
    From a 3d k-space of an ARPES measurement system, sample noisy measurements for a range of momenta.
    Data are normalized to the matrix elements, i.e. equal HHG flux and measurement time

    :param data: k-space 3d numpy array, amplitudes (not intensity)
    :param k0_vals: energies at which to sample/measure in units of the data resolution
    :param dk: energy integration bandwidth
    :param total_counts: total number of electrons in measurement (in the absence of noise, so results will vary)
    :param grazing_incidence_angle: grazing angle of incidence of the beam. Assuming along x and with p-polarization
    :param xy_incidence_angle: Incidence of the beam, 0 for along x from left, 90 for along y form top...
    :param background_counts: average number of counts on the background (homogeneous)
    :param sensitivity_correction: correct intensity for projection on the xy plane
    :param sensitivity_threshold: division by zero safeguard
    :param binning: integer for binning of the raw simulated poissonian data
    :param inputdata_interpolation: useful when using small values for dk
    :return: noise-free amplitudes, simulated sqrt of measured intensity
    """
    if inputdata_interpolation != 1:
        kspace = abs(interpolation.fourier_interpolate_3d(abs(data) ** 2, factor=inputdata_interpolation))
    else:
        kspace = abs(data) ** 2
    _k0_vals = k0_vals * inputdata_interpolation
    _dk = dk * inputdata_interpolation

    noise_free = []
    for k0 in _k0_vals:
        hemispherical_slice = hemisphere_from_kspace(kspace, k0, _dk, project_to_plane=False, **kwargs)
        hemispherical_slice *= a_dot_k_3d(kxkykz_shape=hemispherical_slice.shape,
                                          grazing_incidence_angle=grazing_incidence_angle,
                                          xy_incidence_angle=xy_incidence_angle) ** 2
        noise_free += [np.sum(hemispherical_slice, axis=2)]

    noisy = noisify_intensity(np.array(noise_free),
                              total_counts=total_counts,
                              background_counts=background_counts)
    corrected_amps = []
    for index, noisy_map in enumerate(noisy):
        if binning != 1:
            noisy_map = binning.bin_array(noisy_map, tuple([int(i / binning) for i in noisy_map.shape]))
        if sensitivity_correction:
            sensitivity = kxky_sensitivity(tuple([sh * binning for sh in noisy_map.shape]), _k0_vals[index], _dk,
                                           grazing_incidence_angle=grazing_incidence_angle,
                                           xy_incidence_angle=xy_incidence_angle,
                                           with_a_dot_k=True, **kwargs)
            if binning != 1:
                sensitivity = binning.bin_array(sensitivity,
                                                                tuple([int(i / binning) for i in sensitivity.shape]))
            # mask = (sensitivity > sensitivity_threshold).astype(np.uint)
            noisy_map = noisy_map / np.sqrt(sensitivity ** 2 + sensitivity_threshold ** 2)
            # noisy *= mask
        corrected_amps += [np.sqrt(noisy_map)]
    return [np.sqrt(mp) for mp in noise_free], corrected_amps


def arpes_to_kspace(arpes: np.ndarray, k0: float, dk: float, sensitivity_threshold=0.1, **kwargs):
    """
    Cast the 2d hemispherical ARPES measurement to a 3D momentum array, setting np.nan for unknown values

    :param arpes: ARPES measurement
    :param k0: full photoelectron momentum for this measurement (in pixels)
    :param dk: required momentum resolution (~energy resolution,
               or for high-resolution measurements, a too large value.) Typically 0.25-1 pixel
    :param sensitivity_threshold: at which point to cut of the sensitivity, i.e. how many points to fill in kspace
    :param kwargs: passed on to kxkykz_sensitivity,
    :return: 3d numpy array with measurement values, array with sensitvities
    """
    shell = kxkykz_sensitivity(kwargs.pop('shape', arpes.shape), k0, dk, **kwargs)
    filled_shell = np.moveaxis(np.moveaxis(shell, -1, 0) * arpes / np.sqrt(np.sum(shell, axis=2) ** 2 + 1e-15),
                               0, -1)
    nans = np.where(shell > sensitivity_threshold, shell, np.nan)
    return filled_shell / nans, shell


def arpes_wvlscan_to_kspace(arpes_patterns: tuple, k0_values: tuple, dk: float,
                            sensitivity_threshold: float = 0.1, **kwargs) -> tuple:
    """
    Cast several 2d hemispherical ARPES measurements to a 3D momentum array, setting np.nan for unknown values

    :param arpes_patterns: list of ARPES measurements
    :param k0_values: list of full photoelectron momenta for this measurement (in pixels)
    :param dk: required momentum resolution (~energy resolution,
               or for high-resolution measurements, a too large value.) Typically 0.25-1 pixel
    :param sensitivity_threshold: at which point to cut of the sensitivity, i.e. how many points to fill in kspace
    :param a measurement_keymap: if true, create an array with integers indicating which measurement covers which kpoints.
    :param kwargs: passed on to arpes_to_kspace, kxkykz_sensitivity, (e.g. shape = (ny,nz,nz))
    :return: tuple:
             - 3d Numpy array with measurement values,
             - Array with sensitivities: np.nan for sensitivity below theshold
             - A measurement keymap: integer indicating k-points which were measured together.
             Can be used to isolate and manipulate individual measurements
    """
    sorting = np.argsort(k0_values)[::-1]  # sort by greatest k value first
    _k0_values = np.array(k0_values)[sorting]
    data = np.array(arpes_patterns)[sorting]
    # put the first shell in the 3d array
    kspace, sens = arpes_to_kspace(data[0], _k0_values[0], dk, sensitivity_threshold=sensitivity_threshold,
                                   **kwargs)
    keymap = np.where(sens > sensitivity_threshold, 1, 0)  # 1 for first measurement, 0 for not measured
    kwargs['shape'] = kspace.shape  # following shells should be put in the same array size
    for i in range(1, len(arpes_patterns)):  # place all shells in the array
        kspace_part, shell = arpes_to_kspace(data[i], _k0_values[i], dk,
                                             sensitivity_threshold=sensitivity_threshold, **kwargs)
        kspace = np.nansum([kspace, kspace_part], axis=0)
        sens += shell
        keymap += np.where(shell > sensitivity_threshold, i+1, 0)  # 1 i+1 for the measurement number
    sens_nan = np.where(sens > sensitivity_threshold, sens, np.nan)
    return kspace, sens_nan, keymap


def mirror_kspace(kspace: np.ndarray, sensitivity: np.ndarray = None, shift_mirror: tuple = (1, 1, 1),
                  square_array: bool = True):
    """
    Mirror a 3d kspace array in the kz=0 plane.

    :param kspace: kspace array, e.g. from arpes_wvlscan_to_kspace
    :param sensitivity: if given (from arpes_wvlscan_to_kspace), test
    :param shift_mirror: array rolling done to get good centering. is tested by sensitivity
    :param square_array: pad array to be square
    :return: 3d array
    """

    def mirror_array(arr: np.ndarray, roll=shift_mirror) -> np.ndarray:
        arr = np.concatenate([np.roll(np.flip(arr), (roll[0], roll[1]), axis=(0, 1)), arr[:, :, 1:]],
                             axis=2)
        arr = np.roll(arr, roll[2], axis=2)
        return arr

    full_kspace = mirror_array(kspace)
    if square_array:
        if np.any(np.isnan(full_kspace)):
            cv = np.nan
        else:
            cv = 0
        full_kspace = pad_to_square(full_kspace, constant_values=cv)
    if sensitivity is None:
        return full_kspace
    else:
        mirrored_sens = mirror_array(sensitivity)
        if np.any(np.isnan(mirrored_sens)):
            cv = np.nan
        else:
            cv = 0
        mirrored_sens = pad_to_square(mirrored_sens, constant_values=cv)
        testslice = [len(mirrored_sens) // i for i in [2, 3, 4]]
        test_array = np.where(np.isnan(mirrored_sens), 0, 1)
        for tsl in testslice:
            testslices = [test_array[tsl],
                          test_array[:, tsl],
                          test_array[:, :, tsl],
                          test_array[-tsl],
                          test_array[:, -tsl],
                          test_array[:, :, -tsl]]
            test_res = []
            for a, b in combinations(testslices, 2):
                test_res += [np.all(np.isclose(a, b))]
            assert np.all(
                test_res), 'Non-matching test slices, indicating that shift_mirror parameter should be changed'
        return full_kspace, mirrored_sens

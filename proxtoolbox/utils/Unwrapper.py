"""              Unwrapper.py
            written on Sep 9 by
            Constantin Höing
            Inst. Fuer Numerische und Angewandte Mathematik
            Universität Göttingen

    Description: < Description >
    Input: Xi_A : numpy array
            theta: skalar variable (float or int)

    Output: numpy array
    Usage: res = Unwrapper(Xi_A, theta) """
    
    
    
#from math import pi 
from numpy import zeros, ndenumerate, pi, array
import numpy as np
from copy import copy
    
def Unwrapper(Xi_A, theta):
    pi2 = 2* pi
    thresh= pi
    theta = Xi_A * theta
    res = length(Xi_A) +2
    
    
    #Xi_A must be full
    temp = zeros((res, res))
    temp[1:res-1, 1:res-1] = Xi_A # -1       ?????????????
    Xi_A = copy(temp)
    
    temp[1:res-1, 1:res-1] = theta
    #print(Xi_A)
    theta = temp
    #print(theta)
    
    J = find(Xi_A.T, 0, "!=")
    #print(J)
    istart = np.ceil(J[0]/res)
    istop = np.ceil(J[length(J)-1]/res)
    Maxit = 40
    Iter = 0
    #
    for i in range(int(istart)-1, int(istop)):
        J = find(Xi_A[i,:], 0, "!=")
        #print(J)
        if 0 not in np.shape(J):
            lJ = length(J)
            I2 = J[1:lJ] - J[0:lJ-1]
            #print(I2)
            I2 = find(I2, 0, ">")
            #print(I2)
            #print([0] + list(I2) +[lJ])
            I2 = [0] + list(I2) +[lJ]
            #print(I2)
            lI2 = length(I2) -1# !!!!!
            #print(lI2)
            
            for i2 in range(1, lI2):  # !!!!!!!
                jstart=J[I2[i2-1]]
                #print(jstart)
                jstop = J[I2[i2]-1]
                #print(jstop)
                lI = 1
                Iter = 0
                
                jump = theta[i, jstart:jstop] - theta[i, jstart-1:jstop-1]
                I = find(abs(jump), thresh, ">=")
                #print(I)
                lI = length(I)
                #print(lI)
                if len(I) != 0:
                    jump = np.sign(jump[I])*pi2
                else:
                    jump = []
                I = jstart-1+I
                
                while Iter < Maxit and lI != 0:
                    if lI >0:
                        j = 0
                        while j >= 1 and j <= lI -1:
                            theta[i, I[j]:I[j+1]] = theta[i, J[i]:I[j+1]] - jump[j]
                            #print(theta)
                            if jump[j] + jump[j+1] == 0:
                                j += 2
                            else:
                                j += 1
                        
                        if lI == j:
                            theta[i, I[lI]: jstop] = theta[i, I[lI]: jstop-1]
                        
                    
                    jump = theta[i, jstart+1:jstop] - theta[i, jstart:jstop-1]
                    I = find(abs(jump), thresh, ">=")
                    lI = length(I)
                    
                    if len(I) != 0:
                        jump = np.sign(jump[I])*pi2
                    else:
                        jump = []
                    I = jstart-1+I
                    Iter += 1
                
                Jref = find(Xi_A[i-1, jstart:jstop+1], 0, "!=")
                #print(Jref)
                #print(Xi_A[i-1, jstart:jstop+1])
                
                if len(Jref) != 0:
                    temp = int(np.ceil(length(Jref)/2))-1
                    print(temp)
                    print(Jref)
                    
                    jump = theta[i-1,jstart-1+Jref[temp]] - theta[i,jstart-1+Jref[temp]]
                    #jump = theta[i-1, jstart-1+Jref[temp]] -theta[i, jstart-1jstop] + jump*pi2
                    jump = round(jump/pi2)
                    
                    if jump != 0:
                        theta[i, jstart:jstop+1] = theta[i, jstart:jstop+1] + jump*pi2
                    
                    
    return theta[1:res-1, 1:res-1]
                


            
    
    
    
    
    
    
    
def find(Array, ele=0, operator = "=="):
    L = []
    A = Array.flatten()
    
    
    for idx, val in ndenumerate(A):
        if eval( str(val)  + operator + str(ele)):
            
            L.append(idx[0])
    
    
    return array(L)


def length(arr):
    return max(np.shape(arr)) 






if __name__ == "__main__":
    from numpy import arange
    A = arange(9).reshape(3,3)
    print(Unwrapper(A, 2))
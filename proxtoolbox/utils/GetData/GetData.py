from pathlib import Path
import sys
import urllib.request
import tarfile
from zipfile import ZipFile
import shutil

datadir = Path(__file__).parent.parent.parent.parent / 'InputData'


# shows progress of download
def dlProgress(counter, blocksize, size):
    p = counter * blocksize * 100.0 / size
    sys.stdout.write("\rProgress: %d%%" % p)
    sys.stdout.flush()


# function to ask permission for donwload from user
def query_yes_no(question):
    choices = "[y/n] "
    valid_answer = {'y': True, 'ye': True, 'yes': True, 'n': False, 'ne': False, 'no': False}

    while True:
        sys.stdout.write(question + choices)
        answer = input().lower()
        if answer in valid_answer:
            return valid_answer[answer]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no'. \n")


# downloads the input data to the InputData folder. problemFamily has to be either Phase, CT or Ptychography
def getData(problemFamily):
    if problemFamily == 'Phase':
        my_file = datadir / 'Phase' / 'pupil.pmod'
    elif problemFamily == 'CT':
        my_file = datadir / 'CT' / 'ART_SheppLogan.mat'
    elif problemFamily == 'Ptychography':
        my_file = datadir / 'Ptychography' / 'gaenseliesel.png'
    elif problemFamily == 'OrbitalTomog':
        my_file = datadir / 'OrbitalTomog' / 'coronene_homo1.tif'
    elif problemFamily == 'Elser':
        my_file = datadir / 'Elser' / 'data100E'
    else:
        print("Invalid input in GetData.GetData. problemFamily has to be Phase, CT or Ptychography")
        return -1

    if not (my_file.is_file()):
        print(problemFamily + " input data is missing.")
        if query_yes_no("Do you want to download the " + problemFamily + " input data?"):
            if problemFamily == 'Elser':
                link = 'https://github.com/veitelser/phase-retrieval-benchmarks/archive/master.zip'
                urllib.request.urlretrieve(link, datadir / (problemFamily + '.zip'), reporthook=dlProgress)
                print("\nExtracting data...")
                with ZipFile(datadir / (problemFamily + '.zip'), 'r') as zipObj:
                    names = zipObj.namelist()
                    for name in names:
                        if name.startswith('phase-retrieval-benchmarks-master/data/data'):
                            zipObj.extract(name, datadir / problemFamily)
                            name_end = name.replace('phase-retrieval-benchmarks-master/data/', '')
                            shutil.move(datadir / problemFamily / name, datadir / problemFamily / name_end)
                shutil.rmtree(datadir / problemFamily / 'phase-retrieval-benchmarks-master/')
            else:
                link = " http://vaopt.math.uni-goettingen.de/data/" + problemFamily + ".tar.gz"
                urllib.request.urlretrieve(link, datadir / (problemFamily + '.tar.gz'), reporthook=dlProgress)
                print("\nExtracting data...")
                tar = tarfile.open(datadir / (problemFamily + '.tar.gz'), "r:gz")
                tar.extractall(datadir / problemFamily)
                tar.close()
        if not (my_file.is_file()):
            print('***************************************************************************************')
            print('* Input data still missing.  Please try automatic download again or manually download *')
            print('*     http://vaopt.math.uni-goettingen.de/data/' + problemFamily + '.tar.gz                           *')
            print('* Save and unpack the ' + problemFamily + '.tar.gz datafile in the                                    *')
            print('*    ProxMatlab/InputData subdirectory                                                *')
            print('***************************************************************************************')

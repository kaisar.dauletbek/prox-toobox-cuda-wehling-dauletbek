import cupy as cp

# class CudaCell(cp.ndarray):
#     """
#     Array for numerical data (inherits from CuPy ndarray).
#     This is similar to cells in Matlab but for numerical data and using CuPy for GPU acceleration.
#     """

#     def __new__(subtype, shape, buffer=None, offset=0,
#                 strides=None, order=None):
#         # CuPy doesn't support object data types, so we use a numerical type like float32 or float64
#         dtype = cp.float32  # Or use a different dtype as per your data
#         obj = super(CudaCell, subtype).__new__(subtype, shape, dtype,
#                                               buffer, offset, strides,
#                                               order)
#         return obj



class CudaCell(cp.ndarray):
    """
    A CuPy-based version of the Cell class for storing numerical data on the GPU.
    """

    def __new__(shape, dtype=cp.float32):
        # Use cp.empty or cp.zeros to initialize the array
        obj = cp.empty(shape, dtype)
        return obj



def isCudaCell(u):
    """
    Test if object `u` is an instance of GpuCell class.

    Parameters
    ----------
    u : any object
            
    Returns
    -------
    bool
        True if 'u' is an instance of GpuCell class, False otherwise.
    """
    return isinstance(u, CudaCell)

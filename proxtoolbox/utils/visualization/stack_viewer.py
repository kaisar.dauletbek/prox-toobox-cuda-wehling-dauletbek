import matplotlib.pyplot as plt
from matplotlib import get_backend
from numpy import max, min, iscomplexobj
from matplotlib.widgets import Slider, RadioButtons
from warnings import warn
from .complex_field_visualization import complex_to_rgb

__all__ = ['SingleStackViewer', 'XYZStackViewer']
good_backends = ['Qt5Agg', 'TkAgg', 'Qt4Agg', 'nbAgg']


class SingleStackViewer:
    def __init__(self, volume,
                 cmap: str = None,
                 clim: tuple = (None, None),
                 name: str = None,
                 show_colorbar=True,
                 fix_clim=True):
        """
        Create window showing a slice of the 3d data, allows to scan the slice position using j, k keys.
        Note that k will also change the x-axis if the mouse is hovering over the window.

        :param volume: real-valued np.ndarray (3D) or 4d if last axis is RGB
        :param cmap: e.g. 'seismic', 'viridis' or 'gray'
        :param clim: min, max of the color scale. Defaults to min, max of data, unless seismic, where 0=white
        :param name: Figure name, controls title of the window (must be unique, otherwise overwrites)
        :param show_colorbar: plot colorbar or not
        :param fix_clim: Fix colorscale during operation, if false, set clim to min,max of data
        """
        if get_backend() not in good_backends:
            warn(Warning('Current matplotlib backend may not allow for optimal funcionality! Use, e.g., Qt'))
        if volume.ndim == 4 and volume.shape[-1] == 3:
            rgb_plot = True
        else:
            rgb_plot = False

        self.fig, ax = plt.subplots(num=name)
        ax.volume = volume
        ax.index = volume.shape[0] // 2
        maxval = max(abs(volume))
        if cmap is None:
            self.cmap = 'seismic'
        else:
            self.cmap = cmap
        if clim[0] is None and self.cmap == 'seismic':
            self.clim = (-maxval, maxval)
        elif clim is None:
            self.clim = (min(volume), max(volume))
        else:
            self.clim = clim
        im = ax.imshow(volume[ax.index], cmap=self.cmap, vmin=self.clim[0], vmax=self.clim[1])
        ax.set_title('Use [j,k] to navigate')
        self.fig.canvas.mpl_connect('key_press_event', self.process_key)

        self.fig.subplots_adjust(bottom=0.2, right=0.85)
        if show_colorbar and not rgb_plot:
            cbar_ax = self.fig.add_axes([0.84, 0.23, 0.03, 0.6])
            self.cbar = plt.colorbar(im, cax=cbar_ax)
            self.fix_clim = fix_clim

        axcolor = None  # 'lightgoldenrodyellow'
        sliderlength = 0.6  # Relative length to window size
        limit_slider = 0  # Allow full range
        subax1 = self.fig.add_axes([0.2, 0.1, sliderlength, 0.03], facecolor=axcolor)
        self.ax1slider = Slider(subax1, 'Slice', 0 + limit_slider, -1 + len(ax.volume) - limit_slider,
                                valinit=ax.index, valstep=1, valfmt='%d')
        self.ax1slider.on_changed(self.update_by_slider)

    def process_key(self, event):
        """Look for special events captured by mpl_connect, send to correct function """
        fig = event.canvas.figure
        ax = fig.axes[0]
        if event.key == 'j':
            self.previous_slice(ax)
        elif event.key == 'k':
            self.next_slice(ax)
        fig.canvas.draw()

    def previous_slice(self, ax):
        """Go to the previous slice."""
        volume = ax.volume
        ax.index = (ax.index - 1) % volume.shape[0]  # wrap around using %
        ax.images[0].set_array(volume[ax.index])
        self.update_clim(volume[ax.index])
        self.ax1slider.set_val(ax.index)

    def next_slice(self, ax):
        """Go to the next slice."""
        volume = ax.volume
        ax.index = (ax.index + 1) % volume.shape[0]
        ax.images[0].set_array(volume[ax.index])
        self.update_clim(volume[ax.index])
        self.ax1slider.set_val(ax.index)

    def update_by_slider(self, n):
        """update slice number after change of the slider"""
        ax = self.fig.axes[0]
        ax.images[0].set_array(ax.volume[int(n)])
        self.update_clim(ax.volume[int(n)])
        self.fig.canvas.draw_idle()

    def update_clim(self, data):
        """Update the colorbar"""
        if data.ndim == 2 and not self.fix_clim:
            if not self.cmap == 'seismic':
                self.clim = (min(data), max(data))
            else:
                maxval = max(abs(data))
                self.clim = (-maxval, maxval)
            self.cbar.set_clim(vmin=self.clim[0], vmax=self.clim[1])
            self.cbar.draw_all()
        else:
            pass


class XYZStackViewer:
    def __init__(self, volume,
                 limit_sliders: tuple = (0, 0, 0),
                 cmap: str = None,
                 clim: tuple = (None, None),
                 data_transform: callable = None,
                 name: str = None,
                 set_aspect: tuple = (None, None, None)):
        """
        Dynamic matplotlib plot showing slices out of a 3d dataset

        :param volume: 3d numpy array
        :param limit_sliders: sliders allow range given by [limit, N-limit], where N is the maximal length
        :param cmap: color map to use (e.g. 'seismic' or 'viridis'). For 'seismic', will center on 0
        :param clim: tuple with the min-max values of the colorscale
        :param data_transform: simple transformation function to apply to data before plot.
                               defaults to abs() for complex data
        :param name: Set the name of the figure to a string.
        :param set_aspect: force set aspect ratio of plots
        """
        if get_backend() not in good_backends:
            warn(Warning('Current matplotlib backend may not allow for optimal funcionality! Use, e.g., Qt'))

        if volume.ndim == 4 and volume.shape[-1] in [3, 4]:
            rgb_plot = True
            show_colorbar = False
        else:
            # assert volume.ndim == 3, 'Functionality is designed for 3D arrays'
            rgb_plot = False
            show_colorbar = True

        radio_start = 0
        if data_transform is not None:
            self.cast_fn = data_transform
        elif iscomplexobj(volume):
            self.cast_fn = abs  # complex_to_rgb
            show_colorbar = True  # False
            rgb_plot = False  # True
            radio_start = 0  # 1
            if cmap is None:
                cmap = 'viridis'
        else:
            def dummy(array):
                return array

            self.cast_fn = dummy

        if cmap is None:
            cmap = 'seismic'
            show_colorbar = True

        self.volume = volume
        self.indices = [s // 2 for s in volume.shape]
        self.shape = self.volume.shape
        if clim[0] is not None:
            self.minval, self.maxval = clim
        elif cmap == 'seismic':
            self.maxval = max(abs(volume))
            self.minval = -1*self.maxval
        else:
            if not rgb_plot:
                self.maxval = max(self.cast_fn(volume))
                self.minval = min([0, min(self.cast_fn(volume))])
            else:
                self.minval, self.maxval = 0, 255

        self.fig, self.ax = plt.subplots(1, 3, figsize=(10, 4), num=name)
        im = self.ax[0].imshow(self.cast_fn(volume[self.indices[0], :, :]), cmap=cmap, vmin=self.minval,
                               vmax=self.maxval, aspect=set_aspect[0])
        im = self.ax[1].imshow(self.cast_fn(volume[:, self.indices[1], :]), cmap=cmap, vmin=self.minval,
                               vmax=self.maxval, aspect=set_aspect[1])
        im = self.ax[2].imshow(self.cast_fn(volume[:, :, self.indices[2]]), cmap=cmap, vmin=self.minval,
                               vmax=self.maxval, aspect=set_aspect[2])
        self.ax[1].set_yticks([])
        self.ax[2].set_yticks([])

        self.fig.subplots_adjust(bottom=0.1, right=0.85)
        if show_colorbar and not rgb_plot:
            cbar_ax = self.fig.add_axes([0.87, 0.2, 0.01, 0.6])
            plt.colorbar(im, cax=cbar_ax)

        axcolor = None  # 'lightgoldenrodyellow'
        sliderlength = 0.17
        subax1 = self.fig.add_axes([0.15, 0.1, sliderlength, 0.03], facecolor=axcolor)
        self.ax1slider = Slider(subax1, 'Slice', 0 + limit_sliders[0], -1 + self.shape[0] - limit_sliders[0],
                                valinit=self.indices[0], valstep=1, valfmt='%d')
        self.ax1slider.on_changed(self.update_1)

        subax2 = self.fig.add_axes([0.41, 0.1, sliderlength, 0.03], facecolor=axcolor)
        self.ax2slider = Slider(subax2, 'Slice', 0 + limit_sliders[1], -1 + self.shape[1] - limit_sliders[1],
                                valinit=self.indices[1], valstep=1, valfmt='%d')
        self.ax2slider.on_changed(self.update_2)

        subax3 = self.fig.add_axes([0.67, 0.1, sliderlength, 0.03], facecolor=axcolor)
        self.ax3slider = Slider(subax3, 'Slice', 0 + limit_sliders[2], -1 + self.shape[2] - limit_sliders[2],
                                valinit=self.indices[2], valstep=1, valfmt='%d')
        self.ax3slider.on_changed(self.update_3)

        if iscomplexobj(self.volume):
            self.fig.set_size_inches(10, 5)
            self.fig.subplots_adjust(top=0.95)
            rax = self.fig.add_axes([0.45, 0.75, 0.15, 0.2], facecolor=axcolor, frame_on=False)
            self.abs_label = 'Abs value'
            self.complexcolorlabel = 'Complex to color'
            self.radio = RadioButtons(rax, (self.abs_label, self.complexcolorlabel), active=radio_start)
            self.radio.on_clicked(self.set_cast_fn)

        self.fig.show()

    def update_1(self, n):
        """Update subfigure 1 on change of the slider """
        self.indices[0] = int(n)
        self.ax[0].images[0].set_array(self.cast_fn(self.volume[int(n), :, :]))
        self.fig.canvas.draw_idle()

    def update_2(self, n):
        """Update subfigure 2 on change of the slider """
        self.indices[1] = int(n)
        self.ax[1].images[0].set_array(self.cast_fn(self.volume[:, int(n), :]))
        self.fig.canvas.draw_idle()

    def update_3(self, n):
        """Update subfigure 3 on change of the slider """
        self.indices[2] = int(n)
        self.ax[2].images[0].set_array(self.cast_fn(self.volume[:, :, int(n)]))
        self.fig.canvas.draw_idle()

    def set_cast_fn(self, radio_label):
        if radio_label == self.abs_label:
            self.cast_fn = abs
        elif radio_label == self.complexcolorlabel:
            self.cast_fn = complex_to_rgb
        self.update_1(self.ax1slider.val)  # apply the change
        self.update_2(self.ax2slider.val)
        self.update_3(self.ax3slider.val)

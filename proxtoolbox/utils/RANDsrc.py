"""              RANDsrc.py
            written on September 2. 2019 by
            Constantin Höing
            Inst. Fuer Numerische und Angewandte Mathematik
            Universität Göttingen

    Description: < Description >
    Input:  m,n: shape of return numpy array
            alphabet: array of two values
            state: propability for values in alphabet

    Output: random numpy array of shape m,n
    Usage: randMatrix = RANDsrc(m ,n ,alphabet=[1,-1], state=[0.5, 0.5] )
    
    !!!!!!!!!!!!!!!!!!! has no error handling !!!!!!!!!!!!!!!
    
    """
    
    
    
from numpy import full, ndenumerate
from numpy.random import rand

def RANDsrc(m, n, alphabet=[1,-1], state=[0.5, 0.5]):
    
    #res = full((m, n), alphabet[0])
    res = rand(m, n)
    check1 = state[0]
    #check2 = state[1]
    
    for idx, val in ndenumerate(res):
        if val < check1:
            res[idx] = alphabet[0]
        elif val >= check1:
            res[idx] = alphabet[1]
            
    #print(res)
    return res
    
    
    
    
    

if __name__ == "__main__":
    RANDsrc(2, 3, [3, 4], [0,1])
"""              < div-grad >
            written on Okt 6.2022 by
            Russell Luke
            Inst. Fuer Numerische und Angewandte Mathematik
            Universität Göttingen

    Description: Finite difference operator (div) and its transpose (grad)
    Input: x: an array of function values on a cartesian grid (1D, 2d or 3D)
           scale: is the interval length (default is scale=1)

    Output: the value of the discrete div-grad operator.  No bounday conditions.
    Usage: grad_x = grad(x, scale) """
    

import numpy as np

def grad(x,scale):
    """
    Computes the gradient of an image x
    """
    if scale==None:
        scale=1

    s = x.shape
    dim=np.size(s)
    if dim==1:
        # x is a 1d array
        G = np.zeros((s, 1), x.dtype)
        G[:-1] = scale*(x[1:] - x[:-1])
    if dim==2:
        # x is a 2d array
        G = np.zeros((s[0], s[1], 2), x.dtype)
        G[:, :-1, 0] = scale*(x[:, 1:] - x[:, :-1])
        G[:-1, :, 1] = scale*(x[1:] - x[:-1])
    if dim==3:
        # x is a 3d array
        G = np.zeros((s[0], s[1], s[2], 3), x.dtype)
        G[:,:, :-1, 0] = scale*(x[:,:, 1:] - x[:,:, :-1])
        G[:,:-1, :, 1] = scale*(x[:,1:,:] - x[:,:-1,:])
        G[:-1,:, :, 2] = scale*(x[1:,:,:] - x[:-1,:,:])
    return G

def grad_T(G):
    """
    -div of G = transpose of grad
    """
    s = G.shape
    dim=np.size(s)
    I = np.zeros(s, G.dtype)
    if dim==1:
        I[:-1] -= G[:-1]
        I[1: ] += G[:-1]
    if dim==2:
        I[:, :-1] -= G[:, :-1, 0]
        I[:, 1: ] += G[:, :-1, 0]
        I[:-1]    -= G[:-1, :, 1]
        I[1: ]    += G[:-1, :, 1]
    if dim==3:
        I[:, :, :-1] -= G[:, :, :-1, 0]
        I[:, :, 1: ] += G[:, :, :-1, 0]
        I[:, :-1, :] -= G[:, :-1, :, 1]
        I[:, 1: ,:]  += G[:, :-1, :, 1]
        I[:-1, :, :] -= G[:-1, :, :, 2]
        I[1: ,:, :]  += G[:-1, :, :, 2]
    return I
    

"""              < Prog Name >
            written on Sep 12. by
            Constantin Höing
            Inst. Fuer Numerische und Angewandte Mathematik
            Universität Göttingen

    Description: Discrete Laplacian
    Input: f: is the discretized function
            l: is the interval length

    Output: the value of the discrete Laplacian excluding the boundary
    Usage: Lf = Laplacian(f, l) """
    
    





import numpy as np

def Laplacian(f, l):   

    [m,n] = np.shape(f)
    Lf = np.zeros((m,n))

    if m==1 or n==1:
        m=max(m,n)
        h=(l/m)**2
        Lf[1:m-1] = (f[0:m-2] + f[2:m] - 2*f[1:m-1])/h
    else:
        hx=np.ceil((l[0]/m)**2)
        hy=np.ceil((l[1]/n)**2)
        Lf[1:m-1,1:n-1]=(f[1:m-1,0:n-2]+f[1:m-1,2:n]+f[0:m-2,1:n-1]+f[2:m,1:n-1]-4*f[1:m-1,1:n-1])/(hy*hx)  
  
    return Lf
 
    
    
    
if __name__ == "__main__":
    A = np.arange(9).reshape(3,3)
    print(Laplacian(A, [1,2]))
    
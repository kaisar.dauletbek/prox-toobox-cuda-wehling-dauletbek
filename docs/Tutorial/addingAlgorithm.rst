Adding a new algorithm
-----------------------

The focus of this section is to describe how a new algorithm
class can be implemented.

.. contents::
   :local:

Creating a new algorithm class
+++++++++++++++++++++++++++++++

Such a class must be derived from the abstract :class:`Algorithm` class
and be put in the :file:`algorithms` folder.
Then, one has to override the method :meth:`evaluate()` which computes the
next iterate from a given iterate :attr:`u`. For instance, for the ADMM algorithm the 
:meth:`evaluate()` method reads:

.. code-block:: python
   :linenos:

   def evaluate(self, u):

      lmbda = self.computeRelaxation()
      u_new = u.copy()
      self.prox1.lmbda = lmbda
      u_new[0] = self.prox1.eval(u)
      self.prox2.lmbda = lmbda
      u_new[1] = self.prox2.eval(u_new)
      u_new[2] = self.lagrange_mult_update.eval(u_new)

      return u_new

The ADMM algorithm, like many others, uses a relaxation parameter
whose value may change as a function of the current number of iterations.
The :class:`Algorithm` class already provides the data members 
:attr:`lambda_0` and :attr:`lambda_max` which specify the range
of values of this relaxation parameter. :attr:`lambda_0` is the value
that will be used for the first iteration, while :attr:`lambda_max` is
the maximum value. The :class:`Algorithm` class also provides 
a the :attr:`lambda_switch` which controls how fast the relaxation 
parameter changes as the number of iterations increases. All these
parameters can be specified during the instanciation of the experiment
class.

The method :meth:`computeRelaxation()` used in line 3 computes
the value of the relaxation parameter for the given iteration.

When the algorithm uses a relaxation parameter, it is also helpful
to override the method :meth:`getDescription()` to provide a string of
characters that describes the algorithm. 
This string will be used later for generating titles for the 
graphical output.

.. code-block:: python
   :linenos:
    
   def getDescription(self):
      return self.getDescriptionHelper("\\lambda", self.lambda_0, self.lambda_max)

Most of the work is done by the method :meth:`getDescriptionHelper()` 
which generates a string based on the algorithm's name and, optionally,
the name of the relaxation parameter (first argument) and the values 
of :attr:`lambda_0` and :attr:`lambda_max`. 

The next and mandatory step is to add an import statement into the
file :file:`__init__.py` contained in the :file:`algorithms` folder.
For instance, for the ADMM algorithm, we have

.. code-block:: python
   :linenos:

   from .ADMM import *

This is required so that the algorithm can be instantiated by 
the experiment class.

Now, a given algorithm may require specific proximal operators.

Creating a new prox operator class
++++++++++++++++++++++++++++++++++

Such a class may derive from the abstract :class:`ProxOperator` class.
Then, one has to override the method :meth:`eval()` which evaluates 
the prox operator for a given iterate :attr:`u`. As an example, here is
is the code for the :class:`P_avg` prox operator class. This 
prox operator computes the projection onto the diagonal of 
a product space:

.. code-block:: python
   :linenos:
   
   class P_avg(ProxOperator):

      def __init__(self, experiment):
         self.n_prox = experiment.nProx
         self.Nx = experiment.Nx
         self.Ny = experiment.Ny
         self.Nz = experiment.Nz

      def eval(self, u, prox_index = None):
         if not isCell(u):
               k = self.n_prox # the number of prox mappings to average
               if self.Ny == 1:
                  u_avg = sum(u,0)/k
               elif self.Nx == 1:
                  u_avg = sum(u,1)/k
               elif self.Nz == 1:
                  u_avg = u.mean(2)
               else:
                  u_avg = u.mean(3)
         else:
               u_avg = zeros_like(u[0])
               N = len(u)
               for j in range(N):
                  u_avg = u_avg + u[j]
               u_avg = u_avg / N
         return u_avg

A prox operator usually relies on some data coming from the 
given experiment. This data is stored in the prox operator by 
the :meth:`__init__()` method.

Depending on the structure of the iterate, the :meth:`eval()`
method may need to consider if the iterate is a Cell object or 
a regular multidimenional array. Cells are typically used to
describe data in a product space formulation. 

As a rule, the Python file containing the prox operator class
must be contained in the :file:`proxoperators` folder.
Then, one has to add an import statement into the
file :file:`__init__.py` contained in the :file:`proxoperators` folder.

.. code-block:: python
   :linenos:

   from .P_avg import *

An iterate monitor class is used with any given algorithm. Its role is 
to compute the changes in the iterates and additional statistics. The 
default and simplest iterate monitor is given by the 
:class:`IterateMonitor` class. The :class:`FeasibilityIterateMonitor`
class is more involved and is appropriate for feasibility problems.
The type of iterate monitor used in an experiment is typically 
specified in the experiment's default parameters. Such a choice can
be overriden when the experiment is instantiated. 

Due to a specific data layout or special needs, it may be necessary
to implement a specific iterate monitor for a given algorithm.

Creating a new iterator monitor class
+++++++++++++++++++++++++++++++++++++

This is usually more work that implementing a given algorithm.
Here is the iterate monitor used for the ADMM algorithm:

.. code-block:: python
   :linenos:

   class ADMM_IterateMonitor(IterateMonitor):

      def __init__(self, experiment):
         super(ADMM_IterateMonitor, self).__init__(experiment)
         self.gaps = None
         self.shadows = None
         self.rel_errors = None
         self.product_space_dimension = experiment.product_space_dimension
         if hasattr(experiment, 'norm_data'):
               self.normM = np.sqrt(experiment.Nx*experiment.Ny)
         else:
               # won't normalize data to make it independent of array size
               self.normM = 1.0

      def preprocess(self, alg):
         """
         Allocate data structures needed to collect 
         statistics. Called before running the algorithm.
   
         Parameters
         ----------
         alg : instance of Algorithm class
               Algorithm to be monitored.
         """
         super(ADMM_IterateMonitor, self).preprocess(alg)

         # In ADMM, the iterate u is a cell of blocks of variables.
         # In the analysis of Aspelmeier, Charitha& Luke, SIAM J. Imaging
         # Sciences, 2016, only the step difference on the "dual" 
         # blocks of variables is monitored.

         self.u_monitor = self.u0.copy()

         # set up diagnostic arrays
         if self.diagnostic:
               self.gaps = self.changes.copy()
               self.gaps[0] = 999
               self.shadows = self.changes.copy()
               self.shadows[0] = sqrt(999)
               if self.truth is not None:
                  self.rel_errors = self.changes.copy()
                  self.rel_errors[0] = sqrt(999)
            
      def updateStatistics(self, alg):

         # 'u' is a cell of  three blocks of variables, primal, auxilliary and dual, 
         # each of these containing ARRAYS.  For ADMM, convergence of the
         # algorithm is determined only by the behavior of the 2nd and 3rd
         # blocks of variables (see Aspelmeier, Charitha& Luke, SIAM J. Imaging
         # Sciences, 2016).  The first block of primal variables is monitored as a 
         # "shadow sequence" in analogy with the shadow sequence of the 
         # Douglas-Rachford iteration. 

         u = alg.u_new
         prev_u = self.u_monitor
         tmp_change = 0
         normM = self.normM
         for j in range(1,3):
               # the first cell is the block (assumed single) of 
               # primal variables - assumed an array.  
               # All other cells are the dual blocks, stored as
               # cells or arrays
               if isCell(u[j]):
                  k = len(u[j])
                  _m, _n, p, q = size_matlab(u[j])
               else:
                  m, n, p, q = size_matlab(u[j])
                  k = self.n_product_Prox
               
               for kk in range(k):
                  if p == 1 and q == 1:
                     if isCell(u[j]):
                           # the next line was added on the hunch that a global phase
                           # rotation each iteration is making the algorithm look like
                           # it is converging more slowly than it really is
                           tmp_change += (norm(u[j][kk] - prev_u[j][kk])/normM)**2
                     else: # dealing with 1D arrays on the product space
                           if n == k:
                              tmp_change += (norm(u[j][:,kk] - prev_u[j][:,kk])/normM)**2
                           elif m == k:
                              tmp_change += (norm(u[j][kk,:] - prev_u[j][kk,:])/normM)**2
                  elif q == 1:
                     if isCell(u[j]): # this means cells of cells of 3D arrays...I hope not!
                           for jj in range(p):
                              # the next line was added on the hunch that a global phase
                              # rotation each iteration is making the algorithm look like
                              # it is converging more slowly than it really is
                              tmp_change += (norm(u[j][kk][:,:,jj] - prev_u[j][kk][:,:,jj])/normM)**2
                     else:
                           # we have a 3D array, the third dimension being the
                           # product space
                           tmp_change += (norm(u[j][:,:,kk] - prev_u[j][:,:,kk])/normM)**2
                  else: # cells of 4D arrays?!!!
                     for jj in range(q):
                           for pp in range(p):
                              tmp_change += (norm(u[kk][:,:,pp, jj] - prev_u[kk][:,:,pp, jj])/normM)**2
                  
         self.changes[alg.iter] = sqrt(tmp_change)

         if self.diagnostic:
               tmp_shadow =0
               rel_error = 0
               # looks for the gap.  We use this to hold the function values of the
               # ADMM objective.  This is problem/implementation specific and will 
               # be found in the drivers/*/ProxOperators subdirectories
               # where the problem families are defined.
               self.gaps[alg.iter] = self.calculateObjective(alg)

               # Next, we look at the progress of the primal sequence, or 'shadows' of the 
               # ADMM algorithm.  These are just the steps in the primal sequence
               _m, _n, p, q = size_matlab(u[0])
               if p == 1 and q == 1:
                  # the next line was added on the hunch that a global phase
                  # rotation each iteration is making the algorithm look like
                  # it is converging more slowly than it really is
                  tmp_shadow += (norm(u[0] - prev_u[0])/normM)**2
                  if self.truth is not None:
                     z = u[0]
                     if z.ndim == 1:
                           z = reshape(z, (len(z),1))  # we want a true matrix not just a vector
                     rel_error += norm(self.truth - exp(-1j*angle(trace(matmul(self.truth.T.conj(), z)))) * z) / self.norm_truth
               elif q == 1:
                  for jj in range(p):
                     tmp_shadow += (norm(u[0][:,:,jj] - prev_u[0][:,:,jj])/normM)**2
                     if self.truth is not None:
                           rel_error += norm(self.truth - exp(-1j*angle(trace(matmul(self.truth.T.conj(), u[0][:,:,jj]))))) / self.norm_truth
               else: # cells of 4D arrays?!!!
                  for jj in range(q):
                     for k in range(p):
                           tmp_shadow += (norm(u[0][:,:,k,jj] - prev_u[0][:,:,k,jj])/normM)**2
                           if self.truth is not None:
                              rel_error += norm(self.truth - exp(-1j*angle(trace(matmul(self.truth.T.conj(), u[0][:,:,k,jj]))))) / self.norm_truth
               # end shadow sequence monitor

               # The following is the Euclidean norm of the gap to
               # the unregularized set.  To monitor the Euclidean norm of the gap to the
               # regularized set is expensive to calculate, so we use this surrogate.
               # Since the stopping criteria is on the change in the iterates, this
               # does not matter.
               self.shadows[alg.iter] = sqrt(tmp_shadow)
               if self.truth is not None:
                  self.rel_errors[alg.iter] = rel_error

         self.u_monitor = u.copy()

      def postprocess(self, alg, output):
         output = super(ADMM_IterateMonitor, self).postprocess(alg, output)
         if self.diagnostic:
               stats = output['stats']
               stats['gaps'] = self.gaps[1:alg.iter+1]
               stats['shadows'] = self.gaps[1:alg.iter+1]
               if self.truth is not None:
                  stats['rel_errors'] = self.rel_errors[1:alg.iter+1]
         return output

There are essentially three methods to override.
The :meth:`preprocess()` method allocates data structures needed to collect 
statistics. This method is called before the algorithm runs. Then, most of
the work is done in the :meth:`updateStatistics()` method. This method is 
called after each iteration to collect the statistics and fill the previously
allocated arrays. Finally, the :meth:`postprocess()` method stores statistics in
the given :attr:`output` dictionary. This method is called after the algorithm stops.

To compute the gap, the :meth:`updateStatistics()` method calls the 
:meth:`calculateObjective()` method defined in the parent 
:class:`IterateMonitor` class. To compute the objective, this
method relies on an optimality monitor class being defined for a 
given experiment. The only requirement for this optimality monitor
class is to implement a :meth:`calculateObjective()` method. 
Here is the one from the :class:`ADMM_phase_indicator_objective` class.
This method takes as only argument the instance of the algorithm 
that is running.
 
.. code-block:: python
   :linenos:

   def calculateObjective(self, alg):

      lagrangian = 0
      u = alg.u_new

      m1, n1, p1, _q1 = size_matlab(u[0])
      k2 = self.product_space_dimension
      get, _set, _data_shape = accessors(u[1], self.Nx, self.Ny, k2)

      eta = self.lmbda

      if m1 > 1 and n1 > 1 and p1 == 1:
         FFT = lambda u: fft2(u)
         IFFT = lambda u: ifft2(u)
         for j in range(k2):
               if self.farfield:
                  if self.fresnel_nr is not None and self.fresnel_nr[j] > 0:
                     u_hat = -1j*self.fresnel_nr[j]/(self.Nx*self.Ny*2*pi)*FFT(u[0]-self.illumination[j]) + self.FT_conv_kernel[j]
                  elif self.FT_conv_kernel is not None:
                     u_hat = FFT(self.FT_conv_kernel[j]*u[0]) / (m1*n1)
                  else:
                     u_hat = FFT(u[0]) / (m1*n1)
               else: # near field
                  if self.beam is not None:
                     u_hat = IFFT(self.FT_conv_kernel[j]*FFT(u*self.beam[j]))/self.magn[j]
                  else:
                     u_hat = IFFT(self.FT_conv_kernel[j]*FFT(u))/self.magn[j]
               tmp = u_hat - u[1][j]
               lagrangian += np.real(np.trace(u[2][j].T.conj() @ tmp)) / self.normM**2
               lagrangian += 0.5 * eta * (norm(tmp)/self.normM)**2
      elif m1 == 1 or n1 == 1:  # 1D signals
         if m1 == 1:
               divisor = n1
         else:
               divisor = m1
         FFT = lambda u: fft(u)
         IFFT = lambda u: ifft(u)
         for j in range(k2): 
               if self.farfield:
                  if self.fresnel_nr is not None and self.fresnel_nr[j] > 0:
                     raise NotImplementedError('Error ADMM_phase_indicator_objective: complicated far field set-ups for 1D signals not implemented')
                  elif self.FT_conv_kernel is not None:
                     u_hat = FFT(self.FT_conv_kernel[j]*u[0]) / divisor
                  else:
                     u_hat = FFT(u[0]) / divisor
               else:
                  raise NotImplementedError('Error ADMM_phase_indicator_objective: near field set-ups for 1D signals not implemented')
               
               tmp = u_hat - get(u[1],j)
               lagrangian += np.real((get(u[2],j).T.conj() @ tmp)) / self.normM**2
               lagrangian += 0.5 * (norm(tmp)/self.normM)**2
      else:
         print('Error ADMM_phase_indicator_objective: Not designed to handle 4D arrays - throwing a dummy value 999')
         lagrangian = 999
      
      return lagrangian

The use of an optimality monitor is optional, since the gap could
also be computed directly in the :meth:`updateStatistics()` method.

The Python files containing the iterate and optimality monitor classes
must be contained in the :file:`algorithms` folder, and one
has to add corresponding import statements into the
file :file:`__init__.py` contained in the :file:`algorithms` folder:

.. code-block:: python
   :linenos:

   from .ADMM_IterateMonitor import *
   from .ADMM_phase_indicator_objective import *





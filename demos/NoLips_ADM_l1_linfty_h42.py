
import SetProxPythonPath
from proxtoolbox.experiments.NoLips.randomExperiment import randomExperiment

ADM_l1_l1_h42 = randomExperiment(algorithm='ADM', domain_objective='l1', image_objective='linfty', Bregman_potential='h_4', lambda_0=.05, lambda_max=.05, Lip_grad_max=.5, augmentation_scaling=.005, image_dim=51, MAXIT=10000)
ADM_l1_l1_h42.run()
ADM_l1_l1_h42.show()

# Notes: finite termination after 30 iterations with nonconvex coupling
# with convex coupling, get finite termination after 194 iterations
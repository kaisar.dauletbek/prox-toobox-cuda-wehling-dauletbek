
import SetProxPythonPath
from proxtoolbox.experiments.sourceloc.sourceLocExperiment import SourceLocExperiment

sourceExp = SourceLocExperiment(algorithm='DyRePr', sensors=10, noise=True)
sourceExp.run()
sourceExp.show()

import pdb
import SetProxPythonPath
from proxtoolbox.experiments.phase.JWST_Experiment import JWST_Experiment

JWST = JWST_Experiment(algorithm='DRl', lambda_0=1.0, lambda_max=1.0, anim=True)
JWST.run()
JWST.show()

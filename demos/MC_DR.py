import SetProxPythonPath
from proxtoolbox.experiments.MC.MC_Experiment import MC_Experiment

MC = MC_Experiment(algorithm='DR',
                   fraction_known_entrees=0.3,
                   MAXIT=300,
                   picture_infilling=True,
                   )
MC.run()
MC.animate(MC.algorithm)
MC.show()
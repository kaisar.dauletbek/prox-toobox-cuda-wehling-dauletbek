
import SetProxPythonPath
from proxtoolbox.experiments.phase.CDI_Experiment import CDI_Experiment

CDI = CDI_Experiment(algorithm = 'DRAP', lambda_0 = 0.02, lambda_max = 0.02)
CDI.run()
CDI.show()


import SetProxPythonPath
from proxtoolbox.experiments.phase.JWST_Experiment import JWST_Experiment

JWST = JWST_Experiment(algorithm = 'QNAvP', noise=True)
JWST.run()
JWST.show()


import SetProxPythonPath
from proxtoolbox.experiments.phase.CDI_Experiment import CDI_Experiment

CDI = CDI_Experiment(algorithm = 'QNAvP')
CDI.run()
CDI.show()

import SetProxPythonPath
from proxtoolbox.experiments.orbitaltomography.degenerate_orbits import DegenerateOrbital

exp_pm = DegenerateOrbital(algorithm='CP',
                           constraint='sparse real',
                           sparsity_parameter=180)
# exp_pm.plotInputData()
exp_pm.run()
exp_pm.show()

import SetProxPythonPath
from proxtoolbox.experiments.phase.SIRIUS_CDI_Experiment_cuda import SIRIUS_CDI_Experiment_Cuda

CDI = SIRIUS_CDI_Experiment_Cuda(algorithm = 'RAAR_Cuda', Nx = 256, Ny = 256, 
            lambda_0 = 0.9, lambda_max = 0.5)
CDI.run()
CDI.show()

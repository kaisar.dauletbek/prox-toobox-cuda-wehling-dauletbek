
import SetProxPythonPath
from proxtoolbox.experiments.phase.JWST_Experiment import JWST_Experiment

JWST = JWST_Experiment(algorithm='AvP', accelerator_name='GenericAccelerator')
JWST.run()
JWST.show()

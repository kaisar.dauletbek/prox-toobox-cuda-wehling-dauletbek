
import SetProxPythonPath
from proxtoolbox.experiments.NoLips.randomExperiment import randomExperiment

ADM_0_linfty_h2 = randomExperiment(algorithm='ADM', domain_objective= 'none', image_objective= 'linfty', coupling_function='Square_Quadratic_convex', coupling_approximation='quadratic', Bregman_potential='l2', lambda_0=.05, lambda_max=.05, Lip_grad_max=15, augmentation_scaling=1, domain_dim=30, image_dim=40, MAXIT=500000)
ADM_0_linfty_h2.run()
ADM_0_linfty_h2.show()

# Notes:  hard problem.  Converges eventually with a linear (but fast) rate with quadratic approximation,
# augmentation_scaling = 1 (8732 iterations).  augmentation_scaling = 2 still ok (2366 iterations), but slows down locally.  
# augmentation_scaling = 3 does not converge.  augmentation_scaling = 1.75 the best (2900 iterations, no
# local slow-down)
# Maybe the augmentation scaling needs to be adjusted dynamically?  
# very hard to converge with just a linear approximation.  Need augmentation_scaling<.1
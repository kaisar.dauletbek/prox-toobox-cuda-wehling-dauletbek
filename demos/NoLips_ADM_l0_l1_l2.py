
import SetProxPythonPath
from proxtoolbox.experiments.NoLips.randomExperiment import randomExperiment

ADM_l0_l1_h42 = randomExperiment(algorithm='ADM', domain_objective= 'l0', Bregman_potential='l2', lambda_0=.05, lambda_max=.05, Lip_grad_max=1, augmentation_scaling=.15, image_dim=50, MAXIT=10000)
ADM_l0_l1_h42.run()
ADM_l0_l1_h42.show()


import SetProxPythonPath
from proxtoolbox.experiments.phase.CDP_Experiment import CDP_Experiment


CDP = CDP_Experiment(algorithm='DRAP', Nx=256,  MAXIT=1000, TOL=1e-8,
                     data_ball = 1e-15, iterate_monitor_name = 'IterateMonitor',
                     warmup_iter=50)
CDP.run()
CDP.show()

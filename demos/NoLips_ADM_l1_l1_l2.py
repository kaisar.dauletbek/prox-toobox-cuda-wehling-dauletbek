
import SetProxPythonPath
from proxtoolbox.experiments.NoLips.randomExperiment import randomExperiment

ADM_l1_l1_h42 = randomExperiment(algorithm='ADM', domain_objective='l1', lambda_0=.1, lambda_max=.1, Lip_grad_max=1, Bregman_potential='l2', augmentation_scaling=.016, image_dim=51, MAXIT=10000)
ADM_l1_l1_h42.run()
ADM_l1_l1_h42.show()

# Notes:  59 iterations with nonconvex coupling.
# 10 iterations, finite convergence, convex coupling
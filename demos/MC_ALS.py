import SetProxPythonPath
from proxtoolbox.experiments.MC.MC_Experiment import MC_Experiment

MC = MC_Experiment(algorithm='ALS',
                   fraction_known_entrees=0.3,
                   rank=6,
                   MAXIT=300,
                   picture_infilling=True,
                   )
MC.run()
MC.animate(MC.algorithm)
MC.show()

import SetProxPythonPath
from proxtoolbox.experiments.phase.CDP_Experiment import CDP_Experiment

CDP = CDP_Experiment(algorithm='Wirtinger', Nx=256, TOL=1e-8, warmup_iter=50)
CDP.run()
CDP.show()

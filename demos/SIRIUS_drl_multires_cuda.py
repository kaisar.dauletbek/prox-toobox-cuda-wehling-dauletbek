
import SetProxPythonPath
from proxtoolbox.experiments.phase.SIRIUS_CDI_Experiment_cuda import SIRIUS_CDI_Experiment_Cuda

CDI= None
for j in range(5,9):
    if CDI==None:
        CDI = SIRIUS_CDI_Experiment_Cuda(algorithm = 'RAAR_Cuda', Nx = 2**j, Ny = 2**j, 
            lambda_0 = 0.7, lambda_switch=1000, lambda_max = 0.5, MAXIT=2000, TOL=5e-8, sparsity_parameter=8000)
    else:
        CDI = SIRIUS_CDI_Experiment_Cuda(initial_guess = CDI.output['u'], algorithm = 'RAAR_Cuda', Nx = 2**j, Ny = 2**j, 
            lambda_0 = 0.9, lambda_max = 0.5, lambda_switch=100, MAXIT=2000, TOL=5e-5, sparsity_parameter=500)
    CDI.run()
CDI.show()



import SetProxPythonPath
from proxtoolbox.experiments.phase.CDP_Experiment import CDP_Experiment


CDP = CDP_Experiment(algorithm='CDRl', formulation='cyclic', lambda_0=1.0, lambda_max=1.0,
                     MAXIT=6000, warmup_iter=50)
CDP.run()
CDP.show()

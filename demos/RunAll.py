
import SetProxPythonPath

print("******************************\n")
print("* Starting Coffee demos      *\n")
print("******************************\n")
print("\n")
print("\n")

from proxtoolbox.experiments.phase.CDI_Experiment import CDI_Experiment

CDI = CDI_Experiment(algorithm = 'CP', MAXIT = 20, verbose=0)
CDI.run()
CDI.show()

CDI = CDI_Experiment(algorithm = 'AvP', MAXIT = 20, verbose=0)
CDI.run()
CDI.show()

CDI = CDI_Experiment(algorithm = 'QNAvP', MAXIT = 20, verbose=0)
CDI.run()
CDI.show()

CDI = CDI_Experiment(algorithm = 'DRl', MAXIT = 20, verbose=0)
CDI.run()
CDI.show()

CDI = CDI_Experiment(algorithm = 'DRAP', MAXIT = 20, lambda_0 = 0.02, lambda_max = 0.02, verbose=0)
CDI.run()
CDI.show()

CDI = CDI_Experiment(algorithm = 'CDRl', lambda_0 = 1.0, lambda_max = 1.0, MAXIT = 20, verbose=0)
CDI.run()
CDI.show()

CDI = CDI_Experiment(algorithm = 'CDRl', MAXIT = 20, lambda_0 = 0.9, lambda_max = 0.9, verbose=0)
CDI.run()
CDI.show()

print("\n")
print("\n")
print("**********************************************\n")
print("* Starting Siemens demos - no regularization *\n")
print("**********************************************\n")
print("\n")
from proxtoolbox.experiments.phase.Krueger_Experiment import Krueger_Experiment

Krueger = Krueger_Experiment(algorithm = 'AvP', MAXIT = 20, verbose=0)
Krueger.run()
Krueger.show()

Krueger = Krueger_Experiment(algorithm = 'QNAvP', MAXIT = 20, verbose=0)
Krueger.run()
Krueger.show()

Krueger = Krueger_Experiment(algorithm = 'CP', MAXIT = 20, verbose=0)
Krueger.run()
Krueger.show()


Krueger = Krueger_Experiment(algorithm = 'DRl', MAXIT = 20,
                             lambda_0 = 0.5, lambda_max = 0.5, verbose=0)
Krueger.run()
Krueger.show()


Krueger = Krueger_Experiment(algorithm = 'DRAP', MAXIT = 20, verbose=0)
Krueger.run()
Krueger.show()

print("\n")
print("\n")
print("*******************************************\n")
print("* Starting Siemens demos - regularization *\n")
print("*******************************************\n")
print("\n")
print("Starting Siemens demos - regularization")
from proxtoolbox.experiments.phase.Krueger_Experiment import Krueger_Experiment

Krueger = Krueger_Experiment(algorithm = 'AvP', MAXIT = 20, data_ball = 24, verbose=0)
Krueger.run()
Krueger.show()

Krueger = Krueger_Experiment(algorithm = 'QNAvP', MAXIT = 20, data_ball = 20, verbose=0)
Krueger.run()
Krueger.show()

Krueger = Krueger_Experiment(algorithm = 'CP', MAXIT = 20, data_ball = 20, verbose=0)
Krueger.run()
Krueger.show()


Krueger = Krueger_Experiment(algorithm = 'DRl', MAXIT = 20, data_ball = 20,
                             lambda_0 = 0.5, lambda_max = 0.5, verbose=0)
Krueger.run()
Krueger.show()


Krueger = Krueger_Experiment(algorithm = 'DRAP', MAXIT = 20, data_ball = 20, verbose=0)
Krueger.run()
Krueger.show()

print("\n")
print("\n")
print("*******************************************\n")
print("* Starting ptychography demos             *\n")
print("*******************************************\n")
print("\n")
from proxtoolbox.experiments.ptychography.ptychographyExperiment import PtychographyExperiment

warmupParams = {
        'algorithm': 'DRl',
        'MAXIT': 10,
        'TOL': 5e-5,
        'lambda_0': 0.75,
        'lambda_max': 0.75,
        'lambda_switch': 20,
        'data_ball': 1e-30,
        'diagnostic': True,
        'rotate': False,
        'iterate_monitor_name': 'IterateMonitor',
        'verbose': 1,
        'graphics': 0,
        'anim': False
    }

Pty = PtychographyExperiment(debug = False, warmup = True, MAXIT=10, warmup_params=warmupParams, verbose=0)
Pty.run()
Pty.show()

print("Test successfull")

import SetProxPythonPath
from proxtoolbox.experiments.sourceloc.sourceLocExperiment import SourceLocExperiment

sourceExp = SourceLocExperiment(algorithm='CDRl', formulation='cyclic', lambda_0=0.33, lambda_max=0.33, sensors=10, noise=True)
sourceExp.run()
sourceExp.show()

import pdb
import SetProxPythonPath
from proxtoolbox.experiments.phase.Elser_Experiment import Elser_Experiment

Elser = Elser_Experiment(Atoms=400, category='H', algorithm='RRR', lambda_0=0.95, lambda_max=0.95, anim=True)
Elser.run()
Elser.show()

import SetProxPythonPath
# CT & Phasepack take too long.  Orbital still not working
# from proxtoolbox.experiments.CT.ART_Experiment import ART_Experiment
from proxtoolbox.experiments.phase.CDP_Experiment import CDP_Experiment
from proxtoolbox.experiments.phase.JWST_Experiment import JWST_Experiment
from proxtoolbox.experiments.phase.Krueger_Experiment import Krueger_Experiment
# from proxtoolbox.experiments.phase.Phasepack_Experiment import Phasepack_Experiment
from proxtoolbox.experiments.ptychography.ptychographyExperiment import PtychographyExperiment
from proxtoolbox.experiments.sourceloc.sourceLocExperiment import SourceLocExperiment
from proxtoolbox.experiments.phase.Sparse2_Experiment import Sparse2_Experiment
from proxtoolbox.experiments.NoLips.randomExperiment import randomExperiment
from proxtoolbox.experiments.MC.MC_Experiment import MC_Experiment
from proxtoolbox.experiments.BM.SumOfLeastSquares_Experiment import SumOfLeastSquares_Experiment
from proxtoolbox.experiments.sudoku.sudokuExperiment import SudokuExperiment
from proxtoolbox.experiments.phase.CDI_Experiment import CDI_Experiment
from proxtoolbox.experiments.phase.SIRIUS_CDI_Experiment import SIRIUS_CDI_Experiment
# from proxtoolbox.experiments.orbitaltomography import PlanarMolecule #, DegenerateOrbital, OrthogonalOrbitals

import numpy as np
import os
import enum
import time

def getDict(**kwargs):
    return kwargs

def printValue(value):
    if isinstance(value, str):
        print("\'" + value + "\'", end='')
    else:
        print(value, end='')


class TestEngine:
    '''
    TestEngine class
    This class has three purposes:
        1. Generation of the data structure used for the regression tests. 
        2. Update of the data structure used for the regression tests
        3. Running the regression tests

    See script below to see how it is used.
    '''

    def __init__(self, testData=None, maxIterCount=5, maxError=1e-5, **kwargs):
        self.testData = testData
        self.maxIterCount = maxIterCount
        self.maxError = maxError


    def run(self):
        self.failedCount = 0
        for demo in self.testData:
            trueChange = demo['change']
            experiment = self.instantiateExperiment(demo)
            algorithmName = demo['args']['algorithm']
            print("Running " + algorithmName + " on " + experiment.experiment_name + "... ", end='')
            experiment.run()
            output = experiment.output
            changes = output['stats']['changes']
            actualChange = changes[len(changes)-1] # take last value
            # test 
            success, percentageDiff = self.checkValue(trueChange, actualChange)
            if success:
                print(" Passed")
            else:
                print(" Failed")
                print("\t with percentage difference of", percentageDiff*100, '%')
                self.failedCount += 1
        return (self.failedCount == 0)

    def generateTestData(self):
        self.testData = []
        directory = '../demos'
        for filename in os.listdir(directory):
            if filename.endswith(".py") and filename != "RunAll.py":
                path = directory + "/" + filename
                with open(path) as file:
                    content = file.read()
                    # look for "proxtoolbox.experiments"
                    index = content.find("proxtoolbox.experiments")
                    if index == -1:
                        print("Skipping %s at experiment check" % filename)
                        continue
                    content = content[index:]
                    # look for next import statement
                    index = content.find("import")
                    if index == -1:
                        print("Skipping %s as no import found" % filename)
                        continue
                    content = content.split("import", 1)[1]
                    # the next token is the name of the experiment class
                    tokens = content.split()
                    experimentClassName = tokens[0]
                    # for now skip Ptychography due to warmup_params argument
                    # that refer to another variable
                    # we skip Phasepack_Experiment because this consumes too much memory,
                    # and ART because it takes too much time to run those demos;
                    # skip Elser and the Orbital tomog experiments because they are 
                    # not working
                    if experimentClassName == "PtychographyExperiment" \
                         or experimentClassName == "Phasepack_Experiment" \
                         or experimentClassName == "PlanarMolecule" \
                         or experimentClassName == "DegenerateOrbital" \
                         or experimentClassName == "Elser_Experiment" \
                         or experimentClassName == "ART_Experiment": \
                        continue
                    content = content.split(experimentClassName,1)[1]
                    # look for next occurence of experiment class
                    index = content.find(experimentClassName)
                    if index == -1:
                        print("Skipping %s as no classname found" %filename)
                        continue
                    content = content[index:]
                    # look for arguments
                    content = content.split("(",1)[1]
                    index = content.find(")")
                    args = content[:index]
                    # add what we found in testData
                    exp_dict = {}
                    exp_dict['experiment'] = experimentClassName
                    dict_args = eval('getDict(' + args + ')')
                    exp_dict['args'] = dict_args
                    print("processed", filename)
                    self.testData.append(exp_dict)

        self.printTestData()

    def updateTestData(self):
        for demo in self.testData:
            experiment = self.instantiateExperiment(demo)
            algorithmName = demo['args']['algorithm']
            print("Running " + algorithmName + " on " + experiment.experiment_name + "... ")
            experiment.run()
            output = experiment.output
            changes = output['stats']['changes']
            actualChange = changes[len(changes)-1] # take last value
            demo['change'] = actualChange
        self.printTestData()

    def instantiateExperiment(self, demo):
        # retrieve demo info
        experimentName = demo['experiment']
        args = demo['args']
        # retrieve class from experiment name. This works because of 
        # import statement at the beginning of file
        experimentClass = globals()[experimentName]
        # instantiate and run experiment
        MAXIT = self.maxIterCount
        if 'MAXIT' in demo:
            MAXIT = demo['MAXIT']
        args['MAXIT'] = MAXIT
        args['silent'] = True # we don't want any message during those runs
        experiment = experimentClass(**args)
        return experiment

    def checkValue(self, trueVal, actualVal):
        percentageDiff = np.abs((trueVal - actualVal) / (trueVal+1e-30))
        return (percentageDiff <= self.maxError), percentageDiff

    def printTestData(self):
        print("\ttestData = [")
        demoCount = len(self.testData)
        i = 1
        for demo in self.testData:
            print("\t\t{", "\'experiment\' :", "\'" + demo['experiment'] + "\'",
                  ',', "\n\t\t  \'args\' : getDict(", end='')
            args = demo['args']
            j = 0
            for arg, value in args.items():
                if arg == 'silent' or arg == 'MAXIT': # skip arguments that are set by this class
                    continue
                if j != 0:
                    print(', ', end='')
                if j > 3:
                    print("\n\t\t\t", end='')
                print(arg, "= ", end='')
                printValue(value)
                j += 1
            print(")", end='')
            # optional keys
            if 'change' in demo or 'MAXIT' in demo:
                print(",")
                if 'change' in demo:
                    print("\t\t  \'change\' :", demo['change'], end='')
                if 'MAXIT' in demo:
                    print(",")
                    print("\t\t  \'MAXIT\' :", demo['MAXIT'], end='')
            print(' }', end='')
            if i != demoCount: # this is not the last demo
                print(',')
            i += 1
        print("\n\t\t]")

class Mode(enum.Enum):
   GenerateTestData = 1
   UpdateTestData = 2
   RunTests = 3

if __name__ == "__main__":
    """
    These regression tests will run a list of demos 
    for a given number of iterations.
    
    There are three ways to use this script:

    1. Generation of the data structure used for the regression tests: This 
    data structure is created by parsing and extracting the relevant information
    from the demo files contained in the demos folder. The data structure
    is formally a list of dictionaries. Each dictionary describes a demo by specifying
    the experiment class and the arguments required to instantiate an experiment.
    The resulting data structure is printed in the terminal window used to run this script
    and must be copy-pasted into the script (as the 'testData' variable)
    Optionally, a `MAXIT` entry can be manually added to the dictionary describing each demo
    If present, this will override the default number of iterations that is used
    to run all the demos.

    2. Update of the data structure: the script runs all the demos described in
    the data structure and a `change` entry is added in the dictionary describing each demo.
    This value describes the change in iterates when the specified 
    number of iterations is reached.

    3. Running the regression tests: This is the most common use of this script.
    It requires that the data structure used for the tests exists and provides
    a `change` entry for each demo described. The script runs each demo and 
    the actual change in iterates is compared to the value contained in the data structure.

    """
    testData = [
                { 'experiment' : 'randomExperiment' , 
                  'args' : getDict(algorithm = 'ADM', domain_objective = 'l2', image_objective = 'linfty', Bregman_potential = 'l2', 
                        lambda_0 = 0.5, 
                        lambda_max = 0.5, 
                        Lip_grad_max = 1, 
                        augmentation_scaling = 0.001, 
                        image_dim = 51),
                  'change' : 1.2517475668477092 },
                { 'experiment' : 'MC_Experiment' , 
                  'args' : getDict(algorithm = 'AP', fraction_known_entrees = 0.3, rank = 6, picture_infilling = True),
                  'change' : 7.94224826952401 },
                { 'experiment' : 'SourceLocExperiment' , 
                  'args' : getDict(algorithm = 'CP', formulation = 'cyclic', noise = True),
                  'change' : 0.0029751369217766235 },
                { 'experiment' : 'CDI_Experiment' , 
                  'args' : getDict(algorithm = 'AvP'),
                  'change' : 4.6778445639254755 },
                { 'experiment' : 'JWST_Experiment' , 
                  'args' : getDict(algorithm = 'Wirtinger', noise = True),
                  'change' : 0.049914482007897526 },
                { 'experiment' : 'SumOfLeastSquares_Experiment' , 
                  'args' : getDict(algorithm = 'GPM', Nx = 8, Ny = 12, Nz = 50, 
                        constBound = 20),
                  'change' : 0.2839486834148186 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'AvP'),
                  'change' : 0.3733544093581862 },
                { 'experiment' : 'Krueger_Experiment' , 
                  'args' : getDict(algorithm = 'CP', data_ball = 20),
                  'change' : 0.009297330050179213 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'DyRePr', Nx = 128, TOL = 1e-08, data_ball = 1e-15, 
                        warmup_iter = 50),
                  'change' : 2.1044296046724577e-05 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'AvP', Nx = 128, TOL = 1e-08, data_ball = 1e-15, 
                        accelerator_name = 'GenericAccelerator'),
                  'change' : 0.6663241159710994 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'DRl', Nx = 128, lambda_0 = 0.75, lambda_max = 0.75, 
                        TOL = 1e-08, 
                        data_ball = 1e-15, 
                        iterate_monitor_name = 'IterateMonitor'),
                  'change' : 1.5723471921970014 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'AvP2', lambda_0 = 0.75, lambda_max = 0.75, TOL = 5e-10),
                  'change' : 0.0719748990700038 },
                { 'experiment' : 'SourceLocExperiment' , 
                  'args' : getDict(algorithm = 'CDRl', formulation = 'cyclic', lambda_0 = 0.33, lambda_max = 0.33, 
                        sensors = 10, 
                        noise = True),
                  'change' : 1.4899053958188089e-06 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'ADMM', constraint = 'support only', lambda_0 = 10, lambda_max = 10, 
                        warmup_iter = 50),
                  'change' : 21.38973820888715 },
                { 'experiment' : 'SourceLocExperiment' , 
                  'args' : getDict(algorithm = 'DRl', lambda_0 = 1.0, lambda_max = 1.0),
                  'change' : 0.005057809729781892 },
                { 'experiment' : 'JWST_Experiment' , 
                  'args' : getDict(algorithm = 'QNAvP', noise = True),
                  'change' : 0.23761737552201437 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'CP', warmup_iter = 50),
                  'change' : 0.2802395344629181 },
                { 'experiment' : 'SourceLocExperiment' , 
                  'args' : getDict(algorithm = 'DRAP', lambda_0 = 0.25, lambda_max = 0.25, sensors = 10),
                  'change' : 0.0021156955263752454 },
                { 'experiment' : 'randomExperiment' , 
                  'args' : getDict(algorithm = 'ADM', domain_objective = 'none', image_objective = 'linfty', coupling_function = 'Square_Quadratic_convex', 
                        coupling_approximation = 'linear', 
                        Bregman_potential = 'l2', 
                        lambda_0 = 0.05, 
                        lambda_max = 0.05, 
                        Lip_grad_max = 15, 
                        augmentation_scaling = 0.008, 
                        domain_dim = 30, 
                        image_dim = 40),
                  'change' : 1667.2056507779182 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'AvP2', Nx = 128, lambda_0 = 0.75, lambda_max = 0.75, 
                        TOL = 1e-08, 
                        warmup_iter = 50),
                  'change' : 0.09434959497646991 },
                { 'experiment' : 'JWST_Experiment' , 
                  'args' : getDict(algorithm = 'Wirtinger'),
                  'change' : 0.0499907880744614 },
                { 'experiment' : 'SourceLocExperiment' , 
                  'args' : getDict(algorithm = 'AvP'),
                  'change' : 0.001116540575903532 },
                { 'experiment' : 'Sparse2_Experiment' , 
                  'args' : getDict(algorithm = 'CP', cutoff = 25),
                  'change' : 0.023928706457233828 },
                { 'experiment' : 'JWST_Experiment' , 
                  'args' : getDict(algorithm = 'CDRl', formulation = 'cyclic', lambda_0 = 1.0, lambda_max = 1.0),
                  'change' : 0.48364654120340794 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'CDRl', formulation = 'cyclic', lambda_0 = 0.33, lambda_max = 0.33),
                  'change' : 0.00743693440478869 },
                { 'experiment' : 'JWST_Experiment' , 
                  'args' : getDict(algorithm = 'CDRl', formulation = 'cyclic', lambda_0 = 0.99, lambda_max = 0.5, 
                        rotate = True),
                  'change' : 0.49441230925429763 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'CDRl', Nx = 128, formulation = 'cyclic', lambda_0 = 0.3, 
                        lambda_max = 0.3, 
                        TOL = 1e-08, 
                        warmup_iter = 50),
                  'change' : 0.0023251374061570907 },
                { 'experiment' : 'JWST_Experiment' , 
                  'args' : getDict(algorithm = 'DRl', lambda_0 = 1.0, lambda_max = 1.0, noise = True),
                  'change' : 1.4869269993134888 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'Wirtinger'),
                  'change' : 0.057869992215465814 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'AvP', warmup_iter = 50),
                  'change' : 0.18271647829249524 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'CDRl', Nx = 128, formulation = 'cyclic', lambda_0 = 0.3, 
                        lambda_max = 0.3, 
                        TOL = 1e-08),
                  'change' : 0.9074042807042689 },
                { 'experiment' : 'SourceLocExperiment' , 
                  'args' : getDict(algorithm = 'AvP', accelerator_name = 'GenericAccelerator', sensors = 10),
                  'change' : 0.0031823954438991377 },
                { 'experiment' : 'SourceLocExperiment' , 
                  'args' : getDict(algorithm = 'QNAvP'),
                  'change' : 0.0 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'CP', Nx = 128, formulation = 'cyclic', lambda_0 = 0.75, 
                        lambda_max = 0.75, 
                        TOL = 1e-08, 
                        data_ball = 1e-15, 
                        iterate_monitor_name = 'IterateMonitor'),
                  'change' : 1.4477416483773033 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'DRl', Nx = 128, lambda_0 = 1.0, lambda_max = 1.0, 
                        TOL = 1e-08),
                  'change' : 3.478814689994805 },
                { 'experiment' : 'randomExperiment' , 
                  'args' : getDict(algorithm = 'ADM', domain_objective = 'l1', image_objective = 'l1', coupling_function = 'Square_Quadratic_convex', 
                        Bregman_potential = 'h_4', 
                        lambda_0 = 0.05, 
                        lambda_max = 0.05, 
                        Lip_grad_max = 0.5, 
                        augmentation_scaling = 0.005, 
                        image_dim = 51),
                  'change' : 825.8419521784749 },
                { 'experiment' : 'Krueger_Experiment' , 
                  'args' : getDict(algorithm = 'AvP', data_ball = 24),
                  'change' : 0.0054483427532242695 },
                { 'experiment' : 'JWST_Experiment' , 
                  'args' : getDict(algorithm = 'ADMM', lambda_0 = 4.0, lambda_max = 4.0, anim = True),
                  'change' : 0.006911634576586328 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'DRl', Nx = 128, lambda_0 = 1.0, lambda_max = 1.0, 
                        TOL = 1e-08, 
                        warmup_iter = 50),
                  'change' : 3.4652873086901206 },
                { 'experiment' : 'JWST_Experiment' , 
                  'args' : getDict(algorithm = 'CADRl', formulation = 'cyclic', lambda_0 = 1.0, lambda_max = 1.0, 
                        rotate = True),
                  'change' : 0.342904979233453 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'DRl', Nx = 128, lambda_0 = 0.75, lambda_max = 0.75, 
                        TOL = 1e-08, 
                        data_ball = 1e-15, 
                        iterate_monitor_name = 'IterateMonitor', 
                        warmup_iter = 50),
                  'change' : 1.5488804067347997 },
                { 'experiment' : 'randomExperiment' , 
                  'args' : getDict(algorithm = 'ADM', domain_objective = 'l0', Bregman_potential = 'h_4_2', lambda_0 = 0.05, 
                        lambda_max = 0.05, 
                        Lip_grad_max = 5, 
                        augmentation_scaling = 0.09, 
                        image_dim = 51),
                  'change' : 5475.785364106493 },
                { 'experiment' : 'JWST_Experiment' , 
                  'args' : getDict(algorithm = 'DRAP', lambda_0 = 0.25, lambda_max = 0.25, noise = True),
                  'change' : 0.2275545891146696 },
                { 'experiment' : 'SourceLocExperiment' , 
                  'args' : getDict(algorithm = 'DyRePr', sensors = 10, noise = True),
                  'change' : 3.984508156135621e-07 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'AvP', Nx = 128, TOL = 1e-08),
                  'change' : 0.36641612740796786 },
                { 'experiment' : 'Sparse2_Experiment' , 
                  'args' : getDict(algorithm = 'QNAvP', cutoff = 3),
                  'change' : 0.05558857179549994 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'CDRl', Nx = 128, formulation = 'cyclic', lambda_0 = 1.0, 
                        lambda_max = 1.0, 
                        TOL = 1e-08),
                  'change' : 1.282242655984041 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'CDRl', formulation = 'cyclic', lambda_0 = 1.0, lambda_max = 1.0, 
                        warmup_iter = 50),
                  'change' : 1.3339036671358373 },
                { 'experiment' : 'Sparse2_Experiment' , 
                  'args' : getDict(algorithm = 'DRl', TOL = 0.0005),
                  'change' : 0.2881026011400392 },
                { 'experiment' : 'JWST_Experiment' , 
                  'args' : getDict(algorithm = 'AvP', accelerator_name = 'GenericAccelerator', noise = True),
                  'change' : 0.305763077807265 },
                { 'experiment' : 'Sparse2_Experiment' , 
                  'args' : getDict(algorithm = 'AvP', accelerator_name = 'GenericAccelerator'),
                  'change' : 0.021168286958578753 },
                { 'experiment' : 'SumOfLeastSquares_Experiment' , 
                  'args' : getDict(algorithm = 'BCD', inner_MAXIT = 10, inner_algorithm = 'GPM', Nx = 8, 
                        Ny = 12, 
                        Nz = 50, 
                        constBound = 20, 
                        randomizedAlgo = True),
                  'change' : 0.4886497878837932 },
                { 'experiment' : 'CDI_Experiment' , 
                  'args' : getDict(algorithm = 'DRAP', lambda_0 = 0.02, lambda_max = 0.02),
                  'change' : 2.950664953648575 },
                { 'experiment' : 'SourceLocExperiment' , 
                  'args' : getDict(algorithm = 'DRAP', lambda_0 = 0.25, lambda_max = 0.25, noise = True),
                  'change' : 0.005386090451010736 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'DRAP', Nx = 128, TOL = 1e-08, data_ball = 1e-15, 
                        iterate_monitor_name = 'IterateMonitor', 
                        warmup_iter = 50),
                  'change' : 1.6285628525727063 },
                { 'experiment' : 'JWST_Experiment' , 
                  'args' : getDict(algorithm = 'DyRePr', TOL = 5e-09, noise = True),
                  'change' : 0.0004720636937675946 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'DyRePr', Nx = 128, TOL = 1e-08, data_ball = 1e-15),
                  'change' : 0.0002948483649203534 },
                { 'experiment' : 'Krueger_Experiment' , 
                  'args' : getDict(algorithm = 'DRAP', data_ball = 20),
                  'change' : 0.010319098395357586 },
                { 'experiment' : 'Sparse2_Experiment' , 
                  'args' : getDict(algorithm = 'CP', cutoff = 3),
                  'change' : 0.017501549264108487 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'QNAvP', Nx = 128, TOL = 1e-08, warmup_iter = 50),
                  'change' : 0.17713785363007198 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'AvP', constraint = 'support only', accelerator_name = 'GenericAccelerator', warmup_iter = 50),
                  'change' : 0.2252889914846456 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'CDRl', formulation = 'cyclic', lambda_0 = 1.0, lambda_max = 1.0),
                  'change' : 1.2288178479212477 },
                { 'experiment' : 'JWST_Experiment' , 
                  'args' : getDict(algorithm = 'CP', formulation = 'cyclic', noise = True, rotate = True),
                  'change' : 0.2870278864280826 },
                { 'experiment' : 'randomExperiment' , 
                  'args' : getDict(algorithm = 'ADM', domain_objective = 'none', image_objective = 'l1', coupling_function = 'Square_Quadratic_convex', 
                        coupling_approximation = 'linear', 
                        Bregman_potential = 'h_4', 
                        lambda_0 = 0.05, 
                        lambda_max = 0.05, 
                        Lip_grad_max = 5, 
                        augmentation_scaling = 0.025, 
                        domain_dim = 50, 
                        image_dim = 51),
                  'change' : 1797.546268916592 },
                { 'experiment' : 'SourceLocExperiment' , 
                  'args' : getDict(algorithm = 'DRAP', lambda_0 = 0.25, lambda_max = 0.25, sensors = 10, 
                        noise = True),
                  'change' : 0.005125745784275642 },
                { 'experiment' : 'CDI_Experiment' , 
                  'args' : getDict(algorithm = 'QNAvP'),
                  'change' : 8.256773112923149 },
                { 'experiment' : 'SourceLocExperiment' , 
                  'args' : getDict(algorithm = 'AvP', accelerator_name = 'GenericAccelerator', sensors = 10, noise = True),
                  'change' : 0.007869491801963741 },
                { 'experiment' : 'JWST_Experiment' , 
                  'args' : getDict(algorithm = 'HPR', lambda_0 = 0.85, lambda_max = 0.85, anim = True),
                  'change' : 0.8256133604091099 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'Wirtinger', Nx = 128, TOL = 1e-08),
                  'change' : 0.053094807952338105 },
                { 'experiment' : 'SourceLocExperiment' , 
                  'args' : getDict(algorithm = 'AvP', accelerator_name = 'GenericAccelerator', noise = True),
                  'change' : 0.008391608529508545 },
                { 'experiment' : 'CDI_Experiment' , 
                  'args' : getDict(algorithm = 'DRl'),
                  'change' : 2.8624675000475786 },
                { 'experiment' : 'SourceLocExperiment' , 
                  'args' : getDict(algorithm = 'AvP', sensors = 10),
                  'change' : 0.0018547339405736586 },
                { 'experiment' : 'randomExperiment' , 
                  'args' : getDict(algorithm = 'ADM', domain_objective = 'l2', image_objective = 'linfty', Bregman_potential = 'h_4', 
                        lambda_0 = 0.5, 
                        lambda_max = 0.5, 
                        Lip_grad_max = 1, 
                        augmentation_scaling = 0.001, 
                        image_dim = 51),
                  'change' : 1.7129060832362177 },
                { 'experiment' : 'SourceLocExperiment' , 
                  'args' : getDict(algorithm = 'CP', sensors = 10),
                  'change' : 0.002046773530387638 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'AvP2', Nx = 128, lambda_0 = 0.75, lambda_max = 0.75, 
                        TOL = 1e-08),
                  'change' : 0.06746672776265665 },
                { 'experiment' : 'Krueger_Experiment' , 
                  'args' : getDict(algorithm = 'QNAvP'),
                  'change' : 0.006183259667118771 },
                { 'experiment' : 'SourceLocExperiment' , 
                  'args' : getDict(algorithm = 'CP', sensors = 10, noise = True),
                  'change' : 0.004570003108536433 },
                { 'experiment' : 'JWST_Experiment' , 
                  'args' : getDict(algorithm = 'RRR', lambda_0 = 0.95, lambda_max = 0.95, noise = True),
                  'change' : 1.394200968021113 },
                { 'experiment' : 'Krueger_Experiment' , 
                  'args' : getDict(algorithm = 'QNAvP', data_ball = 20),
                  'change' : 0.006183259667118771 },
                { 'experiment' : 'JWST_Experiment' , 
                  'args' : getDict(algorithm = 'RRR', lambda_0 = 0.95, lambda_max = 0.95, anim = True),
                  'change' : 1.0319096966343562 },
                { 'experiment' : 'SourceLocExperiment' , 
                  'args' : getDict(algorithm = 'CDRl', formulation = 'cyclic', lambda_0 = 1, lambda_max = 1, 
                        noise = True),
                  'change' : 4.420658004898516e-05 },
                { 'experiment' : 'Sparse2_Experiment' , 
                  'args' : getDict(algorithm = 'CDRl', lambda_0 = 1.0, lambda_max = 1.0, TOL = 0.0005),
                  'change' : 0.04503436075145002 },
                { 'experiment' : 'CDI_Experiment' , 
                  'args' : getDict(algorithm = 'CP'),
                  'change' : 3.678092798186146 },
                { 'experiment' : 'SudokuExperiment' , 
                  'args' : getDict(algorithm = 'DRl'),
                  'change' : 0.18207260487950389 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'QNAvP', Nx = 128, TOL = 1e-08),
                  'change' : 0.42045846104464046 },
                { 'experiment' : 'Krueger_Experiment' , 
                  'args' : getDict(algorithm = 'AvP'),
                  'change' : 0.0054483427532242695 },
                { 'experiment' : 'Sparse2_Experiment' , 
                  'args' : getDict(algorithm = 'CDRl', TOL = 0.0005),
                  'change' : 0.028415986028241906 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'DRl', lambda_0 = 1.0, lambda_max = 1.0),
                  'change' : 3.440973315193069 },
                { 'experiment' : 'randomExperiment' , 
                  'args' : getDict(algorithm = 'ADM', domain_objective = 'none', image_objective = 'l1', coupling_function = 'Square_Quadratic_convex', 
                        coupling_approximation = 'linear', 
                        Bregman_potential = 'l2', 
                        lambda_0 = 0.05, 
                        lambda_max = 0.05, 
                        Lip_grad_max = 5, 
                        augmentation_scaling = 0.025, 
                        domain_dim = 50, 
                        image_dim = 51),
                  'change' : 153912827.86871016 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'DRAP', Nx = 128, TOL = 1e-08, data_ball = 1e-15, 
                        iterate_monitor_name = 'IterateMonitor'),
                  'change' : 1.7229422216469834 },
                { 'experiment' : 'Sparse2_Experiment' , 
                  'args' : getDict(algorithm = 'QNAvP', cutoff = 15),
                  'change' : 0.0610514761660707 },
                { 'experiment' : 'SourceLocExperiment' , 
                  'args' : getDict(algorithm = 'AvP2', formulation = 'NSLS', lambda_0 = 0.3, lambda_max = 0.3, 
                        sensors = 10, 
                        noise = True),
                  'change' : 0.0004407882777755345 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'DRAP'),
                  'change' : 1.594031769343937 },
                { 'experiment' : 'JWST_Experiment' , 
                  'args' : getDict(algorithm = 'CP', formulation = 'cyclic', TOL = 5e-15, rotate = False),
                  'change' : 0.237879900753243 },
                { 'experiment' : 'JWST_Experiment' , 
                  'args' : getDict(algorithm = 'DRAP', lambda_0 = 0.65, lambda_max = 0.65),
                  'change' : 0.5712294011276137 },
                { 'experiment' : 'JWST_Experiment' , 
                  'args' : getDict(algorithm = 'DyRePr', TOL = 5e-09, anim = True),
                  'change' : 0.00047372483209381195 },
                { 'experiment' : 'randomExperiment' , 
                  'args' : getDict(algorithm = 'ADM', domain_objective = 'none', image_objective = 'l1', coupling_function = 'Square_Quadratic_convex', 
                        coupling_approximation = 'quadratic', 
                        Bregman_potential = 'l2', 
                        lambda_0 = 0.05, 
                        lambda_max = 0.05, 
                        Lip_grad_max = 5, 
                        augmentation_scaling = 1, 
                        domain_dim = 50, 
                        image_dim = 51),
                  'change' : 1.0065341790968372 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'CDRl', Nx = 128, formulation = 'cyclic', lambda_0 = 1.0, 
                        lambda_max = 1.0, 
                        TOL = 1e-08, 
                        warmup_iter = 50),
                  'change' : 1.2789576551127286 },
                { 'experiment' : 'randomExperiment' , 
                  'args' : getDict(algorithm = 'ADM', domain_objective = 'l2', image_objective = 'l1', Bregman_potential = 'l2', 
                        lambda_0 = 0.05, 
                        lambda_max = 0.05, 
                        Lip_grad_max = 2, 
                        augmentation_scaling = 0.005, 
                        image_dim = 51),
                  'change' : 3.192252280632688 },
                { 'experiment' : 'SourceLocExperiment' , 
                  'args' : getDict(algorithm = 'DRAP', lambda_0 = 0.25, lambda_max = 0.25),
                  'change' : 0.0010384409147302935 },
                { 'experiment' : 'randomExperiment' , 
                  'args' : getDict(algorithm = 'ADM', domain_objective = 'l0', image_objective = 'linfty', Bregman_potential = 'h_4_2', 
                        lambda_0 = 0.05, 
                        lambda_max = 0.05, 
                        Lip_grad_max = 5, 
                        augmentation_scaling = 0.2, 
                        image_dim = 51),
                  'change' : 103.65357355676167 },
                { 'experiment' : 'JWST_Experiment' , 
                  'args' : getDict(algorithm = 'AvP2', lambda_0 = 0.75, lambda_max = 0.75),
                  'change' : 0.10033048883397684 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'Wirtinger', warmup_iter = 50),
                  'change' : 0.07937133199674927 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'CDRl', formulation = 'cyclic', lambda_0 = 0.33, lambda_max = 0.33, 
                        warmup_iter = 50),
                  'change' : 2.353770515860537e-06 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'Wirtinger', Nx = 128, TOL = 1e-08, warmup_iter = 50),
                  'change' : 0.1271534572613781 },
                { 'experiment' : 'SourceLocExperiment' , 
                  'args' : getDict(algorithm = 'DRl', lambda_0 = 0.85, lambda_max = 0.85, sensors = 10, 
                        noise = True),
                  'change' : 0.020675053625160908 },
                { 'experiment' : 'SourceLocExperiment' , 
                  'args' : getDict(algorithm = 'DyRePr', noise = True),
                  'change' : 7.678266823807028e-11 },
                { 'experiment' : 'CDI_Experiment' , 
                  'args' : getDict(algorithm = 'CDRl', lambda_0 = 1.0, lambda_max = 1.0),
                  'change' : 2.862467500047731 },
                { 'experiment' : 'JWST_Experiment' , 
                  'args' : getDict(algorithm = 'CDRl', formulation = 'cyclic', lambda_0 = 1.0, lambda_max = 1.0, 
                        noise = True),
                  'change' : 0.12703408786199177 },
                { 'experiment' : 'MC_Experiment' , 
                  'args' : getDict(algorithm = 'DR', fraction_known_entrees = 0.3, picture_infilling = True),
                  'change' : 4.217486311845684 },
                { 'experiment' : 'SumOfLeastSquares_Experiment' , 
                  'args' : getDict(algorithm = 'BCD', inner_MAXIT = 10, inner_algorithm = 'GPM', Nx = 8, 
                        Ny = 12, 
                        Nz = 50, 
                        randomizedAlgo = True),
                  'change' : 0.5026284228978356 },
                { 'experiment' : 'JWST_Experiment' , 
                  'args' : getDict(algorithm = 'CDRl', formulation = 'cyclic', lambda_0 = 0.25, lambda_max = 0.25, 
                        noise = True, 
                        rotate = True),
                  'change' : 0.32268316184971557 },
                { 'experiment' : 'Krueger_Experiment' , 
                  'args' : getDict(algorithm = 'DRAP'),
                  'change' : 0.010319098395357586 },
                { 'experiment' : 'JWST_Experiment' , 
                  'args' : getDict(algorithm = 'ADMM', lambda_0 = 3.0, lambda_max = 3.0, noise = True, 
                        rotate = True, 
                        anim = True),
                  'change' : 0.008831951107154944 },
                { 'experiment' : 'SourceLocExperiment' , 
                  'args' : getDict(algorithm = 'AvP2', formulation = 'NSLS', lambda_0 = 0.3, lambda_max = 0.3),
                  'change' : 0.00048037722269640807 },
                { 'experiment' : 'Sparse2_Experiment' , 
                  'args' : getDict(algorithm = 'CP', cutoff = 5),
                  'change' : 0.06760865009032889 },
                { 'experiment' : 'JWST_Experiment' , 
                  'args' : getDict(algorithm = 'DRl', lambda_0 = 0.55, lambda_max = 0.55, anim = True),
                  'change' : 0.22828418482423143 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'AvP', Nx = 128, TOL = 1e-08, warmup_iter = 50),
                  'change' : 0.15264502836759364 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'DRl', lambda_0 = 0.6, lambda_max = 0.6, warmup_iter = 50),
                  'change' : 0.1788930467623773 },
                { 'experiment' : 'JWST_Experiment' , 
                  'args' : getDict(algorithm = 'AvP'),
                  'change' : 0.2025744888623074 },
                { 'experiment' : 'randomExperiment' , 
                  'args' : getDict(algorithm = 'ADM', domain_objective = 'none', image_objective = 'linfty', coupling_function = 'Square_Quadratic_convex', 
                        coupling_approximation = 'quadratic', 
                        Bregman_potential = 'l2', 
                        lambda_0 = 0.05, 
                        lambda_max = 0.05, 
                        Lip_grad_max = 15, 
                        augmentation_scaling = 1, 
                        domain_dim = 30, 
                        image_dim = 40),
                  'change' : 0.1470104988440247 },
                { 'experiment' : 'JWST_Experiment' , 
                  'args' : getDict(algorithm = 'AvP', accelerator_name = 'GenericAccelerator'),
                  'change' : 0.3221295524342829 },
                { 'experiment' : 'randomExperiment' , 
                  'args' : getDict(algorithm = 'ADM', domain_objective = 'l2', image_objective = 'l1', Bregman_potential = 'h_4', 
                        lambda_0 = 0.5, 
                        lambda_max = 0.5, 
                        Lip_grad_max = 1, 
                        augmentation_scaling = 0.001, 
                        image_dim = 51),
                  'change' : 1.6725482521983441 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'AvP2', lambda_0 = 0.75, lambda_max = 0.75, TOL = 5e-10, 
                        warmup_iter = 50),
                  'change' : 0.06320474186281881 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'DyRePr', constraint = 'support only'),
                  'change' : 0.0012983727360050313 },
                { 'experiment' : 'randomExperiment' , 
                  'args' : getDict(algorithm = 'ADM', domain_objective = 'l0', Bregman_potential = 'l2', lambda_0 = 0.05, 
                        lambda_max = 0.05, 
                        Lip_grad_max = 1, 
                        augmentation_scaling = 0.15, 
                        image_dim = 50),
                  'change' : 4984.273243923446 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'QNAvP'),
                  'change' : 0.448032865731089 },
                { 'experiment' : 'Sparse2_Experiment' , 
                  'args' : getDict(algorithm = 'QNAvP', cutoff = 25),
                  'change' : 0.030304618173895202 },
                { 'experiment' : 'MC_Experiment' , 
                  'args' : getDict(algorithm = 'ALS', fraction_known_entrees = 0.3, rank = 6, picture_infilling = True),
                  'change' : 6.489835284828498 },
                { 'experiment' : 'JWST_Experiment' , 
                  'args' : getDict(algorithm = 'CADRl', formulation = 'cyclic', lambda_0 = 1.0, lambda_max = 1.0, 
                        noise = True, 
                        rotate = True),
                  'change' : 0.5110526901550254 },
                { 'experiment' : 'SourceLocExperiment' , 
                  'args' : getDict(algorithm = 'QNAvP', noise = True),
                  'change' : 0.0 },
                { 'experiment' : 'JWST_Experiment' , 
                  'args' : getDict(algorithm = 'AvP', TOL = 5e-15, noise = True),
                  'change' : 0.18617585760964395 },
                { 'experiment' : 'randomExperiment' , 
                  'args' : getDict(algorithm = 'ADM', domain_objective = 'none', image_objective = 'l1', coupling_function = 'Square_Quadratic_convex', 
                        coupling_approximation = 'quadratic', 
                        Bregman_potential = 'None', 
                        Gprox = '3prox', 
                        lambda_0 = 0.05, 
                        lambda_max = 0.05, 
                        Lip_grad_max = 5, 
                        augmentation_scaling = 1, 
                        domain_dim = 50, 
                        image_dim = 51),
                  'change' : 1.0065341790968372 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'DRl', lambda_0 = 1.0, lambda_max = 1.0, warmup_iter = 50),
                  'change' : 0.5833023914427733 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'AvP', Nx = 128, TOL = 1e-08, data_ball = 1e-15, 
                        accelerator_name = 'GenericAccelerator', 
                        warmup_iter = 50),
                  'change' : 0.29392940967823905 },
                { 'experiment' : 'SourceLocExperiment' , 
                  'args' : getDict(algorithm = 'CDRl', formulation = 'cyclic', lambda_0 = 1, lambda_max = 1),
                  'change' : 1.4354991469503436e-06 },
                { 'experiment' : 'Sparse2_Experiment' , 
                  'args' : getDict(algorithm = 'QNAvP'),
                  'change' : 0.060922680935644125 },
                { 'experiment' : 'randomExperiment' , 
                  'args' : getDict(algorithm = 'ADM', domain_objective = 'l1', lambda_0 = 0.1, lambda_max = 0.1, 
                        Lip_grad_max = 1, 
                        Bregman_potential = 'l2', 
                        augmentation_scaling = 0.016, 
                        image_dim = 51),
                  'change' : 84.93611228333215 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'CP'),
                  'change' : 0.6598811706693641 },
                { 'experiment' : 'randomExperiment' , 
                  'args' : getDict(algorithm = 'ADM', domain_objective = 'none', image_objective = 'l1', coupling_function = 'Square_Quadratic_convex', 
                        coupling_approximation = 'linear', 
                        Bregman_potential = 'l2', 
                        lambda_0 = 0.05, 
                        lambda_max = 0.05, 
                        Lip_grad_max = 5, 
                        augmentation_scaling = 0.02, 
                        domain_dim = 50, 
                        image_dim = 51),
                  'change' : 931055.979434413 },
                { 'experiment' : 'SourceLocExperiment' , 
                  'args' : getDict(algorithm = 'AvP2', formulation = 'NSLS', lambda_0 = 0.3, lambda_max = 0.3, 
                        noise = True),
                  'change' : 0.0009981138344866696 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'AvP', constraint = 'support only', accelerator_name = 'GenericAccelerator'),
                  'change' : 0.7138593217728066 },
                { 'experiment' : 'SourceLocExperiment' , 
                  'args' : getDict(algorithm = 'DRl', lambda_0 = 1.0, lambda_max = 1.0, sensors = 10),
                  'change' : 0.007111991210504244 },
                { 'experiment' : 'Krueger_Experiment' , 
                  'args' : getDict(algorithm = 'DRl', data_ball = 20, lambda_0 = 0.5, lambda_max = 0.5),
                  'change' : 0.011437042229841014 },
                { 'experiment' : 'SourceLocExperiment' , 
                  'args' : getDict(algorithm = 'DRl', lambda_0 = 1.0, lambda_max = 1.0, sensors = 10, 
                        noise = True),
                  'change' : 0.05069356690205844 },
                { 'experiment' : 'Sparse2_Experiment' , 
                  'args' : getDict(algorithm = 'CP', debug = False),
                  'change' : 0.02158555449642067 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'DRl', lambda_0 = 0.6, lambda_max = 0.6),
                  'change' : 0.7858468818207088 },
                { 'experiment' : 'JWST_Experiment' , 
                  'args' : getDict(algorithm = 'AvP2', lambda_0 = 0.75, lambda_max = 0.75, noise = True),
                  'change' : 0.12340035109401419 },
                { 'experiment' : 'JWST_Experiment' , 
                  'args' : getDict(algorithm = 'DRl', lambda_0 = 0.55, lambda_max = 0.55, noise = True),
                  'change' : 0.22312478914126252 },
                { 'experiment' : 'JWST_Experiment' , 
                  'args' : getDict(algorithm = 'AP'),
                  'change' : 0.2025744888623074 },
                { 'experiment' : 'JWST_Experiment' , 
                  'args' : getDict(algorithm = 'QNAvP'),
                  'change' : 0.25377823684639794 },
                { 'experiment' : 'SumOfLeastSquares_Experiment' , 
                  'args' : getDict(algorithm = 'GPM', Nx = 8, Ny = 12, Nz = 50),
                  'change' : 0.2839486834148186 },
                { 'experiment' : 'Sparse2_Experiment' , 
                  'args' : getDict(algorithm = 'QNAvP', cutoff = 5),
                  'change' : 0.053632801930678264 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'DRAP', warmup_iter = 50),
                  'change' : 0.21581198426462786 },
                { 'experiment' : 'Krueger_Experiment' , 
                  'args' : getDict(algorithm = 'DRl', lambda_0 = 0.5, lambda_max = 0.5),
                  'change' : 0.011437042229841014 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'DyRePr', constraint = 'support only', warmup_iter = 50),
                  'change' : 0.0007221671483883774 },
                { 'experiment' : 'CDI_Experiment' , 
                  'args' : getDict(algorithm = 'CDRl', lambda_0 = 0.9, lambda_max = 0.9),
                  'change' : 3.6017082134509844 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'CP', Nx = 128, formulation = 'cyclic', lambda_0 = 0.75, 
                        lambda_max = 0.75, 
                        TOL = 1e-08, 
                        data_ball = 1e-15, 
                        warmup_iter = 50, 
                        iterate_monitor_name = 'IterateMonitor'),
                  'change' : 0.593594425462113 },
                { 'experiment' : 'Krueger_Experiment' , 
                  'args' : getDict(algorithm = 'CP'),
                  'change' : 0.009297330050179213 },
                { 'experiment' : 'CDP_Experiment' , 
                  'args' : getDict(algorithm = 'QNAvP', warmup_iter = 50),
                  'change' : 0.26707698412354025 },
                { 'experiment' : 'Sparse2_Experiment' , 
                  'args' : getDict(algorithm = 'CP', cutoff = 20),
                  'change' : 0.02677541008179117 },
                { 'experiment' : 'SourceLocExperiment' , 
                  'args' : getDict(algorithm = 'DRl', lambda_0 = 1.0, lambda_max = 1.0, noise = True),
                  'change' : 0.01846731838926986 },
                { 'experiment' : 'SIRIUS_CDI_Experiment' , 
                  'args' : getDict(algorithm = 'DRl', Nx = 128, Ny = 128, lambda_0 = 0.9, 
                        lambda_max = 0.5),
                  'change' : 96.01773675302928 },
                { 'experiment' : 'JWST_Experiment' , 
                  'args' : getDict(algorithm = 'PHeBIE', anim = True, noise = True),
                  'change' : 0.3136466105969367 },
                { 'experiment' : 'randomExperiment' , 
                  'args' : getDict(algorithm = 'ADM', domain_objective = 'l1', image_objective = 'linfty', Bregman_potential = 'h_4', 
                        lambda_0 = 0.05, 
                        lambda_max = 0.05, 
                        Lip_grad_max = 0.5, 
                        augmentation_scaling = 0.005, 
                        image_dim = 51),
                  'change' : 3.115162877887663 }
                ]
    

    testEngine = TestEngine(testData, maxIterCount=5, maxError=1e-5)

    # to select the appropriate mode of execution of this script
    # uncomment the line you need, and comment out the others
    # mode = Mode.GenerateTestData
    mode = Mode.UpdateTestData
    # mode = Mode.RunTests

    t = time.time()
    if mode == Mode.GenerateTestData:
        testEngine.generateTestData()
    elif mode == Mode.UpdateTestData:
        testEngine.updateTestData()
    else: #Mode.RunTests
        success = testEngine.run()
        if success:
            print("All", len(testEngine.testData), "tests were successful.")
        else:
            print("Regression test failed:", testEngine.failedCount, "demo(s) failed.")
    print("Took", time.time() - t, "seconds.")

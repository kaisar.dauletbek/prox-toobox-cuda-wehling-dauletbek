# Build with
#
#     nix-build env.nix -o env
#
# VSCode will see the generated ./env as a virtualenv and offer it as an
# interpreter in the ‘Python: Select Interpreter’ command.
#
# To run Python with all dependencies available, use the wrapper script
# env/bin/python. Alternatively, there's a shell.nix that allows you to
# simply run
#
#   nix-shell
#
# to open a shell with the python environment added to $PATH.

{ nixpkgs ? <nixpkgs>, pkgs ? import nixpkgs {} }:

with pkgs;

poetry2nix.mkPoetryEnv {
  projectDir = ./.;
  preferWheels = true;
  overrides = poetry2nix.overrides.withDefaults (self: super: {
    # We need to explicityly specify the dependency on the plotting backend.
    # Use tkinter for simplicity.
    matplotlib = super.matplotlib.overridePythonAttrs (attrs: {
      propagatedBuildInputs = attrs.propagatedBuildInputs ++ [
        self.tkinter
      ];
    });
  });
}
